import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

from typing import Optional

import hydra
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import LightningDataModule

from src.data import NAG
from src.visualization.visualization import *
import os.path as osp

from src.metrics import *

from tqdm import tqdm

OmegaConf.register_new_resolver("eval", eval)

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:
    
    if cfg.get("device_id"):
        torch.cuda.set_device("cuda:"+str(cfg.device_id))    
    
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule) # create datamodule
    datamodule.setup(test=True) # create train dataset and launch preprocessing
    # set test to True to test only on train set
    nb_level = len(cfg.datamodule.pcp_regularization)
    visualise_interfaces(datamodule.train_dataloader,cfg.datamodule.num_classes,20,"visu_interfaces_")

if __name__ == "__main__":
    test()
