import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

from typing import Optional

import hydra
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import LightningDataModule

from src.data import NAG
from src.visualization.visualization import *
from src.utils import label_to_y
import os.path as osp

import csv

from src.metrics import *

from tqdm import tqdm
from time import time

OmegaConf.register_new_resolver("eval", eval)

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:
    
    if cfg.get("device_id"):
        torch.cuda.set_device("cuda:"+str(cfg.device_id))    
    
    """ Setings : """
    # comment this line to test on the complete dataset
    cfg.datamodule.test_subset = 5 # size of the subset to test on

    pre_transform = False # force recomputation of preprocessing
    train_transform = False 
    val_transform = False
    test_transform = False

    train_onthefly_transform = False
    val_onthefly_transform = False
    test_onthefly_transform = False

    ### visualisation
    visu = False

    ### save transforms time
    measure_time = True
    cfg.datamodule.measure_time = measure_time
    
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule) # create datamodule
    datamodule.setup(test=True) # create train dataset and do pre_processing if not already done
    if pre_transform and datamodule.pre_transform is not None: # force preprocessing recomputation
        datamodule.prepare_data(test=True)
    # set test to True to test only on train set

    list_train_processed_data = datamodule.train_dataset.processed_file_names[:cfg.datamodule.test_subset]

    nlevel = len(cfg.datamodule.pcp_regularization)
    
    # keep track of size and quality of over segmentation
    size = averageMeter()
    meters = []
    for l in range(nlevel):
        meters.append(averageMeter())
    cm = [ConfusionMatrix(cfg.datamodule.num_classes) for _ in range(nlevel)]

    for path in tqdm(list_train_processed_data,ascii=True):
        ### pre Process only ###
        nag = NAG.load(osp.join(datamodule.train_dataset.root,"processed",path))
        if visu:
            print("visualization preprocessed data : ",osp.splitext(osp.split(path)[1])[0])
            visualize(nag,osp.expanduser("visualise_preprocessed_"+osp.splitext(osp.split(path)[1])[0]),
                keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                class_color=datamodule.train_dataset.class_colors)
        
        ### Transforms ### 
        if train_transform and datamodule.train_transform is not None:
            train_nag = datamodule.train_transform(nag.clone())
            if visu:
                print("visualization 'train_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_train_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
            use_train_nag = True
        else:
            use_train_nag = False
        if val_transform and datamodule.val_transform is not None:
            val_nag = datamodule.val_transform(nag.clone())
            if visu:
                print("visualization 'val_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_val_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
            use_val_nag = True
        else:
            use_val_nag = False
        if test_transform and datamodule.test_transform is not None:
            test_nag = datamodule.test_transform(nag.clone())
            if visu:
                print("visualization 'test_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_test_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
            use_test_nag = True
        else:
            use_test_nag = False

        ### on the fly Transforms ###
        nag = nag.cuda()
        if train_onthefly_transform and datamodule.on_device_train_transform is not None:
            tmp_nag = datamodule.on_device_train_transform(train_nag.cuda() if use_train_nag else nag.clone()).cpu()
            if visu:
                print("visualization 'train_device_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_train_device_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
        if val_onthefly_transform and datamodule.on_device_val_transform is not None:
            tmp_nag = datamodule.on_device_val_transform(val_nag.cuda() if use_val_nag else nag.clone()).cpu()
            if visu:
                print("visualization 'val_device_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_val_device_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
        if test_onthefly_transform and datamodule.on_device_test_transform is not None:
            tmp_nag = datamodule.on_device_test_transform(test_nag.cuda() if use_test_nag else nag.clone()).cpu()
            if visu:
                print("visualization 'test_device_processed' data : ",osp.splitext(osp.split(path)[1])[0])
                visualize(tmp_nag,osp.expanduser("visualise_test_device_processed_"+osp.splitext(osp.split(path)[1])[0]),
                    keys=["image","label","SegError"], straight_line_tol=cfg.datamodule.straight_line_tol,
                    class_color=datamodule.train_dataset.class_colors)
        nag = nag.cpu()
        size.update(nag[0].pos.shape[0])
        for l in range(nlevel):
            sizes = nag.get_sub_size(l+1).numpy()
            meters[l].update(sizes)
            if getattr(nag[0],"y"):
                y = nag[0].y
            else:
                y = label_to_y(nag[0].label,cfg.datamodule.num_classes)
            cm[l].update(nag[l+1].y.argmax(dim=1)[nag.get_super_index(high=l+1,low=0)],y)

    if measure_time:
        with open(osp.join(cfg.paths.output_dir,"time.csv"),'w',newline='') as csvfile:
            writer = csv.writer(csvfile)
            def print_transform_time(transform):
                if transform is not None:
                    writer.writerow(["transform"])
                    writer.writerow(["Detail operations"])
                    writer.writerow(["name","nb usage","average time","standard deviation","total time"])
                    for t in transform.transforms:
                        if hasattr(t,"time"):
                            writer.writerow([t.__class__.__name__,t.time.count.item(),t.time.avg.item(),t.time.std.item(),t.time.sum.item()])
                    writer.writerow([""])
            
            if pre_transform:
                writer.writerow(["Pre-Processing"])
                writer.writerow(["nb usage","average time","standard deviation","total time"])
                writer.writerow([datamodule.pre_process_time.count.item(), datamodule.pre_process_time.avg.item(), 
                    datamodule.pre_process_time.std.item(), datamodule.pre_process_time.sum.item()])
                print_transform_time(datamodule.pre_transform)
            
            if train_transform:
                writer.writerow(["Train processing"])
                print_transform_time(datamodule.train_transform)

            if val_transform:
                writer.writerow(["Validation processing"])
                print_transform_time(datamodule.val_transform)

            if test_transform:
                writer.writerow(["Test processing"])
                print_transform_time(datamodule.test_transform)

            if train_onthefly_transform:
                writer.writerow(["otf Train processing"])
                print_transform_time(datamodule.on_device_train_transform)

            if val_onthefly_transform:
                writer.writerow(["otf Validation processing"])
                print_transform_time(datamodule.on_device_val_transform)

            if test_onthefly_transform:
                writer.writerow(["otf Test processing"])
                print_transform_time(datamodule.on_device_test_transform)
    
    print("avg image size : ",size.avg)
    print("std image size : ",size.std)
    for l in range(nlevel):
        print("avg sp size level ",l+1," : ",meters[l].avg)
        print("std sp size level ",l+1," : ",meters[l].std)
        cm[l].print_metrics(class_names = datamodule.train_dataset.class_names)
        print("")

if __name__ == "__main__":
    test()
