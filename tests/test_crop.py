import pyrootutils
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

import pandas as pd

import hydra
#from hydra.core.hydra_config import HydraConfig
from omegaconf import OmegaConf, DictConfig, open_dict
from pytorch_lightning import Callback, LightningDataModule, LightningModule, Trainer
import torch
import numpy as np
from torchvision.utils import save_image
from torch_scatter import scatter_mean
from src.data import Data,NAG
from tqdm import tqdm
import os
import os.path as osp
from src.metrics import *
import csv
from typing import Optional

#import pytorch_lightning as pl
#from tests.test_train import *
OmegaConf.register_new_resolver("eval", eval)

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test_crop(cfg: DictConfig) -> Optional[float]:

    # enforce cuda device id to the chosen one
    if hasattr(cfg,"device_id") and cfg.trainer.get("accelerator") == "gpu":
        device = "cuda:"+str(cfg.device_id)
        torch.cuda.set_device("cuda:"+str(cfg.device_id))
        os.sched_setaffinity(0,[0+41*cfg.device_id,40+41*cfg.device_id])
    else:
        device = "cpu"

    with open_dict(cfg):
        cfg.datamodule.dataloader.batch_size = 1
        cfg.datamodule.pixel_basic_load_keys.append("image")

    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)
    datamodule.setup()

    dir_name = "test_crop"
    if not osp.exists(dir_name):
        os.makedirs(dir_name)
    compt_visu = 0
    max_visu = 5

    fraction_side = 0.5 # length of the croped sides as fraction of the original image

    initial_crop = averageMeter()
    out_crop = averageMeter()
    in_crop = averageMeter()
    bounding_crop = averageMeter()
    
    for batch in tqdm(datamodule.train_dataloader(), ascii=True):
        nag = batch[0].to(device)

        nb_pixels = nag[0].num_pixels

        crop_size = (nag[0].img_size*(fraction_side)).long()
        x_crop = torch.randint((nag[0].img_size[0]-crop_size[0]).item(),(1,),device=device)
        y_crop = torch.randint((nag[0].img_size[1]-crop_size[1]).item(),(1,),device=device)

        mask_crop = torch.zeros(tuple(nag[0].img_size),device=device)
        mask_crop[x_crop:x_crop+crop_size[0],y_crop:y_crop+crop_size[1]] = 1
        initial_crop.update(mask_crop.sum()/nb_pixels)
        if compt_visu < max_visu:
            save_image(nag[0].image.float()*mask_crop,osp.join(dir_name,f"test_image_mask_{compt_visu}_{fraction_side}.png")) # rectangle croped image

        sp_in_crop = scatter_mean(mask_crop.flatten(),nag.get_super_index(nag.num_levels-1,0))
        mask_crop_sp = (sp_in_crop>0)[nag.get_super_index(3,0)]*1 # au moins 1 px dans le crop
        out_crop.update(mask_crop_sp.sum()/nb_pixels)
        if compt_visu < max_visu:
            save_image(nag[0].image.float()*mask_crop_sp.reshape(tuple(nag[0].img_size)),osp.join(dir_name,f"test_image_mask_sp_{compt_visu}_{fraction_side}.png")) # sp croped image

        big_mask = (torch.any(mask_crop_sp.reshape(tuple(nag[0].img_size)),dim=1).unsqueeze(1) * torch.any(mask_crop_sp.reshape(tuple(nag[0].img_size)),dim=0).unsqueeze(0))
        bounding_crop.update(big_mask.sum()/nb_pixels)
        if compt_visu < max_visu:
            save_image(nag[0].image.float()*big_mask,osp.join(dir_name,f"test_image_big_mask_{compt_visu}_{fraction_side}.png")) # rectangle croped image including complete sp partaly included in first crop 

        mask_crop_sp_in = (sp_in_crop>=1-(1e-12))[nag.get_super_index(3,0)]*1 # all pixel included
        in_crop.update(mask_crop_sp_in.sum()/nb_pixels)
        if compt_visu < max_visu:
            save_image(nag[0].image.float()*mask_crop_sp_in.reshape(tuple(nag[0].img_size)),osp.join(dir_name,f"test_image_mask_sp_in_{compt_visu}_{fraction_side}.png")) # sp croped image
            compt_visu += 1
    
    with open(osp.join(dir_name,f"result_{fraction_side}.csv"),'w',newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["result crop",fraction_side])
        writer.writerow(["measure","mean","std"])
        writer.writerow(["initial crop",initial_crop.avg.item(),initial_crop.std.item()])
        writer.writerow(["in crop",in_crop.avg.item(),in_crop.std.item()])
        writer.writerow(["out crop",out_crop.avg.item(),out_crop.std.item()])
        writer.writerow(["bounding box crop",bounding_crop.avg.item(),bounding_crop.std.item()])



if __name__ == "__main__":
    test_crop()
