import pyrootutils
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

from typing import Optional
import hydra
import torch
import pytorch_lightning as pl
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import LightningDataModule, LightningModule
from progressbar import ProgressBar 
from src.data import NAG,NAGBatch
from src.metrics import *
from time import time
import csv
import os
from omegaconf import open_dict
from src.train import train

""" 
    designed to search for the bast number of worker and tasks given to them for the computing power available
    results saved in worker_time.csv
    
    you can set the search parameters 
        defaults : 
            nb worker in [2,14]
            nb tests 5
            num epoch pre test = 1
    
    worker can pack the data for transfert, resize the images for convolution efficiency, transfrom the Nag in advance
"""

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:

    # enforce cuda device id to the chosen one
    if hasattr(cfg,"device_id") and cfg.trainer.get("accelerator") == "gpu":
        device = "cuda:"+str(cfg.device_id)
        assert cfg.device_id < torch.cuda.device_count()
        torch.cuda.set_device("cuda:"+str(cfg.device_id))
        list_cpu = list(os.sched_getaffinity(0))
        os.sched_setaffinity(0,list_cpu[0+int(len(list_cpu)/torch.cuda.device_count())*cfg.device_id:int(len(list_cpu)/torch.cuda.device_count())+int(len(list_cpu)/torch.cuda.device_count())*cfg.device_id])
    else:
        device = "cpu"

    csv_file = open("worker_time.csv",'w',newline='')
    writer = csv.writer(csv_file)
    writer.writerow(["Worker time study"])
    writer.writerow(["nb worker", "stoped", "packing", "resize", "batching", "transform", "total time", "epoch time", "waiting worker", "first step worker", "process", "learning"])

    model: LightningModule = hydra.utils.instantiate(cfg.model)
    model = model.to(device)
    compt_parameters = 0
    for p in model.parameters():
        compt_parameters += p.numel()
    
    ### search parameters ###
    learning_rate = 1e-2
    optimizer = torch.optim.Adam(model.parameters(),lr=learning_rate) 
    num_epoch = 1
    search_interval = [2,14] # nb worker min and max
    nb_study = 5 # max number of tested value
    #########################

    study_list = [int(search_interval[0]+((search_interval[1]-search_interval[0])/(nb_study-1))*i) for i in range(nb_study)]
    best_time = [1e1000]*8
    for w in study_list:
        
        best_current_time = [1e1000]*7
        with open_dict(cfg):
            cfg.datamodule.dataloader.num_workers = w

        for selection in [[i>0,i>1,i>2,i>3] for i in range(5)]:
            with open_dict(cfg):
                cfg.datamodule.pack_for_transfert = selection[0]
                cfg.datamodule.resize_in_worker = selection[1]
                cfg.datamodule.batching_in_worker = selection[2]
                cfg.datamodule.transform_in_worker = selection[3]

            datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)
            datamodule.setup()

            stoped = False
            compt_batch = 0
            worker = 0
            process = 0
            learning = 0
            nb_batch = len(datamodule.train_dataloader())
            bar = ProgressBar(num_epoch*nb_batch).start()
            origine = time()
            start = time() # wait for worker
            for _ in range(num_epoch):
                for batch in datamodule.train_dataloader():
                    worker += time() - start
                    if compt_batch == 0:
                        first_worker = worker
                    start = time()
                    batch = datamodule.transfer_batch_to_device(batch,device,0)
                    nag = datamodule.on_after_batch_transfer(batch,0)
                    del batch
                    process += time() - start
                    start = time()
                    optimizer.zero_grad()
                    loss,pred,yhist = model.model_step(nag)
                    loss.backward()
                    optimizer.step()
                    learning += time() - start
                    compt_batch += 1
                    bar.update(compt_batch)
                    start = time() # worker waiting time
                    if best_current_time[0] < time()-origine: # stop of already longer
                        stoped = True
                        break
                if stoped:
                    break
            end = time()
            bar.finish()
            if best_current_time[0] > end-origine:
                best_current_time[0] = end-origine
                best_current_time[1] = (end - origine)/num_epoch
                best_current_time[2] = worker
                best_current_time[3] = first_worker
                best_current_time[4] = process
                best_current_time[5] = learning
                best_current_time[6] = selection
            writer.writerow([w, stoped, selection[0], selection[1], selection[2], selection[3], end-origine, (end-origine)/num_epoch, worker, first_worker, process, learning])
        if best_time[0] > best_current_time[0]:
            best_time[0] = best_current_time[0]
            best_time[1] = best_current_time[1]
            best_time[2] = best_current_time[2]
            best_time[3] = best_current_time[3]
            best_time[4] = best_current_time[4]
            best_time[5] = best_current_time[5]
            best_time[6] = best_current_time[6]
            best_time[7] = w
        writer.writerow([f"Best for {w} worker"])
        writer.writerow([w, False, best_current_time[6][0], best_current_time[6][1], best_current_time[6][2], best_current_time[6][3], best_current_time[0], best_current_time[1], best_current_time[2], best_current_time[3], best_current_time[4], best_current_time[5]])
        writer.writerow([''])
    writer.writerow(["Best over all"])
    writer.writerow([best_time[7], False, best_time[6][0], best_time[6][1], best_time[6][2], best_time[6][3], best_time[0], best_time[1], best_time[2], best_time[3], best_time[4], best_time[5]])

if __name__ == "__main__":
    test()
