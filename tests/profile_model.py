import pyrootutils
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

# Hack importing pandas here to bypass some conflicts with hydra
import pandas as pd

from typing import List, Optional, Tuple

import hydra
import torch
import torch_geometric
import pytorch_lightning as pl
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import Callback, LightningDataModule, LightningModule, Trainer
from pytorch_lightning.loggers import Logger

#from src import utils

from progressbar import ProgressBar 

from src.data import NAG,NAGBatch
from src.metrics import *

from time import time

from torch.profiler import profile, record_function, ProfilerActivity

import os

from omegaconf import open_dict
from src.train import train

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:

    # enforce cuda device id to the chosen one
    if hasattr(cfg,"device_id") and cfg.trainer.get("accelerator") == "gpu":
        device = "cuda:"+str(cfg.device_id)
        assert cfg.device_id < torch.cuda.device_count()
        torch.cuda.set_device("cuda:"+str(cfg.device_id))
        list_cpu = list(os.sched_getaffinity(0))
        os.sched_setaffinity(0,list_cpu[0+int(len(list_cpu)/torch.cuda.device_count())*cfg.device_id:int(len(list_cpu)/torch.cuda.device_count())+int(len(list_cpu)/torch.cuda.device_count())*cfg.device_id])
    else:
        device = "cpu"

    print("setup datamodule")
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)
    datamodule.setup()
    
    print("setup model")
    model: LightningModule = hydra.utils.instantiate(cfg.model)
    model = model.to(device)
    compt_parameters = 0
    for p in model.parameters():
        compt_parameters += p.numel()
    print("nb parameters : ",compt_parameters)
    
    print("setup trainer")
    learning_rate = 1e-2
    optimizer = torch.optim.Adam(model.parameters(),lr=learning_rate)
    
    print("setup profiler")
    profiling = False # True => profile on num_batch, False => time of num_epoch
    num_epoch = 1
    
    # profiler scheduler
    wait = 15 # 20 workers 
    warmup = 2 # 2 recommanded
    active = 8 # max = 8 (json too big)
    num_batch = wait + warmup + active
     
    if profiling:
        print("profile")
        compt_batch = 0
        bar = ProgressBar(num_batch).start()
        origine = time()
            
        with profile(activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA],schedule=torch.profiler.schedule(wait=wait,warmup=warmup,active=active),record_shapes=False,profile_memory=False,with_stack=False,with_flops=False,with_modules=False) as prof:
            for batch in datamodule.train_dataloader():

                optimizer.zero_grad()
                with record_function("transfert"):
                    batch = datamodule.transfer_batch_to_device(batch,device,0)
                
                with record_function("process"):
                    nag = datamodule.on_after_batch_transfer(batch,0)
                
                del batch

                with record_function("forward"):
                    loss,pred,yhist = model.model_step(nag)
                
                with record_function("backward"):
                    loss.backward()
                
                with record_function("opti step"):
                    optimizer.step()

                compt_batch += 1
                bar.update(compt_batch)
                prof.step()
                if compt_batch >= num_batch:
                    break
            end = time()
            bar.finish()
            
        prof.export_chrome_trace("profile_spot.json")
        print("total time : ",end-origine)
    
    else:
        print("measure epoch")
        compt_batch = 0
        worker = 0
        process = 0
        learning = 0
        nb_batch = len(datamodule.train_dataloader())
        print("nb_batch",nb_batch)
        bar = ProgressBar(num_epoch*nb_batch).start()
        origine = time()
        start = time() # wait for worker
        for _ in range(num_epoch):
            for batch in datamodule.train_dataloader():
                worker += time() - start
                if compt_batch == 0:
                    first_worker = worker
                start = time()
                batch = datamodule.transfer_batch_to_device(batch,device,0)
                nag = datamodule.on_after_batch_transfer(batch,0)
                del batch
                process += time() - start
                start = time()
                optimizer.zero_grad()
                loss,pred,yhist = model.model_step(nag)
                loss.backward()
                optimizer.step()
                learning += time() - start
                compt_batch += 1
                bar.update(compt_batch)
                start = time() # worker waiting time
        end = time()
        bar.finish()

        print("total time : ",end - origine)
        print("time per epoch : ",(end - origine)/num_epoch)
        print("waiting worker time : ",worker)
        print("first worker step : ",first_worker)
        print("waiting worker time (without first) : ",worker-first_worker)
        print("process time : ",process)
        print("learning time : ",learning)

if __name__ == "__main__":
    test()
