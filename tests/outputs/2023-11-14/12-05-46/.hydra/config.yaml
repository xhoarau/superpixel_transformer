task_name: train
optimized_metric: val/miou
tags:
- dev
train: true
test: true
compile: false
ckpt_path: null
seed: null
float32_matmul_precision: highest
datamodule:
  partition_hf:
  - rgb
  point_hf:
  - rgb
  segment_base_hf: []
  segment_mean_hf: []
  segment_std_hf: []
  edge_hf:
  - mean_off
  - std_off
  - mean_dist
  v_edge_hf: []
  h_edge_hf_need_normal: ${eval:'("normal_angle" in ${datamodule.edge_hf}) or ("angle_source"
    in ${datamodule.edge_hf}) or ("angle_target" in ${datamodule.edge_hf})'}
  v_edge_hf_need_normal: ${eval:'"normal_angle" in ${datamodule.v_edge_hf}'}
  edge_hf_need_log_length: ${eval:'"log_length" in ${datamodule.edge_hf} + ${datamodule.v_edge_hf}'}
  edge_hf_need_log_surface: ${eval:'"log_surface" in ${datamodule.edge_hf} + ${datamodule.v_edge_hf}'}
  edge_hf_need_log_volume: ${eval:'"log_volume" in ${datamodule.edge_hf} + ${datamodule.v_edge_hf}'}
  edge_hf_need_log_size: ${eval:'"log_size" in ${datamodule.edge_hf} + ${datamodule.v_edge_hf}'}
  extra_point_hf: ${eval:'["normal"] * ${datamodule.v_edge_hf_need_normal}'}
  extra_segment_hf: ${eval:'["normal"] * (${datamodule.h_edge_hf_need_normal} + ${datamodule.v_edge_hf_need_normal})
    + ["log_length"] * ${datamodule.edge_hf_need_log_length} + ["log_surface"] * ${datamodule.edge_hf_need_log_surface}
    + ["log_volume"] * ${datamodule.edge_hf_need_log_volume} + ["log_size"] * ${datamodule.edge_hf_need_log_size}'}
  segment_hf: ${eval:'${datamodule.segment_base_hf} + ["mean_" + x for x in ${datamodule.segment_mean_hf}]
    + ["std_" + x for x in ${datamodule.segment_std_hf}]'}
  point_hf_preprocess: ${eval:'list(set( ${datamodule.partition_hf} + ${datamodule.point_hf}
    + ${datamodule.segment_mean_hf} + ${datamodule.segment_std_hf} + ${datamodule.extra_point_hf}
    )) if ${datamodule.lite_preprocessing} else None'}
  segment_base_hf_preprocess: ${eval:'list(set( ${datamodule.segment_base_hf} + ${datamodule.extra_segment_hf}
    )) if ${datamodule.lite_preprocessing} else None'}
  segment_mean_hf_preprocess: ${eval:'${datamodule.segment_mean_hf} if ${datamodule.lite_preprocessing}
    else None'}
  segment_std_hf_preprocess: ${eval:'${datamodule.segment_std_hf} if ${datamodule.lite_preprocessing}
    else None'}
  num_hf_point: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.point_hf}
    ])'}
  num_hf_segment: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.segment_hf}
    ])'}
  num_hf_edge: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.edge_hf}
    ])'}
  num_hf_v_edge: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.v_edge_hf}
    ])'}
  point_save_keys: null
  point_no_save_keys:
  - edge_index
  - edge_attr
  - node_size
  - grid_size
  segment_save_keys: null
  point_basic_load_keys:
  - pos
  - 'y'
  - super_index
  segment_basic_load_keys:
  - pos
  - 'y'
  - super_index
  - sub
  - edge_index
  - edge_attr
  point_load_keys: ${eval:'list(set( ${datamodule.point_basic_load_keys} + ${datamodule.point_hf}
    + ${datamodule.extra_point_hf} ))'}
  segment_load_keys: ${eval:'list(set( ${datamodule.segment_basic_load_keys} + ${datamodule.segment_hf}
    + ${datamodule.extra_segment_hf} ))'}
  feat_size:
    pos: 2
    rgb: 3
    hsv: 3
    lab: 3
    size: 1
    intensity: 1
    log_pos: 2
    log_rgb: 3
    log_hsv: 3
    log_lab: 3
    log_size: 1
    mean_pos: 2
    mean_rgb: 3
    mean_hsv: 3
    mean_lab: 3
    mean_size: 1
    mean_intensity: 1
    std_pos: 2
    std_rgb: 3
    std_hsv: 3
    std_lab: 3
    std_size: 1
    std_intensity: 1
    mean_off: 2
    std_off: 2
    mean_dist: 1
    centroid_dist: 1
  _target_: src.datamodules.pascalvoc.PascalVOCDataModule
  data_dir: ${paths.data_dir}
  num_classes: 21
  save_y_to_csr: true
  save_pos_dtype: float32
  save_fp_dtype: float16
  in_memory: false
  lite_preprocessing: true
  max_num_nodes: 50000
  max_num_edges: 1000000
  pre_transform:
  - transform: DataTo
    params:
      device: cpu
  - transform: PointFeatures
    params:
      keys: ${datamodule.point_hf_preprocess}
  - transform: DataTo
    params:
      device: cuda
  - transform: AdjacencyGraph
    params:
      dist: ${datamodule.pcp_dist}
      connectivity: ${datamodule.pcp_connectivity}
  - transform: DataTo
    params:
      device: cpu
  - transform: AddKeysTo
    params:
      keys: ${datamodule.partition_hf}
      to: x
      delete_after: false
  - transform: CutPursuitPartition
    params:
      regularization: ${datamodule.pcp_regularization}
      spatial_weight: ${datamodule.pcp_spatial_weight}
      cutoff: ${datamodule.pcp_cutoff}
      iterations: ${datamodule.pcp_iterations}
      parallel: true
      verbose: false
  - transform: NAGRemoveKeys
    params:
      level: all
      keys: x
  - transform: NAGTo
    params:
      device: cuda
  - transform: EdgesFeatures
    params:
      keys:
      - mean_off
      - std_off
      - mean_dist
  - transform: NAGTo
    params:
      device: cpu
  train_transform: null
  val_transform: ${datamodule.train_transform}
  test_transform: ${datamodule.val_transform}
  on_device_train_transform:
  - transform: NAGCast
  - transform: NAGAddKeysTo
    params:
      level: 0
      keys: ${eval:'ListConfig([k for k in ${datamodule.point_hf} if k != "rgb"])'}
      to: x
  - transform: NAGAddKeysTo
    params:
      level: 1+
      keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf} if k != "rgb"])'}
      to: x
  - transform: NAGColorNormalize
    params:
      level: all
  - transform: NAGColorAutoContrast
    params:
      p: ${datamodule.rgb_autocontrast}
  - transform: NAGColorDrop
    params:
      p: ${datamodule.rgb_drop}
  - transform: NAGAddKeysTo
    params:
      keys: rgb
      to: x
      strict: false
  - transform: NAGAddSelfLoops
  - transform: NodeSize
  on_device_val_transform:
  - transform: NAGCast
  - transform: NAGAddKeysTo
    params:
      level: 0
      keys: ${eval:'ListConfig([k for k in ${datamodule.point_hf} if k != "rgb"])'}
      to: x
  - transform: NAGAddKeysTo
    params:
      level: 1+
      keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf} if k != "rgb"])'}
      to: x
  - transform: NAGColorNormalize
    params:
      level: all
  - transform: NAGAddKeysTo
    params:
      keys: rgb
      to: x
      strict: false
  - transform: NAGAddSelfLoops
  - transform: NodeSize
  on_device_test_transform: ${datamodule.on_device_val_transform}
  tta_runs: null
  tta_val: false
  submit: false
  dataloader:
    batch_size: 1
    num_workers: 4
    pin_memory: true
    persistent_workers: true
  trainval: false
  pcp_regularization:
  - 0.01
  - 0.5
  pcp_spatial_weight:
  - 0.1
  - 0.1
  pcp_cutoff:
  - 10
  - 10
  pcp_k_adjacency: 10
  pcp_iterations: 15
  pcp_dist:
  - 0
  pcp_connectivity:
  - 0
  rgb_jitter: 0
  rgb_autocontrast: 0.5
  rgb_drop: 0
model:
  _target_: src.models.segmentation.PixelSegmentationModule
  num_classes: ${datamodule.num_classes}
  sampling_loss: false
  loss_type: ce_kl
  weighted_loss: true
  init_linear: null
  init_rpe: null
  multi_stage_loss_lambdas:
  - 1
  - 50
  transformer_lr_scale: 0.1
  gc_every_n_steps: 0
  optimizer:
    _target_: torch.optim.AdamW
    _partial_: true
    lr: 0.01
    weight_decay: 0.0001
  scheduler:
    _target_: src.optim.CosineAnnealingLRWithWarmup
    _partial_: true
    T_max: ${eval:'${trainer.max_epochs} - ${model.scheduler.num_warmup}'}
    eta_min: 1.0e-06
    warmup_init_lr: 1.0e-06
    num_warmup: 20
    warmup_strategy: cos
  criterion:
    _target_: torch.nn.CrossEntropyLoss
  _point_mlp:
  - 32
  - 64
  - 128
  _node_mlp_out: 32
  _h_edge_mlp_out: 32
  _v_edge_mlp_out: 32
  _point_hf_dim: ${eval:'${model.net.use_pos} * 2 + ${datamodule.num_hf_point} + ${model.net.use_diameter_parent}'}
  _node_hf_dim: ${eval:'${model.net.use_node_hf} * ${datamodule.num_hf_segment}'}
  _node_injection_dim: ${eval:'${model.net.use_pos} * 2 + ${model.net.use_diameter}
    + ${model.net.use_diameter_parent} + (${model._node_mlp_out} if ${model._node_mlp_out}
    and ${model.net.use_node_hf} and ${model._node_hf_dim} > 0 else ${model._node_hf_dim})'}
  _h_edge_hf_dim: ${datamodule.num_hf_edge}
  _v_edge_hf_dim: ${datamodule.num_hf_v_edge}
  _down_dim:
  - 64
  - 64
  - 64
  - 64
  _up_dim:
  - 64
  - 64
  - 64
  _mlp_depth: 2
  net:
    point_mlp: ${eval:'[${model._point_hf_dim}] + ${model._point_mlp}'}
    point_drop: null
    down_dim: ${eval:'${model._down_dim}[:2]'}
    down_pool_dim: ${eval:'[${model._point_mlp}[-1]] + ${model._down_dim}[:-1]'}
    down_in_mlp: ${eval:'[ [${model._node_injection_dim} + ${model._point_mlp}[-1]
      * (not ${model.net.nano}) + ${datamodule.num_hf_segment} * (${model.net.nano}
      and not ${model.net.use_node_hf})] + [${model._down_dim}[0]] * ${model._mlp_depth},
      [${model._node_injection_dim} + ${model._down_dim}[0]] + [${model._down_dim}[1]]
      * ${model._mlp_depth}]'}
    down_out_mlp: null
    down_mlp_drop: null
    down_num_heads: 16
    down_num_blocks: 3
    down_ffn_ratio: 1
    down_residual_drop: null
    down_attn_drop: null
    down_drop_path: null
    up_dim: ${eval:'${model._up_dim}[:1]'}
    up_in_mlp: ${eval:'[ [${model._node_injection_dim} + ${model._down_dim}[-1] +
      ${model._down_dim}[-2]] + [${model._up_dim}[0]] * ${model._mlp_depth}]'}
    up_out_mlp: ${model.net.down_out_mlp}
    up_mlp_drop: ${model.net.down_mlp_drop}
    up_num_heads: ${model.net.down_num_heads}
    up_num_blocks: 1
    up_ffn_ratio: ${model.net.down_ffn_ratio}
    up_residual_drop: ${model.net.down_residual_drop}
    up_attn_drop: ${model.net.down_attn_drop}
    up_drop_path: ${model.net.down_drop_path}
    activation:
      _target_: torch.nn.LeakyReLU
    norm:
      _target_: src.nn.GraphNorm
      _partial_: true
    pre_norm: true
    no_sa: false
    no_ffn: true
    qk_dim: 4
    qkv_bias: true
    qk_scale: null
    in_rpe_dim: ${eval:'${model._h_edge_mlp_out} if ${model._h_edge_mlp_out} else
      ${model._h_edge_hf_dim}'}
    k_rpe: true
    q_rpe: true
    v_rpe: true
    k_delta_rpe: false
    q_delta_rpe: false
    qk_share_rpe: false
    q_on_minus_rpe: false
    stages_share_rpe: false
    blocks_share_rpe: false
    heads_share_rpe: false
    _target_: src.models.components.spt.SPT
    nano: false
    node_mlp: ${eval:'[${model._node_hf_dim}] + [${model._node_mlp_out}] * ${model._mlp_depth}
      if ${model._node_mlp_out} and ${model.net.use_node_hf} and ${model._node_hf_dim}
      > 0 else None'}
    h_edge_mlp: ${eval:'[${model._h_edge_hf_dim}] + [${model._h_edge_mlp_out}] * ${model._mlp_depth}
      if ${model._h_edge_mlp_out} else None'}
    v_edge_mlp: ${eval:'[${model._v_edge_hf_dim}] + [${model._v_edge_mlp_out}] * ${model._mlp_depth}
      if ${model._v_edge_mlp_out} else None'}
    share_hf_mlps: false
    mlp_activation:
      _target_: torch.nn.LeakyReLU
    mlp_norm:
      _target_: src.nn.GraphNorm
      _partial_: true
    use_pos: true
    use_node_hf: true
    use_diameter: false
    use_diameter_parent: true
    pool: max
    unpool: index
    fusion: cat
    norm_mode: graph
callbacks:
  model_checkpoint:
    _target_: pytorch_lightning.callbacks.ModelCheckpoint
    dirpath: ${paths.output_dir}/checkpoints
    filename: epoch_{epoch:03d}
    monitor: val/miou
    verbose: false
    save_last: true
    save_top_k: 1
    mode: max
    auto_insert_metric_name: false
    save_weights_only: false
    every_n_train_steps: null
    train_time_interval: null
    every_n_epochs: null
    save_on_train_epoch_end: null
  early_stopping:
    _target_: pytorch_lightning.callbacks.EarlyStopping
    monitor: val/miou
    min_delta: 0.0
    patience: 500
    verbose: false
    mode: max
    strict: true
    check_finite: true
    stopping_threshold: null
    divergence_threshold: null
    check_on_train_epoch_end: null
  model_summary:
    _target_: pytorch_lightning.callbacks.RichModelSummary
    max_depth: -1
  rich_progress_bar:
    _target_: pytorch_lightning.callbacks.RichProgressBar
  lr_monitor:
    _target_: pytorch_lightning.callbacks.LearningRateMonitor
    logging_interval: epoch
    log_momentum: true
  gradient_accumulator:
    _target_: pytorch_lightning.callbacks.GradientAccumulationScheduler
    scheduling:
      0: 1
logger:
  csv:
    _target_: pytorch_lightning.loggers.csv_logs.CSVLogger
    save_dir: ${paths.output_dir}
    name: csv/
    prefix: ''
trainer:
  _target_: pytorch_lightning.Trainer
  default_root_dir: ${paths.output_dir}
  min_epochs: 1
  max_epochs: 60
  accelerator: cpu
  devices: 1
  check_val_every_n_epoch: 10
  deterministic: false
paths:
  root_dir: ${oc.env:PROJECT_ROOT}
  data_dir: ${paths.root_dir}/data/
  log_dir: ${paths.root_dir}/logs/
  output_dir: ${hydra:runtime.output_dir}
  work_dir: ${hydra:runtime.cwd}
extras:
  ignore_warnings: false
  enforce_tags: true
  print_config: true
