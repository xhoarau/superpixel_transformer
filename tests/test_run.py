import pyrootutils
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))
# Hack importing pandas here to bypass some conflicts with hydra
import pandas as pd

from typing import List, Optional, Tuple

import hydra
import torch
import torch_geometric
import pytorch_lightning as pl
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import Callback, LightningDataModule, LightningModule, Trainer
from pytorch_lightning.loggers import Logger

from src import utils

from tqdm import tqdm

from tests.test_train import *

from src.data import NAG
from src.visualization.visualization import *
import os.path as osp

from src.metrics import *
# OmegaConf.register_new_resolver("eval", eval)


@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:
    
    cfg.datamodule.test_subset = cfg.datamodule.dataloader.batch_size
    cfg.datamodule.shuffle = False

    test_train_fast_dev_run_gpu(cfg)

if __name__ == "__main__":
    test()
