import pyrootutils
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

import torch
import torch.nn as nn
import torch_geometric
import time
import csv
import os.path as osp
import os
from src.data import NAG,Data
from src import utils
from src.nn.Local_GNN import Simple_Encoder
import random
import copy
from tqdm import tqdm
from time import time
from src.metrics.confusion_matrix import ConfusionMatrix
from torchmetrics import MaxMetric, MeanMetric, MinMetric

def print_result(result):
    if isinstance(result[0],list):
        for i in range(len(result)):
            print_result(result[i])
    else:
        print(result)

def reset_confusion_matrix(cm):
    if cm is not None:
        cm.confmat = torch.zeros(cm.confmat.shape)

def run_epoch(model,loss_func,optimizer,poly_set,batch_size,task="C",train=True,shuffle=False,cm=None):
    set_size = len(poly_set)
    n_batch = int(set_size/batch_size)
    poly = [0]*batch_size
    data = [0]*batch_size
    mean_loss = 0
    if shuffle:
        random.shuffle(poly_set)
    ecart_ensemble = [torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)]
    ecart_type = [
        [[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)],[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)]], 
        [[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)],[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)]], 
        [[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)],[torch.tensor(-1e100), torch.tensor(1e100), torch.tensor(0.)]]]
    compt_type = [[0,0],[0,0],[0,0]]
    soft = nn.Softmax(dim=1)
    for batch in range(n_batch):
        for i in range(batch_size):
            poly[i],data[i] = torch.load(osp.join(data_path,poly_set[i+batch]))

        optimizer.zero_grad()

        vertex, edges, index, line = utils.poly_to_tensor(poly)
        vertex, edges, index, max_dist = utils.normalise_poly(vertex,edges,index,angle=angle)
        
        out = model((vertex,edges,index))
        
        if task == "C":
            assert isinstance(loss_func,torch.nn.MSELoss)
            label = torch.tensor([poly[i].area/poly[i].convex_hull.area for i in range(len(poly))]).unsqueeze(1)
        if task == "cH":
            #assert isinstance(loss_func,torch.nn.MSELoss)
            #label = torch.tensor([data[i][0] for i in range(len(data))]).float().unsqueeze(1)
            assert isinstance(loss_func,torch.nn.CrossEntropyLoss)
            label = torch.tensor([data[i][0] for i in range(len(data))])
        if task == "cP":
            assert isinstance(loss_func,torch.nn.CrossEntropyLoss)
            label = torch.tensor([data[i][1] for i in range(len(data))])-1
        if task == "A":
            assert isinstance(loss_func,torch.nn.MSELoss)
            label = torch.tensor([poly[i].area/torch.pow(max_dist[i],2) for i in range(len(poly))]).unsqueeze(1)
        if task == "P":
            assert isinstance(loss_func,torch.nn.MSELoss)
            label = torch.tensor([poly[i].length/max_dist[i] for i in range(len(poly))]).unsqueeze(1)
        if task == "mR":
            assert isinstance(loss_func,torch.nn.CrossEntropyLoss)
            label = (torch.tensor([data[i][2] for i in range(len(data))])*4).floor().long()
        if task == "mS":
            assert isinstance(loss_func,torch.nn.CrossEntropyLoss)
            label = (torch.tensor([data[i][3] for i in range(len(data))])*4).floor().long()
        
        if cm is not None:
            #print(soft(out))
            #print(label,"\n")
            loss = loss_func(soft(out),label)
            cm[i].update(torch.argmax(out,dim=1),label)
        else:
            loss = loss_func(out,label)
            ecart = torch.abs((out-label)/label).detach()
            ecart_ensemble[0] = max(ecart_ensemble[0],torch.max(ecart))
            ecart_ensemble[1] = min(ecart_ensemble[1],torch.min(ecart))
            ecart_ensemble[2] = ecart_ensemble[2] + torch.sum(ecart)
            ecart_type[data[i][0]][data[i][1]-1][0] = max(ecart_type[data[i][0]][data[i][1]-1][0],torch.max(ecart))
            ecart_type[data[i][0]][data[i][1]-1][1] = min(ecart_type[data[i][0]][data[i][1]-1][1],torch.min(ecart))
            ecart_type[data[i][0]][data[i][1]-1][2] = ecart_type[data[i][0]][data[i][1]-1][2] + torch.sum(ecart)
            compt_type[data[i][0]][data[i][1]-1] = compt_type[data[i][0]][data[i][1]-1] + 1
        if hasattr(model[0],"recon_loss"): # TODO
            loss = loss + model[0].recon_loss(
        if train:
            loss.backward()
            """
            for p in model.parameters():
                print(p.grad)
            print(" ")"""
            optimizer.step()
        mean_loss = (mean_loss + loss).detach()
    mean_loss = mean_loss/n_batch
    ecart_ensemble[2] = ecart_ensemble[2]/set_size
    for i in range(3):
        for j in range(2):
            ecart_type[i][j][2] = ecart_type[i][j][2]/compt_type[i][j]
    if cm is not None:
        result= (cm.oa().tolist(),cm.miou().tolist(),cm.macc().tolist())
    else:
        result = (ecart_ensemble,ecart_type)

    return mean_loss,result

n_epoch = 20
scheduler_step = 20
batch_size = 5
n_validate = 2

angle = False
lattent_dim = 8
hidden_dim = 8
pooling_dim = 4
pool = 'att'
act_fn = "leakyrelu"
linear_dropout = 0.1
conv_layer_type = 'edgeconv'
num_linear_layers_mult = 2
agg = 'mean'
num_res_blocks = 2
residual_type = 'add'

data_path = "/root/polychain_dataset/"
with open(osp.join(data_path,"train.txt")) as f:
    train = sorted(list(set(map(lambda s:s[:-1]+".pt",f.readlines()))))
with open(osp.join(data_path,"val.txt")) as f:
    val = sorted(list(set(map(lambda s:s[:-1]+".pt",f.readlines()))))
with open(osp.join(data_path,"test.txt")) as f:
    test = sorted(list(set(map(lambda s:s[:-1]+".pt",f.readlines()))))

"""
task in
    C: measur convexity 
        poly.area / convex_hull.area
    cH: compt holes
    cP: compt pieces
    A: compute aire
    P: compute perimeter
    mR: measure regularity
    mS: measure spikiness
"""
task = "C"
if task in ["cH"]:
    out_dim = 3
    loss_func = torch.nn.CrossEntropyLoss() #weight=torch.tensor([1/218,1/170,1/112]))
    cm = ConfusionMatrix(out_dim)
if task in ["mR","mS"]:
    out_dim = 3
    loss_func = torch.nn.CrossEntropyLoss()
    cm = ConfusionMatrix(out_dim)
if task in ["cP"]:
    out_dim = 2
    loss_func = torch.nn.CrossEntropyLoss() #weight=torch.tensor([1/274,1/226]))
    cm = ConfusionMatrix(out_dim)
if task in ["C","A","P"]:
    out_dim = 1
    loss_func = torch.nn.MSELoss()
    cm = None 

if model_name = "simple_encoder":
    model = nn.Sequential(Simple_Encoder(num_features=int(2+angle), latent_dim=lattent_dim, hidden_dim=hidden_dim, pooling_dim=pooling_dim, 
        conv_layer_type=conv_layer_type, num_linear_layers_mult=num_linear_layers_mult, num_res_blocks=num_res_blocks, 
        residual_type=residual_type, pool=pool, act_fn=act_fn, linear_dropout=linear_dropout, agg=agg),nn.Linear(lattent_dim,out_dim))
elif model_name = "VGAE":
    model = nn.Sequential(VGAE_polygones(),nn.Linear(8,out_dim))
optimizer = torch.optim.SGD(model.parameters(), lr=.01, momentum=0.9)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer,scheduler_step,gamma=0.1)

mean_loss_train,result_train_start = run_epoch(model, loss_func, optimizer, train, 1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)
mean_loss_val,result_val_start = run_epoch(model, loss_func, optimizer, val, 1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)
mean_loss_test,result_test_start = run_epoch(model, loss_func, optimizer, test, 1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)

result=[[-1,mean_loss_train.item(),mean_loss_val.item(),mean_loss_val.item(),mean_loss_test.item()]]
model_save = copy.deepcopy(model)
for epoch in tqdm(range(n_epoch), ascii=True):
    mean_loss_train,_ = run_epoch(model, loss_func, optimizer, train, batch_size, task=task, train=True, shuffle=True, cm=cm)
    reset_confusion_matrix(cm)
    if epoch%n_validate == 0 or epoch == (n_epoch-1):
        mean_loss,_ = run_epoch(model, loss_func, optimizer, val, 1, task=task, train=False, cm=cm)
        reset_confusion_matrix(cm)
        if mean_loss<mean_loss_val:
            model_save = copy.deepcopy(model)
            mean_loss_val = mean_loss
    result.append([epoch,mean_loss_train.item(),mean_loss_val.item(),mean_loss.item()])
    scheduler.step()

mean_loss_train,result_train_end = run_epoch(model_save, loss_func, optimizer, train, 1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)
mean_loss_val,result_val_end = run_epoch(model_save, loss_func, optimizer, val, 1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)
mean_loss_test,result_test_end = run_epoch(model_save, loss_func, optimizer, test,1, task=task, train=False, cm=cm)
reset_confusion_matrix(cm)
result.append([epoch+1,mean_loss_train.item(),mean_loss_val.item(),mean_loss_val.item(),mean_loss_test.item()])

now = time()
with open(osp.join(f"result_{now}.csv"),'w',newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow([f"Result task {task}",now])
    writer.writerow(["epoch","train loss","best val loss","val loss","test_loss"])
    for r in result:
        writer.writerow(r)
        print(r)

print("\ntrain start :")
print_result(result_train_start)
print("train end :")
print_result(result_train_end)
print("\nval start :")
print_result(result_val_start)
print("val end :")
print_result(result_val_end)
print("\ntest start :")
print_result(result_test_start)
print("test end :")
print_result(result_test_end)


