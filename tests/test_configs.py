import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))
import os
os.chdir(root)

import hydra
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig, OmegaConf

from src.utils import rich_utils

OmegaConf.register_new_resolver("eval", eval)

def print_transform(transform):
    if transform.__class__.__name__ == 'Compose':
        transform = transform.transforms
    for t in transform:
        print(t.__class__.__name__)
        for k in t.__dict__:
            print("   ",k," : ",t.__dict__[k])
        print("\n")

def test_train_config(cfg_train: DictConfig, test_datamodule=False, test_model=False, test_trainer=False, 
        affiche_config_file=False, affiche_transforms=False, affiche_model=False, affiche_trainer=False):
    assert cfg_train
    assert not test_datamodule or cfg_train.datamodule
    assert not test_model or cfg_train.model
    assert not test_trainer or cfg_train.trainer

    HydraConfig().set_config(cfg_train)
    if affiche_config_file:
        rich_utils.print_config_tree(cfg_train, resolve=False, save_to_file=False)

    if test_datamodule:
        datamodule = hydra.utils.instantiate(cfg_train.datamodule)
        if affiche_transforms:
            print("pre transform : \n")
            print_transform(datamodule.pre_transform)
            print("\n")
            print("train transform : \n")
            print_transform(datamodule.on_device_train_transform)
            print("\n")
            print("val transform : \n")
            print_transform(datamodule.on_device_val_transform)
            print("\n")
            print("test transform : \n")
            print_transform(datamodule.on_device_test_transform)
            print("\n")
        del datamodule
    
    if test_model:
        model = hydra.utils.instantiate(cfg_train.model)
        if affiche_model:
            print(model)
            compt = 0
            for p in model.parameters():
                compt += p.numel()
            print("\n\nNb Parameters : ",compt)
        del model
    
    if test_trainer:
        trainer = hydra.utils.instantiate(cfg_train.trainer)
        if affiche_trainer:
            print("callbacks : ")
            for item in trainer.callbacks:
                print("   ",item.__class__.__name__)
            print("eval every ",trainer.check_val_every_n_epoch," epoch")
            if trainer.accelerator.is_available:
                print(trainer.accelerator.__class__.__name__," is available")
            else:
                print(trainer.accelerator.__class__.__name__," is not available")
            print("training between : ",trainer.max_epochs," and ",trainer.min_epochs," epochs")
        del trainer

"""
def test_eval_config(cfg_eval: DictConfig, test_datamodule=False, test_model=False, test_trainer=False, 
        affiche_config_file=False, affiche_transforms=False, affiche_model=False, affiche_trainer=False):
    assert cfg_eval
    assert not test_datamodule or cfg_eval.datamodule
    assert not test_model or cfg_eval.model
    assert not test_trainer or cfg_eval.trainer

    #HydraConfig().set_config(cfg_eval)

    if test_datamodule:
        datamodule = hydra.utils.instantiate(cfg_eval.datamodule)
        if affiche_transforms:
            print("pre transform : \n")
            print_transform(datamodule.pre_transform)
            print("\n")
            print("train transform : \n")
            print_transform(datamodule.on_device_train_transform)
            print("\n")
            print("val transform : \n")
            print_transform(datamodule.on_device_val_transform)
            print("\n")
            print("test transform : \n")
            print_transform(datamodule.on_device_test_transform)
            print("\n")
        del datamodule
    if test_model:
        model =  hydra.utils.instantiate(cfg_eval.model)
        if affiche_model:
            print(model)
            compt = 0
            for p in model.parameters():
                compt += p.numel()
            print("\n\nNb Parameters : ",compt)
        del model

    if test_trainer:
        HydraConfig().set_config(cfg_eval)
        trainer = hydra.utils.instantiate(cfg_eval.trainer)
        if affiche_trainer:
            print("callbacks : ")
            for item in trainer.callbacks:
                print("   ",item.__class__.__name__)
            print("eval every ",trainer.check_val_every_n_epoch," epoch")
            if trainer.accelerator.is_available:
                print(trainer.accelerator.__class__.__name__," is available")
            else:
                print(trainer.accelerator.__class__.__name__," is not available")
            print("training between : ",trainer.max_epochs," and ",trainer.min_epochs," epochs")
        del trainer
"""
def main():
    Train = True
    Eval = False
    test_datamodule = False
    test_model = False
    test_trainer = False
    affiche_config_file = True
    affiche_transforms = False
    affiche_model = False
    affiche_trainer = False

    if Train:
        with hydra.initialize(version_base=None, config_path="../configs", job_name="test_app"):
            cfg = hydra.compose(config_name="train.yaml",return_hydra_config=True)
        
        test_train_config(cfg, test_datamodule, test_model, test_trainer, affiche_config_file, affiche_transforms, affiche_model, affiche_trainer)

    if Eval:
        with hydra.initialize(version_base=None, config_path="../configs", job_name="test_app"):
            cfg = hydra.compose(config_name="eval.yaml",return_hydra_config=True)
        
        test_train_config(cfg, test_datamodule, test_model, test_trainer, affiche_config_file, affiche_transforms, affiche_model, affiche_trainer)



if __name__ == "__main__":
    main()
