import pyrootutils
 
root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

import hydra
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig, OmegaConf, open_dict
import yaml
from src.utils import rich_utils
import os
import os.path as osp

OmegaConf.register_new_resolver("eval", eval)

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def main(cfg: DictConfig):
    rich_utils.print_config_tree(cfg, resolve=True, save_to_file=True)

    datamodule = hydra.utils.instantiate(cfg.datamodule)
    datamodule.setup()
    f = open(osp.join(cfg.paths.output_dir,"datamodule.txt"),"w")
    print(datamodule,file=f)
    print(datamodule.train_dataset,file=f)
    f.close()

    model = hydra.utils.instantiate(cfg.model)
    f = open(osp.join(cfg.paths.output_dir,"model.txt"),"w")
    print(model,file=f)
    compt = 0
    for p in model.parameters():
        compt += p.numel()
    print("\n\nNb Parameters : ",compt,file=f)
    f.close()
    
    print("dataset : ",datamodule)
    print("model size : ",compt)

    print(f"all configuration information in {cfg.paths.output_dir}")


if __name__ == "__main__":
    main()


#with hydra.initialize(version_base=None, config_path="configs/", job_name="test_app"):
#    cfg = hydra.compose(config_name="train.yaml",return_hydra_config=True)

#HydraConfig().set_config(cfg)
#rich_utils.print_config_tree(cfg, resolve=False, save_to_file=False)
