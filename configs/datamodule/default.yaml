defaults:
  - _features.yaml

_target_: null

data_dir: ${paths.data_dir}

# Number of classes must be specified to help instantiating the model.
# Concretely, num_classes here will be passed to the DataModule and the
# Dataset upon hydra.utils.instantiate. But it will likely be ignored by
# those. Specifying num_classes in the data config actually allows the
# model config to capture it and assign the proper model output size by
# config interpolation
num_classes: ???

# I/O parameters
save_y_to_csr: True # save 'y' label histograms using a custom CSR format to save memory and I/O time
save_pos_dtype: 'float32'  # dtype to which 'pos' will be saved on disk
save_fp_dtype: 'float16'  # dtype to which all other floating point tensors will be saved to disk
compress_files: False # Whether to compress nag files on the disk, slow down preprocessing and loading but save disk memory
in_memory: False
pack_for_transfert: True # speed up transfert by transforming the batch into a tensor (in collate_fn) and rebuild the batch on the GPU
crop: False # select between crop and extend
resize_in_worker: True # crop or extend all images in collate_fn, increase minimum needed worker
batching_in_worker: True # create the batch in collate_fn, see NAGBatch.from_nag_list, increase minimum needed worker, enforce resize_in_worker
transform_in_worker: False # do the on_device_transforms in collate_fn, done on CPU, greatly increase needed workers, need batching_in_worker=True 

# Disk memory
# Set lite_preprocessing to only preprocess and save to disk features
# strictly needed for training, to save disk memory. If False, all
# supported pixel and segment features will be computed. This can be useful
# if you are experimenting with various feature combinations and do not
# want preprocessing to start over whenever testing a new combination
# If True, lite_preprocessing alleviate disk memory use and makes I/O
# faster, hence faster training and inference
lite_preprocessing: True

# GPU memory
# The following parameters are not the only ones affecting GPU memory.
# Several strategies can be deployed to mitigate memory impact, from
# batch construction to architecture size. However, these are good
# safeguard settings as a last resort to prevent our base model from OOM
# a 32G GPU at training time. May be adapted to other GPUs, models and
# training procedures
max_num_nodes: 50000
max_num_edges: 1000000

# Transforms
pre_transform: null
train_transform: null
val_transform: null
test_transform: null
on_device_train_transform: null
on_device_val_transform: null
on_device_test_transform: null

use_diameter: False # whether features should include node handcrafted features (after optional node_mlp, if features are actually loaded by the datamodule)
use_diameter_parent: False # whether features should include the superpixel's diameter (from sphere-normalization)

# Test-time augmentation
tta_runs: null
tta_val: False

# Produce submission data if test is true
submit: False

# set size on witch the test will be done (change value in function)
test_subset: null

# shuffle train dataset during testing of the code (change value in function)
shuffle: null

# Time measure
measure_time: ${measure_time}

# limit loaded data to the model needs
load_low: ${eval:'int(${model.net.nano})'}
load_high: ${eval:'int(len(${model.net.down_dim}))'}

# general parameters
_drop_nodes: 0.05
_drop_edges: 0.1

# DataLoader parameters. Would be good to have them live in another file
dataloader:
    batch_size: 4 # can be changefor each dataset
    num_workers: 10 # to be selected with respect to your computing power, see tests/worker_select.py to find best configuration. recommended 10
    pin_memory: True 
    persistent_workers: True
    prefetch_factor: 1
