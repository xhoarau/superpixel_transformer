# Default version of voronoi preprocessing
## Voronoi datasets is a very simple synthetic dataset with only patch of colors in the image

defaults:
  - default.yaml

_target_: src.datamodules.Synthetic.SyntheticDataModule

# custom_hash; used to enforce reuse of preprocess data (in case on device changement for exemple)
#custom_hash: 494ab74fdc0d6f69718227790fb22f55

dataloader:
    batch_size: 1

# These parameters are not actually used by the DataModule, but are used
# here to facilitate model parameterization with config interpolation
num_classes: 5
nb_image: 100
frac_train: 0.7
frac_val: 0.2
dataset_num: 0
img_dir: "images_noise"
label_dir: "masks"

trainval: False
in_memory: False # set to True to load all processed PascalVOC in RAM. This may accelerate training but CAREFUL not to modify the data in place !

# Features that will be computed, saved, loaded for pixels and segments

# pixel features used for the partition
partition_hf:
    - 'lab'

# pixel features used for training 
pixel_hf: 
    - 'rgb'

# segment-wise features computed at preprocessing
segment_base_hf: 
    - 'global_mean_size'
    - 'graph_size'
    - 'node_size'
    - 'perimeter'
    - 'pca_ratio'
    - 'pca_dir'
    
# segment features computed as the mean of pixel feature in each
# segment, saved with "mean_" prefix
segment_mean_hf:
    - 'rgb'

# segment features computed as the std of pixel feature in each segment,
# saved with "std_" prefix
segment_std_hf:
    - 'rgb'

# horizontal edge features used for training
edge_hf:
    - 'interface_length'
    - 'same_SP'
    - 'centroid_dir'
    - 'centroid_dist'

v_edge_hf: []  # vertical edge features used for training ## not implemented for images

# Shape interface parameters
straight_line_tol: 1
polyline_hf: # polyline features (shape and interface encoding), pos is used by default ### angle is he onlyone implemented
    - 'angle'
    - 'norm_angle'
    - 'norm_dist'

# Parameters declared here to facilitate tuning configs without copying
# all the pre_transforms

# Based on SPG: https://arxiv.org/pdf/1711.09869.pdf
pcp_regularization: [5e-2, 5e-2]
pcp_spatial_weight: [1e-1, 1e-2] # the larger, the more important the position is
pcp_feature_weight: ${eval:'[1e-1, 1., 1.]'} # + [1.] * (${datamodule.num_classes}+1)'} # float or list of lenght features (rgb = 3, intencity = 1, lab=3)
pcp_cutoff: [10,30]
pcp_iterations: 15
#pcp_dist: [0]
pcp_connectivity: 1
#pcp_size_modulation: true

# number of hops to take into account from the Cut-Pursuit graph 
nb_hops: 2 # 1 takes the original graph, n takes 1 and n but not all intermediary, to take multiple hops put a list like [1,2,3]

# Preprocessing
pre_transform:
    - transform: DataTo
      params:
        device: 'cpu'
    - transform: PixelFeatures
      params:
        keys: ${datamodule.pixel_hf_preprocess}
        n_classe: ${datamodule.num_classes}
    - transform: AdjacencyGraph
      params:
        connectivity: ${datamodule.pcp_connectivity}
    - transform: AddKeysTo  # move some features to 'x' to be used for partition
      params:
        keys: ${datamodule.partition_hf}
        to: 'x'
        delete_after: False
    - transform: CutPursuitPartition
      params:
        regularization: ${datamodule.pcp_regularization}
        spatial_weight: ${datamodule.pcp_spatial_weight}
        feature_weight: ${datamodule.pcp_feature_weight}
        cutoff: ${datamodule.pcp_cutoff}
        iterations: ${datamodule.pcp_iterations}
        parallel: False
        verbose: False
        #size_modulation: ${datamodule.pcp_size_modulation}
    - transform: NAGRemoveKeys  # remove 'x' used for partition (features are still preserved under their respective Data attributes)
      params:
        level: 'all'
        keys: 'x'
    - transform: NAGAddHopEdges
      params:
        nb_hops: ${datamodule.nb_hops}
    #- transform: ShapeInterfacePolygones
    #  params:
    #    straight_line_tol: ${datamodule.straight_line_tol} # tolerance for shape aproximation (cf. multilabel_potrace https://gitlab.com/1a7r0ch3/multilabel-potrace)
    #    polyline_hf: ${datamodule.polyline_hf}
    #    use_local_image_feature: ${use_local_image_feature}
    - transform: NAGBoundingBox
      params:
        level: 'all'
    - transform: Diameter
    - transform: NAGTo
      params:
        device: ${eval:'"cuda:" + str(${device_id})'} 
    - transform: SegmentFeatures # also compute edge features 
      params:
        keys: ${datamodule.segment_base_hf_preprocess}
        mean_keys: ${datamodule.segment_mean_hf_preprocess}
        std_keys: ${datamodule.segment_std_hf_preprocess}
        edge_keys: ${datamodule.edge_hf}
        strict: False  # will not raise error if a mean or std key is missing
        num_classes: ${datamodule.num_classes} # compute class weight here
        topology_keys: ${eval:'ListConfig(${datamodule.topological_keys}) if ${topological_attention} else None'}
    - transform: NAGTo
      params:
        device: 'cpu'

process_shape_interface: ${eval:'any([i["transform"] == "ShapeInterfacePolygones" for i in ${datamodule.pre_transform}])'}

# CPU-based train transforms
train_transform: null

# CPU-based val transforms
val_transform: ${datamodule.train_transform}

# CPU-based test transforms
test_transform: ${datamodule.val_transform}

# GPU-based train transforms
on_device_train_transform:

    # Cast all attributes to either float or long. Doing this only now
    # allows speeding up disk I/O and CPU->GPU transfer
    - transform: NAGCast

    - transform: NAGAdjacencyGraph # recompute level 0 graph (not saved to save memory)
      params:
        connectivity: ${datamodule.pcp_connectivity}

    - transform: DropNodes
      params:
        p: ${datamodule._drop_nodes}
        by_size: true

    - transform: DropEdges # check correction
      params:
        p: ${datamodule._drop_edges}

    # Add a `node_size` attribute to all segments, this is needed for
    # segment-wise position normalization with UnitSphereNorm
    # and to use it as a segment feature
    - transform: NodeSize

    - transform: NAGColorAutoContrast
      params:
        p: 0.2
        level: 'all'

    # Compute some node features and horizontal and vertical edges on-the-fly. Those are
    # only computed now since they can be deduced from pixel and node
    # attributes, we also need to duplicate some precomputed one.
    # Besides, the OnTheFlyHorizontalEdgeFeatures transform
    # takes a trimmed graph as input and doubles its size, creating j->i
    # for each input i->j edge
    - transform: OnTheFlyNodeFeatures
      params:
        keys: ${datamodule.segment_base_hf}
        image_keys: ${datamodule.pixel_hf}
    - transform: OnTheFlyHorizontalEdgeFeatures
      params:
        keys: ${datamodule.edge_hf}
        topology_keys: ${eval:'ListConfig(${datamodule.topological_keys}) if ${topological_attention} else None'}

    # Add self-loops in the horizontal graph
    - transform: NAGAddSelfLoops
      params:
        keys: ${datamodule.edge_hf}

    - transform: RandomRotateImage
      params:
        theta: 180
        image: True
        graph: True
        as_image: False

    - transform: RandomAxisFlip
      params:
        axis: 0
        p: 0.5
        image: True
        graph: True

    - transform: RandomAxisFlip
      params:
        axis: 1
        p: 0.5
        image: True
        graph: True

    # Move all pixel and segment features to 'x'
    - transform: NAGAddKeysTo
      params:
        level: 0
        keys: ${eval:'ListConfig(["image"])'}
        to: 'x'
    - transform: NAGAddKeysTo
      params:
        level: '1+'
        keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf}])'}
        to: 'x'

    - transform: NAGJitterKey
      params:
        level: 'all'
        key: 'x'
        sigma: 0.01
        trunc: 0.05

    - transform: NAGJitterKey
      params:
        level: '1+'
        key: 'edge_attr'
        sigma: 0.01
        trunc: 0.05

# GPU-based val transforms
on_device_val_transform:

    # Cast all attributes to either float or long. Doing this only now
    # allows speeding up disk I/O and CPU->GPU transfer
    - transform: NAGCast
    
    - transform: NAGAdjacencyGraph # recompute level 0 graph (not saved to save memory)
      params:
        connectivity: ${datamodule.pcp_connectivity}

    # Add a `node_size` attribute to all segments, this is needed for
    # segment-wise position normalization with UnitSphereNorm
    - transform: NodeSize

    # Compute some node features and horizontal and vertical edges on-the-fly. Those are
    # only computed now since they can be deduced from pixel and node
    # attributes, we also need to duplicate some precomputed one.
    # Besides, the OnTheFlyHorizontalEdgeFeatures transform
    # takes a trimmed graph as input and doubles its size, creating j->i
    # for each input i->j edge
    - transform: OnTheFlyNodeFeatures
      params:
        keys: ${datamodule.segment_base_hf}
        image_keys: ${datamodule.pixel_hf}
    - transform: OnTheFlyHorizontalEdgeFeatures
      params:
        keys: ${datamodule.edge_hf}
        topology_keys: ${eval:'ListConfig(${datamodule.topological_keys}) if ${topological_attention} else None'}

    # Add self-loops in the horizontal graph
    - transform: NAGAddSelfLoops
      params:
        keys: ${datamodule.edge_hf}
    
    # Move all pixel and segment features to 'x'
    - transform: NAGAddKeysTo
      params:
        level: 0
        keys: ${eval:'ListConfig(["image"])'}
        to: 'x'
    - transform: NAGAddKeysTo
      params:
        level: '1+'
        keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf}])'}
        to: 'x'

# GPU-based test transforms
on_device_test_transform: ${datamodule.on_device_val_transform}
