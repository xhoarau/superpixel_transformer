import math
import torch
from torch_scatter import scatter_add, scatter_mean, scatter_min, scatter_max, scatter_sum
from itertools import combinations_with_replacement
from src.utils.edge import edge_wise_pixels
from torch_geometric.utils import coalesce
from torch_geometric.nn.pool.consecutive import consecutive_cluster


__all__ = [
    'scatter_covar','scatter_pca_2D','scatter_mean_weighted', 'scatter_pca', 'idx_preserving_mask']

def scatter_covar(a,b,index):
    size = torch.bincount(index)-1+1e-5
    centered_a = a-scatter_mean(a,index)[index]
    centered_b = b-scatter_mean(b,index)[index]
    return scatter_sum(centered_a*centered_b,index)/size

def scatter_pca_2D(x,index):
    assert x.dim() == 2 and x.shape[1] == 2
    assert index.dim() == 1 and index.shape[0] == x.shape[0]
    Vx = scatter_covar(x[:,0],x[:,0],index)
    Vy = scatter_covar(x[:,1],x[:,1],index)
    C = scatter_covar(x[:,0],x[:,1],index)
    Delta = (Vx+Vy).pow(2) - 4*(Vx*Vy-C.pow(2))
    Delta[Delta<0] = 0
    l1 = ((Vx+Vy)+Delta.sqrt())/2
    l2 = ((Vx+Vy)-Delta.sqrt())/2

    den = (Vx-l1)
    den[den==0] = 1e-15

    v2_l1 = -1/(torch.sqrt((((-C)/den).pow(2))+1))
    v1_l1 = v2_l1 * ((-C)/den)

    den = (Vx-l2)
    den[den==0] = 1e-15

    v2_l2 = -1/(torch.sqrt((((-C)/den).pow(2))+1))
    v1_l2 = v2_l2 * ((-C)/den)

    return torch.cat((l1.unsqueeze(1),l2.unsqueeze(1),v1_l1.unsqueeze(1),v2_l1.unsqueeze(1),v1_l2.unsqueeze(1),v2_l2.unsqueeze(1)),dim=1)

def scatter_mean_weighted(x, idx, w, dim_size=None):
    """Helper for scatter_mean with weights"""
    assert w.ge(0).all(), "Only positive weights are accepted"
    assert w.dim() == idx.dim() == 1, "w and idx should be 1D Tensors"
    assert x.shape[0] == w.shape[0] == idx.shape[0], \
        "Only supports weighted mean along the first dimension"

    # Concatenate w and x in the same tensor to only call scatter once
    w = w.view(-1, 1).float()
    wx = torch.cat((w, x * w), dim=1)

    # Scatter sum the wx tensor to obtain
    wx_segment = scatter_add(wx, idx, dim=0, dim_size=dim_size)

    # Extract the weighted mean from the result
    w_segment = wx_segment[:, 0]
    x_segment = wx_segment[:, 1:]
    w_segment[w_segment == 0] = 1
    mean_segment = x_segment / w_segment.view(-1, 1)

    return mean_segment


def scatter_pca(x, idx, on_cpu=True):
    """Scatter implementation for PCA.

    Returns eigenvalues and eigenvectors for each group in idx.
    If x has shape N1xD and idx covers indices in [0, N2], the
    eigenvalues will have shape N2xD and the eigenvectors will
    have shape N2xDxD. The eigenvalues and eigenvectors are
    sorted by increasing eigenvalue.
    """
    assert idx.dim() == 1
    assert x.dim() == 2
    assert idx.shape[0] == x.shape[0]
    assert x.shape[1] > 1

    d = x.shape[1]
    device = x.device

    # Substract mean
    mean = scatter_mean(x, idx, dim=0)
    x = x - mean[idx]

    # Compute pixelwise covariance as a N_1x(DxD) matrix
    ij = torch.tensor(list(combinations_with_replacement(range(d), 2)), device=device)
    upper_triangle = x[:, ij[:, 0]] * x[:, ij[:, 1]]

    # Aggregate the covariances as a N_2x(DxD) with scatter_sum
    # and convert it to a N_2xDxD batch of matrices
    upper_triangle = scatter_add(upper_triangle, idx, dim=0) / d
    cov = torch.empty((upper_triangle.shape[0], d, d), device=device)
    cov[:, ij[:, 0], ij[:, 1]] = upper_triangle

    # Eigendecompostion
    if on_cpu:
        device = cov.device
        cov = cov.cpu()
        eval, evec = torch.linalg.eigh(cov, UPLO='U')
        eval = eval.to(device)
        evec = evec.to(device)
    else:
        eval, evec = torch.linalg.eigh(cov, UPLO='U')

    # If Nan values are computed, return equal eigenvalues and
    # Identity eigenvectors
    idx_nan = torch.where(torch.logical_and(
        eval.isnan().any(1), evec.flatten(1).isnan().any(1)))
    eval[idx_nan] = torch.ones(2, dtype=eval.dtype, device=device)
    evec[idx_nan] = torch.eye(2, dtype=evec.dtype, device=device)

    # Precision errors may cause close-to-zero eigenvalues to be
    # negative. Hard-code these to zero
    eval[torch.where(eval < 0)] = 0

    return eval, evec


def idx_preserving_mask(mask, idx, dim=0):
    """Helper to pass a boolean mask and an index, to make sure indexing
    using the mask will not entirely discard all elements of index.
    """
    is_empty = scatter_add(mask.float(), idx, dim=dim) == 0
    return mask | is_empty[idx]

