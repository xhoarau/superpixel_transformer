import torch
from torch_scatter import scatter_mean
from torch.profiler import record_function

import warnings

__all__ = ['crop_nag_list','crop_nag','extend_nag_list','extend_nag','image_to_feature',"label_to_y"]

def crop_nag_list(nag_list):
    """ crop all base image to the same size for batch creation """
    start_len = len(nag_list)
    device = nag_list[0].device
    if hasattr(nag_list[0][0],'image'):
        with record_function("crop dim"):
            dim_min = torch.tensor(nag_list[0][0].image.shape[1:],device=device)
            for n in nag_list[1:]:
                d = n[0]
                dim_min = torch.minimum(dim_min,torch.tensor(d.image.shape[1:],device=device))
        with record_function("crop loop"):
            list_pop = []
            for i in range(len(nag_list)):
                nag_list[i] = crop_nag(nag_list[i],dim_min)
                if nag_list[i][-1]["edge_index"].shape[1] == 0:
                    list_pop.append(i-len(list_pop))
            for i in list_pop:
                nag_list.pop(i) # remove images with no edges a top level (induce bug in later computations)
                # only occure if crop keeps 1 or 0 node
    if start_len-len(nag_list) > 2:
        warnings.warn(f"{start_len-len(nag_list)} images has been removed from a batch, if it occure too often change superpixel parameters to smaller ones")
    return nag_list
            
def crop_nag(nag,dim):
    """ crop the base image to the given dimentions and remove all superpixels nor completly included in the crop
    
    assume BoundingBox at top level

    :param nag: NAG object with an image in level 0
    :param dim: dimention of the crop
    
    work in progress, bounding box
    """
    device = nag.device
    if hasattr(nag[0],'image'): 
        img_size = torch.tensor(nag[0].image.shape[1:],device=device)
        if torch.any(img_size!=dim):
            
            with record_function("sp index"):
                coord = (torch.rand(2,device=device)*(img_size-dim)).long()
                selected_SP = torch.logical_and(torch.prod((nag[2].bounding_box * img_size.max()).long()[:,:2]>coord.flip(0),dim=1),
                    torch.prod((nag[2].bounding_box * img_size.max()).long()[:,2:]<(coord+dim).flip(0),dim=1))
            with record_function("index mask"):
                idx = selected_SP.nonzero().squeeze() # select all super_pixel of max level with all pixels in the crop
                image_index = torch.zeros((*dim),device=device,dtype=torch.bool)
                image_index[:,coord[0]:coord[0]+dim[0],coord[1]:coord[1]+dim[1]]=True
                nag[0].image_index = image_index.flatten().nonzero().squeeze()
            with record_function("crop img"):
                nag[0].image = nag[0].image[:,coord[0]:coord[0]+dim[0],coord[1]:coord[1]+dim[1]]
            with record_function("crop label"):
                if hasattr(nag[0],'label'):
                    nag[0].label = nag[0].label[coord[0]:coord[0]+dim[0],coord[1]:coord[1]+dim[1]]
            with record_function("select sp"):
                if idx.numel() == 1:
                    idx = idx.unsqueeze(0)
                nag = nag.select(nag.num_levels-1,idx)
        else:
            nag[0].image_index = torch.arange(nag[0].num_pixels,device=device,dtype=torch.long)
        nag[0].img_size=dim

    return nag

def extend_nag_list(nag_list,label_value=0):
    """ etand all base image to the same size for batch creation 
        opposit to crop (all image are the size of the bigest)"""
    device = nag_list[0].device
    if hasattr(nag_list[0][0],'image'):
        with record_function("extend dim"):
            dim_max = torch.tensor(nag_list[0][0].image.shape[1:],device=device)
            for n in nag_list[1:]:
                d = n[0]
                dim_max = torch.maximum(dim_max,torch.tensor(d.image.shape[1:],device=device))
        with record_function("extend loop"):
            for i in range(len(nag_list)):
                nag_list[i] = extend_nag(nag_list[i],dim_max)
    return nag_list

def extend_nag(nag,dim,label_value=0):
    """ crop the base image to the given dimentions and remove all superpixels nor completly included in the crop
    
    assume BoundingBox at top level

    :param nag: NAG object with an image in level 0
    :param dim: dimention of the crop
    
    work in progress, bounding box
    """
    device = nag.device
    if hasattr(nag[0],'image'): 
        img_size = torch.tensor(nag[0].image.shape[1:],device=device)
        if torch.any(img_size!=dim): 
            with record_function("index mask"):
                index = torch.ones(*img_size,dtype=torch.bool,device=device)
                nag[0].image_index = torch.nn.functional.pad(input=index,pad=(0,dim[1]-nag[0].image.shape[2],0,dim[0]-nag[0].image.shape[1])).flatten().nonzero().squeeze()
            with record_function("extend img"):
                nag[0].image = torch.nn.functional.pad(input=nag[0].image,pad=(0,dim[1]-nag[0].image.shape[2],0,dim[0]-nag[0].image.shape[1]))
            with record_function("crop label"):
                if hasattr(nag[0],'label'):
                    nag[0].label = torch.nn.functional.pad(input=nag[0].label,pad=(0,dim[1]-nag[0].label.shape[1],0,dim[0]-nag[0].label.shape[0]),value=label_value)
        else:
            nag[0].image_index = torch.arange(nag[0].num_pixels,device=device,dtype=torch.long)
        nag[0].img_size=dim

    return nag

    
        
def image_to_feature(image,feature_dim=None,image_index=None,rot=0,flip=None):
    """creat a feature tensor in graph format from an image format tensor

    :param image: image to flatten
    :param feature_dim: index of features to keep 
        in case of multiple features (rgb + infrared + elevation ...)
    :param image_index: index of pixel to keep in crop or drop
    """
    
    if flip is not None:
        flip.reverse()
        for f in flip:
            image = image.flip(f+1)
        flip.reverse()

    if rot != 0:
        image = image.rot90(k=-rot,dims=[-2,-1])

    if image.dim() == 4:
        image = image.permute(0,2,3,1).flatten(0,2)
    else:
        image = image.flatten(-2).permute(1,0)
    
    if feature_dim is not None:
        image = image[:,feature_dim]
    
    if image_index is None:
        return image
    return image[image_index]

def label_to_y(label,num_classes):
    """ transform ground truth format from image with index to node with vector """
    y = torch.zeros([label.numel(),num_classes+1]).long()
    y[torch.arange(label.numel()),label.flatten().squeeze()] = 1
    return y
