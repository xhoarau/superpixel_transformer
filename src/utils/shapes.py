from torch_geometric.nn.pool.consecutive import consecutive_cluster
from src.utils.sparse import indices_to_pointers
from torch_geometric.utils import subgraph
import torch

from torch.profiler import record_function

__all__ = ['update_shapes']

def update_shapes(idx,shapes):
    """select the shapes to keep wrt idx
    """
    out = shapes.__class__() # create an empty data object, must be initialised juste after
    with record_function("isin"):
        exist = torch.zeros(shapes.batch_node.max()+1,dtype=torch.bool,device=shapes.device)
        exist[idx] = True
        mask = exist[shapes.batch_node]
    with record_function("subgraph"):
        out.edge_index = subgraph(mask,shapes.edge_index,relabel_nodes=True)[0]
    with record_function("consecutive_cluster"):
        out.batch_node = consecutive_cluster(shapes.batch_node[mask])[0]
    with record_function("other key"):
        for key in shapes.keys:
            if key not in ['edge_index','batch_node','ptr']:
                out[key] = shapes[key][mask]
    if hasattr(shapes,'ptr'):
        out.ptr = indices_to_pointers(out.batch)[0]
    return out
    

