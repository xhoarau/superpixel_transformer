from collections.abc import Iterable


__all__ = [
    'PIXEL_FEATURES', 'SEGMENT_BASE_FEATURES', 'SUBEDGE_FEATURES',
    'ON_THE_FLY_HORIZONTAL_FEATURES', 'ON_THE_FLY_VERTICAL_FEATURES',
    'LEARNED_FEATURES','sanitize_keys', 'list_unique_batch_keys']


# tke this list if no feature in config file
PIXEL_FEATURES = [
    'rgb']

SEGMENT_BASE_FEATURES = [
    'size']

SUBEDGE_FEATURES = []

ON_THE_FLY_HORIZONTAL_FEATURES = [
    'e_size',
    'e_perimeter',
    'centroid_dir',
    'centroid_dist',
    'same_SP',
    'interface_lenght']

ON_THE_FLY_VERTICAL_FEATURES = [
    'centroid_dist',
    'log_size']

LEARNED_FEATURES = [
    'shape_embedind',
    'interface_embeding']

UNIQUE_BATCH = [
    'node_pack_lists',
    'edge_pack_lists']

def list_unique_batch_keys(keys):
    """ return the list of keys the must stay identical in batch """
    unique_keys = []
    for k in keys:
        if k in UNIQUE_BATCH or k.endswith('_dim'):
            unique_keys.append(k)
    return unique_keys
    


def sanitize_keys(keys, default=[]):
    """Sanitize an iterable of string key into a sorted list of unique
    keys. This is necessary for consistently hashing key list arguments
    of some transforms.
    """
    # Convert to list of keys
    if isinstance(keys, str):
        out = [keys]
    elif isinstance(keys, Iterable):
        out = list(keys)
    else:
        out = list(default)

    assert all(isinstance(x, str) for x in out), \
        f"Input 'keys' must be a string or an iterable of strings, but some " \
        f"non-string elements were found in '{keys}'"

    # Remove duplicates and sort elements
    out = tuple(sorted(list(set(out))))

    return out
