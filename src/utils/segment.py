import numpy as np
import torch
from tqdm import tqdm
from scipy.cluster.hierarchy import DisjointSet
from time import time

__all__ = ["felzenswalb_segmentation"]

class felzenswalb_DisjointSet(DisjointSet):
    def __init__(self,elements=None):
        self._Int = {}
        super().__init__(elements)

    def subset_size(self,x):
        return self._sizes[self[x]]

    def subset_Int(self,x):
        return self._Int[self[x]]

    def add(self,x):
        super().add(x)
        self._Int[x] = 0;

    def merge(self,x,y,Int):
        if super().merge(x,y):
            self._Int[self[x]] = Int
            return True
        return False


def felzenswalb_segmentation(k,x,edge_index,diff="L2",edge_attr=None):
    """ compute a segmentation on an undirected graph
    
    f refer to the dimentions of the node features
    N refer to the number of nodes
    E refer to the number of edges
    
    :param k: float or int
        FELZENSZWALB and HUTTENLOCHER algorithm k parameter
        https://link.springer.com/article/10.1023/B:VISI.0000022288.19776.77v
    :param x: tensor (N,f)
        node features
    :param edge_index: tensor (2,E)
        edge define a pair of node index
    :param diff: str
        norm to use as a difference operator for edge weight
        choose in : L2 or L1
    :param edge_attr: optional tensor (E)
        edge weight scale
    """
    
    if diff == "L2":
        distances = ((x[edge_index[0]]-x[edge_index[1]])**2).sum(1)**(1/2)
    else:
        if diff == "L1":
            distances = torch.abs(x[edge_index[0]]-x[edge_index[1]]).sum(1)
        else:
            raise NotImplementedError(f"diff {diff} not implemented, choose in L2 or L1")
    
    if edge_attr is not None:
        assert edge_index.shape[1] == edge_attr.shape[0], "inconsitent number of edges"
        distances = distances * edge_attr

    distances,reorder = distances.sort()
    edges = edge_index[:,reorder]
    assignement = felzenswalb_DisjointSet(np.arange(x.shape[0]))
    #Int = [0 for _ in range(x.shape[0])]
    #size = [1 for _ in range(x.shape[0])]
    idx_time = 0.
    MInt_time = 0.
    merge_time = 0.
    for i in tqdm(range(edges.shape[1]), ascii=True):
        start = time()
        n1 = assignement[edges[0,i].item()]
        n2 = assignement[edges[1,i].item()]
        idx_time += time()-start
        if n1 != n2:
            start = time()
            MInt = min(assignement._Int[n1]+k/assignement._sizes[n1],assignement._Int[n2]+k/assignement._sizes[n2])
            MInt_time += time()-start
            if MInt > distances[i]:
                start = time()
                assignement.merge(n1,n2,distances[i])
                merge_time += time()-start

    print("idx_time : ",idx_time)
    print("MInt_time : ",MInt_time)
    print("merge_time : ",merge_time)


    #unique = torch.unique(assignement)
    #a = torch.empty(assignement.max()+1,dtype=assignement.dtype)
    #a[unique] = torch.arange(unique.numel())
    #assignement = a[assignement]

    return assignement
