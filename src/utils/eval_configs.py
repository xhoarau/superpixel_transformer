import os,sys

""" manual script
To be used on multirun train log directory to build multirun evaluation config files

exemple call : python3 src/utils/eval_configs.py multirun_train/ 5 configs/eval_version/

    argv description
1 : relative path to multirun train logs
2 : number of runs (asked for simplicity)
3 : relative path to save configs (without file_name) (exemple configs/eval_version/)
"""

multirun_name = sys.argv[1].split('/')
if multirun_name[-1] == '':
    multirun_name = multirun_name[-2]
else:
    multirun_name = multirun_name[0]

num_nested_file = sys.argv[3].split('/')
if num_nested_file[-1] == '':
    num_nested_file = len(num_nested_file) - 1
else:
    num_nested_file = len(num_nested_file)

names = []
for i in range(int(sys.argv[2])):
    new_config_path = os.path.join(sys.argv[3],multirun_name+"_"+str(i)+".yaml")
    names.append(multirun_name+"_"+str(i)+".yaml")
    for checkpoints_name in os.listdir(os.path.join(multirun_name,str(i),"checkpoints")):
        if "epoch" in checkpoints_name:
            break
    f = open(new_config_path,"w")

    print(f"# @package _global_\n\ndefaults:\n  - {'../'*num_nested_file}@_here_: {multirun_name}/{i}/csv/version_0/hparams.yaml\nckpt_path: {os.path.join(sys.argv[1],str(i),'checkpoints',checkpoints_name)}\ntask_name: 'eval'",file=f)
    f.close()
f = open(os.path.join(sys.argv[3],multirun_name+".txt"),"w")
for name in names:
    print(name+', ',file=f,end="")
