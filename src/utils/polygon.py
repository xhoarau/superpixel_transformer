import math, random
from typing import List, Tuple
import shapely as sy
import torch
from shapely.validation import make_valid
import matplotlib.pyplot as plt
from torch_scatter import scatter_mean, scatter_std, scatter_max

def generate_polygon(center: Tuple[float, float], avg_radius: float,
                     irregularity: float, spikiness: float,
                     num_vertices: int) -> List[Tuple[float, float]]:
    """
    Start with the center of the polygon at center, then creates the
    polygon by sampling points on a circle around the center.
    Random noise is added by varying the angular spacing between
    sequential points, and by varying the radial distance of each
    point from the centre.

    Args:
        center (Tuple[float, float]):
            a pair representing the center of the circumference used
            to generate the polygon.
        avg_radius (float):
            the average radius (distance of each generated vertex to
            the center of the circumference) used to generate points
            with a normal distribution.
        irregularity (float):
            variance of the spacing of the angles between consecutive
            vertices.
        spikiness (float):
            variance of the distance of each vertex to the center of
            the circumference.
        num_vertices (int):
            the number of vertices of the polygon.
    Returns:
        List[Tuple[float, float]]: list of vertices, in CCW order.
    """
    # Parameter check
    if irregularity < 0 or irregularity > 1:
        raise ValueError("Irregularity must be between 0 and 1.")
    if spikiness < 0 or spikiness > 1:
        raise ValueError("Spikiness must be between 0 and 1.")

    irregularity *= 2 * math.pi / num_vertices
    spikiness *= avg_radius
    angle_steps = random_angle_steps(num_vertices, irregularity)

    # now generate the points
    points = []
    angle = random.uniform(0, 2 * math.pi)
    for i in range(num_vertices):
        radius = clip(random.gauss(avg_radius, spikiness), 0, 2 * avg_radius)
        point = (center[0] + radius * math.cos(angle),
                 center[1] + radius * math.sin(angle))
        points.append(point)
        angle += angle_steps[i]

    return points

def random_angle_steps(steps: int, irregularity: float) -> List[float]:
    """Generates the division of a circumference in random angles.

    Args:
        steps (int):
            the number of angles to generate.
        irregularity (float):
            variance of the spacing of the angles between consecutive vertices.
    Returns:
        List[float]: the list of the random angles.
    """
    # generate n angle steps
    angles = []
    lower = (2 * math.pi / steps) - irregularity
    upper = (2 * math.pi / steps) + irregularity
    cumsum = 0
    for i in range(steps):
        angle = random.uniform(lower, upper)
        angles.append(angle)
        cumsum += angle

    # normalize the steps so that point 0 and point n+1 are the same
    cumsum /= (2 * math.pi)
    for i in range(steps):
        angles[i] /= cumsum
    return angles

def clip(value, lower, upper):
    """
    Given an interval, values outside the interval are clipped to the interval
    edges.
    """
    return min(upper, max(value, lower))

def polygon_random_points_in(poly, num_points):
    min_x, min_y, max_x, max_y = poly.bounds
    points = []
    compt = 0
    while len(points) < num_points:
        random_point = sy.Point([random.uniform(min_x, max_x), random.uniform(min_y, max_y)])
        if (random_point.within(poly)):
            points.append(random_point)
            compt = 0
        else:
            compt = compt + 1
        if compt == 20 : 
            return [-1]
    return points

def polygon_random_points_out(poly, num_points, min_x=-300, min_y=-300, max_x=300, max_y=300):
    points = []
    compt = 0
    while len(points) < num_points:
        random_point = sy.Point([random.uniform(min_x, max_x), random.uniform(min_y, max_y)])
        if not (random_point.within(poly)):
            points.append(random_point)
            compt = 0
        else:
            compt = compt + 1
        if compt == 20 : 
            return [-1]
    return points

def check_poly(poly):
    return all([isinstance(poly[i],sy.Polygon) or isinstance(poly[i],sy.MultiPolygon) for i in range(len(poly))])

def poly_to_tensor(poly):
    if isinstance(poly,list):
        assert check_poly(poly), "inputs are not all polygons"
    else:
        assert isinstance(poly,sy.Polygon) or isinstance(poly,sy.MultiPolygon), "input is not a polygon"
        poly = [poly]
    first = True
    for i in range(len(poly)):
        if isinstance(poly[i].boundary,sy.MultiLineString):
            vertices = list(poly[i].boundary.geoms)
        else:
            vertices = [poly[i].boundary]
        for j in range(len(vertices)):
            v = list(vertices[j].coords)[:-1]
            if first:
                vertex = torch.tensor(v)
                edges = torch.roll(torch.repeat_interleave(torch.arange(len(v)),2),-1).reshape(len(v),2)
                batch = torch.full((len(v),), i)
                lines = torch.full((len(v),), j)
                first = False
            else:
                vertex = torch.cat((vertex,torch.tensor(v)))
                edges = torch.cat((edges,torch.roll(torch.repeat_interleave(torch.arange(len(v)),2),-1).reshape(len(v),2)+(torch.max(edges)+1)))
                batch = torch.cat((batch,torch.full((len(v),), i)))
                lines = torch.cat((lines,torch.full((len(v),), j)))
    return vertex,edges.permute(1,0),batch,lines

def visu_poly(poly,path):
    assert isinstance(poly,sy.Polygon) or isinstance(poly,sy.MultiPolygon), "input is not a polygon"
    if isinstance(poly,sy.MultiPolygon):
        poly = list(poly.geoms)
    else:
        poly = [poly]

    for p in poly:
        if isinstance(p.boundary,sy.MultiLineString):
            lines = list(p.boundary.geoms)
        else:
            lines = [p.boundary]
        for l in lines:
            plt.plot(*l.xy)
    plt.axis("off") 
    plt.savefig(path)
    plt.close()

def normalise_poly(shape_vertex,shape_edges,shape_index,angle=False):
    """
        Normalize shapes
        each polygones or polylines is centered on it's centroïd, scaled in the unit sphere and rotate such that the farthest points from the center is on the abscissa axis
    """
    centers = scatter_mean(shape_vertex,shape_index,dim=0)
    shape_vertex = shape_vertex-centers[shape_index]

    distances = torch.sqrt(torch.sum(torch.pow(shape_vertex,2),dim=1))
    max_dist,max_dist_index = scatter_max(distances,shape_index)
    shape_vertex = shape_vertex/max_dist[shape_index].unsqueeze(1)

    shape_vertex = torch.stack((torch.sum(shape_vertex*shape_vertex[max_dist_index][shape_index], dim=1), 
        torch.sum(shape_vertex*(torch.flip(shape_vertex[max_dist_index][shape_index], (1,))*torch.tensor([-1,1])), dim=1)), dim=1)

    if angle:
        # Compute interiore angle of all vertices
        vecteurs = shape_vertex[shape_edges[1]]-shape_vertex[shape_edges[0]] # compute vectors of each edge
        vecteurs_norm = torch.sqrt(torch.sum(torch.square(vecteurs),dim=1)).unsqueeze(1)

        # remove vector with norm null
        index_null = (vecteurs_norm==0).nonzero(as_tuple=True)[0] # index of null vector and unnecesary edges
        index_node = shape_edges[0,index_null] # index of duplicate nodes
        index = torch.ones(shape_vertex.shape[0], dtype=bool)
        index[index_node] = False
        shape_vertex = shape_vertex[index] # select all non duplicate nodes
        shape_index = shape_index[index]
        index = torch.ones(shape_edges.shape[1], dtype=bool)
        index[index_null] = False
        shape_edges = shape_edges[:,index] # select all necesary edge
        vecteurs = vecteurs[index]
        vecteurs_norm = vecteurs_norm[index]
        shape_edges = shape_edges - torch.sum(shape_edges>index_node.repeat_interleave(
            shape_edges.numel()).reshape((index_node.numel(),*shape_edges.shape)),dim=0) # update node index in edges
        vecteurs = vecteurs/vecteurs_norm

        vect_s = torch.zeros(vecteurs.shape)
        vect_t = torch.zeros(vecteurs.shape)
        vect_s[shape_edges[0]] = vecteurs # for each vertices, keep the vector of witch it is the source
        vect_t[shape_edges[1]] = -vecteurs # for each vertices, keep the vector of witch it is the target

        shape_angle = torch.arccos(torch.clamp(torch.sum(vect_s * vect_t,dim=1),min=-1,max=1))
            # compute angle for all points (in [0,pi]), clamp for float imprecision that cause nan values
        sin = torch.sum((vect_s * torch.flip(vect_t,(1,)))*torch.tensor([1,-1]),dim=1).squeeze() # check if angles in [pi,2*pi] (sign of sinus)
        shape_angle[sin<0] = (2*torch.pi)-shape_angle[sin<0] # change angles in [pi,2*pi]

        # add angles to vertices features
        shape_vertex = torch.cat((shape_vertex,shape_angle.unsqueeze(1)),dim=1)
    return shape_vertex, shape_edges, shape_index, max_dist
