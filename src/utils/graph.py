import torch
import numpy as np
import math
from torch_scatter import scatter_min, scatter_max, scatter_mean, scatter_sum
from torch_geometric.utils import coalesce, remove_self_loops
from torch_geometric.nn.pool.consecutive import consecutive_cluster
from src.utils.tensor import arange_interleave
from src.utils.geometry import base_vectors_2d
from src.utils.sparse import sizes_to_pointers, sparse_sort, \
    sparse_sort_along_direction
from src.utils.scatter import scatter_pca, idx_preserving_mask
from src.utils.edge import edge_wise_pixels

import os.path as osp
import sys

dependencies_folder = osp.dirname(osp.dirname(osp.abspath(__file__)))
sys.path.append(dependencies_folder)
sys.path.append(osp.join(dependencies_folder, "dependencies/grid_graph/python/bin"))
from grid_graph import grid_to_graph

from time import time

__all__ = [
    'is_pyg_edge_format', 'isolated_nodes', 'edge_to_superedge', # 'subedges',
    'to_trimmed', 'is_trimmed', 'assign2graph']

def is_pyg_edge_format(edge_index):
    """Check whether edge_index follows pytorch geometric graph edge
    format: a [2, N] torch.LongTensor.
    """
    return \
        isinstance(edge_index, torch.Tensor) and edge_index.dim() == 2 \
        and edge_index.dtype == torch.long and edge_index.shape[0] == 2


def isolated_nodes(edge_index, num_nodes=None):
    """Return a boolean mask of size num_nodes indicating which node has
    no edge in edge_index.
    """
    assert is_pyg_edge_format(edge_index)
    num_nodes = edge_index.max() + 1 if num_nodes is None else num_nodes
    mask = torch.ones(num_nodes, dtype=torch.bool, device=edge_index.device)
    mask[edge_index.unique()] = False
    return mask


def edge_to_superedge(edges, super_index, edge_attr=None):
    """Convert pixel-level edges into superedges between clusters, based
    on pixel-to-cluster indexing 'super_index'. Optionally 'edge_attr'
    can be passed to describe edge attributes that will be returned
    filtered and ordered to describe the superedges.

    NB: this function treats (i, j) and (j, i) superedges as identical.
    By default, the final edges are expressed with i <= j
    """
    # We are only interested in the edges connecting two different
    # clusters and not in the intra-cluster connections. So we first
    # identify the edges of interest. This step requires having access
    # to the 'super_index' to convert pixel indices into their
    # corresponding cluster indices
    se = super_index[edges]
    inter_cluster = torch.where(se[0] != se[1])[0]

    # Now only consider the edges of interest (ie inter-cluster edges)
    edges_inter = edges[:, inter_cluster]
    edge_attr = edge_attr[inter_cluster] if edge_attr is not None else None
    se = se[:, inter_cluster]

    # Search for undirected edges, ie edges with (i,j) and (j,i)
    # both present in edge_index. Flip (j,i) into (i,j) to make them
    # redundant. By default, the final edges are expressed with i <= j
    s_larger_t = se[0] > se[1]
    se[:, s_larger_t] = se[:, s_larger_t].flip(0)

    # So far we are manipulating inter-cluster edges, but there may be
    # multiple of those for a given source-target pair. If, we want to
    # aggregate those into 'superedges' and compute corresponding
    # features (designated with 'se_'), we will need unique and
    # compact inter-cluster edge identifiers for torch_scatter
    # operations. We use 'se' to designate 'superedge' (ie an edge
    # between two clusters)
    se_id = \
        se[0] * (max(se[0].max(), se[1].max()) + 1) + se[1]
    se_id, perm = consecutive_cluster(se_id)
    se = se[:, perm]

    return se, se_id, edges_inter, edge_attr

def to_trimmed(edge_index, edge_attr=None, reduce='mean'):
    """Convert to 'trimmed' graph: same as coalescing with the
    additional constraint that (i, j) and (j, i) edges are duplicates.

    If edge attributes are passed, 'reduce' will indicate how to fuse
    duplicate edges' attributes.

    NB: returned edges are expressed with i<j by default.

    :param edge_index: 2xE LongTensor
        Edges in `torch_geometric` format
    :param edge_attr: ExC Tensor, optional
        Edge attributes
    :param reduce: str, optional
        Reduction modes supported by `torch_geometric.utils.coalesce`
    :return:
    """
    # Search for undirected edges, ie edges with (i,j) and (j,i)
    # both present in edge_index. Flip (j,i) into (i,j) to make them
    # redundant
    s_larger_t = edge_index[0] > edge_index[1]
    edge_index[:, s_larger_t] = edge_index[:, s_larger_t].flip(0)
    # Sort edges by row and remove duplicates
    if edge_attr is None:
        edge_index = coalesce(edge_index)
    else:
        edge_index, edge_attr = coalesce(
            edge_index, edge_attr=edge_attr, reduce=reduce)
    # Remove self loops
    edge_index, edge_attr = remove_self_loops(
        edge_index, edge_attr=edge_attr)
    if edge_attr is None:
        return edge_index
    return edge_index, edge_attr


def is_trimmed(edge_index, return_trimmed=False):
    """Check if the graph is 'trimmed': same as coalescing with the
    additional constraint that (i, j) and (j, i) edges are duplicates.

    :param edge_index: 2xE LongTensor
        Edges in `torch_geometric` format
    :param return_trimmed: bool
        If True, the trimmed graph will also be returned. Since checking
        if the graph is trimmed requires computing the actual trimmed
        graph, this may save some compute in certain situations
    :return:
    """
    edge_index_trimmed = to_trimmed(edge_index)
    trimmed = edge_index.shape == edge_index_trimmed.shape
    if return_trimmed:
        return trimmed, edge_index_trimmed
    return trimmed

def assign2graph(assignement,edge_index=None,edge_attr=None):
    """
    create edges for a new super-pixel level:
    Use edges at level 0 to create higher level edges, 
        if there exite an edge between a pixel of super-pixel i and an other pixel of superpixel j then an egde between i and j is created
    if no edge_index given, assume connectivity of 1 (4 neighbors)
    if edge_attr given, compute the sum of the fused edges
    """

    assert edge_attr is None or edge_index is not None

    vertices,size = torch.unique(assignement,return_counts=True)
    num_vertices = len(vertices)
    if num_vertices-1 != vertices.max():
        mapping = torch.zeros(vertices.max())
        mapping[vertices] = torch.arrange(num_vertices)
        assignement = mapping[assignement]
    
    if edge_index is None:
        edges = torch.from_numpy(grid_to_graph(np.array(assignement.shape), connectivity = 1, compute_connectivities=False, graph_as_forward_star = False, 
            row_major_index = True).astype(np.int64).T).to(assignement.device)
        edges = assignement.flatten()[edges]
    else:
        edges = assignement.flatten()[edge_index]
    
    if edge_attr is not None:
        edge_attr = edge_attr[edges[0, :] != edges[1, :]]
    edges = edges[:, edges[0, :] != edges[1, :]] # remove self loop
    edges,_ = torch.sort(edges, dim=0) # {i,j} = {j,i}
    
    edge_map = edges[0, :] + num_vertices * edges[1, :]
    
    if edge_attr is not None:
        attr = scatter_sum(edge_attr,edge_map)
        attr = attr[attr!=0]
        edges = torch.unique(edge_map)
    else:
        edges,attr = torch.unique(edge_map,return_counts=True)
    edges = torch.stack((edges%num_vertices,torch.div(edges,num_vertices,rounding_mode='floor')))

    return vertices,edges,size,attr
