import torch
import numpy as np
#from scipy.spatial import Delaunay
from torch_scatter import scatter_mean, scatter_std, scatter_max
from torch_geometric.utils import add_self_loops, dropout_edge, subgraph
from src.transforms import Transform
from src.data import NAG,Data
from src.utils import is_trimmed, to_trimmed, \
    PIXEL_FEATURES, SEGMENT_BASE_FEATURES, update_shapes, \
    SUBEDGE_FEATURES, ON_THE_FLY_HORIZONTAL_FEATURES,\
    ON_THE_FLY_VERTICAL_FEATURES, sanitize_keys, \
    rgb2hsv, scatter_pca_2D
from src.metrics import averageMeter
import sys
import os.path as osp

dependencies_folder = osp.dirname(osp.dirname(osp.abspath(__file__)))
sys.path.append(dependencies_folder)
sys.path.append(osp.join(dependencies_folder, "dependencies/grid_graph/python/bin"))
sys.path.append(osp.join(dependencies_folder, "dependencies/multilabel-potrace/python/bin"))
from grid_graph import edge_list_to_forward_star, grid_to_graph
from multilabel_potrace_polygraph import multilabel_potrace_polygraph
import threading as th

from src.transforms.geometry import Diameter
from tqdm import tqdm

__all__ = [
    'AdjacencyGraph', 'NAGAdjacencyGraph',
    'SegmentFeatures', 'ShapeInterfacePolygones',
    'OnTheFlyNodeFeatures',
    'OnTheFlyHorizontalEdgeFeatures', 
    'OnTheFlyVerticalEdgeFeatures','NAGAddHopEdges',  
    'NAGAddSelfLoops', 'NodeSize',
    'DropEdges', 'DropNodes']


class AdjacencyGraph(Transform):
    """Create the adjacency graph in `edge_index` and `edge_attr` based
    on image size stored in the data and the chosen connectivity

    NB: this graph is directed wrt Pytorch Geometric, but cut-pursuit
    happily takes this as an input.

    :param connectivity: int (see dependencies/grid_graph)
    """

    def __init__(self, connectivity=1):
        super().__init__()
        self.connectivity = connectivity

    def _process(self, data):
        
        # Save edges and edge features in data
        edge,attr = grid_to_graph(np.array(data.image.shape[-2:]),connectivity = self.connectivity, 
            compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)
        data.edge_index = torch.from_numpy(edge.astype(np.int64).T).to(data.device)
        data.edge_attr = torch.from_numpy(attr.astype(np.float32)).to(data.device)
        return data

class NAGAdjacencyGraph(Transform):
    """Create the adjacency graph in `edge_index` and `edge_attr` based
    on image size stored in the data (nag[0]) and the chosen connectivity

    NB: this graph is directed wrt Pytorch Geometric, but cut-pursuit
    happily takes this as an input.

    :param connectivity: int (see dependencies/grid_graph)
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, connectivity=1):
        super().__init__()
        self.connectivity = connectivity

    def _process(self, nag):
        if hasattr(nag[0],"image") and not nag[0].has_edges:
            transform = AdjacencyGraph(self.connectivity)
            nag._list[0] = transform(nag._list[0])
        return nag

class SegmentFeatures(Transform): 
    """Compute segment features for all the NAG levels except its first
    (ie the 0-level). These are handcrafted node features that will be
    saved in the node attributes. To make use of those at training time,
    remember to move them to the `x` attribute using `AddKeysTo` and
    `NAGAddKeysTo`.

    Compute some edge features, see _compute_edge_features and ON_THE_FLY_HORIZONTAL_FEATURES

    The supported feature keys are the following:
        - size : number of pixels in the superpixel
        - perimeter: number of edges of level 0 linking a node to its neighbors

        - mean_rgb
        - mean_hsv
        - mean_lab
        - mean_intensity
        - mean_nir
        - mean_elevation
        
        - std_rgb
        - std_hsv
        - std_lab
        - std_intensity

        - log_size
        - log_perimeter

    :param keys: List(str), str, or None
        Features to be computed segment-wise and saved under `<key>`.
        If None, all supported features will be computed
    :param mean_keys: List(str), str, or None
        Features to be computed from the pixels and the segment-wise
        mean aggregation will be saved under `mean_<key>`. If None, all
        supported features will be computed
    :param std_keys: List(str), str, or None
        Features to be computed from the pixels and the segment-wise
        std aggregation will be saved under `std_<key>`. If None, all
        supported features will be computed
    :param strict: bool
        If True, will raise an exception if an attribute from key is
        not within the input pixel Data keys
    :param edge_keys: List(str), str, or None
        Features to be computed edge-wise and saved under `<key>` (feature named e_key).
        If None, all supported features will be computed
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    _NO_REPR = ['strict']

    def __init__(
            self,
            keys=None,
            mean_keys=None,
            std_keys=None,
            edge_keys=None,
            strict=True,
            num_classes=None,
            topology_keys=None):
        super().__init__()
        self.keys = sanitize_keys(keys, default=SEGMENT_BASE_FEATURES)
        self.mean_keys = sanitize_keys(mean_keys, default=PIXEL_FEATURES)
        self.std_keys = sanitize_keys(std_keys, default=PIXEL_FEATURES)
        self.edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
        self.strict = strict
        assert num_classes is not None
        self.num_classes=num_classes
        self.topology_keys = topology_keys
        self.normalize = []

    def _process(self, nag):
        for i_level in range(1, nag.num_levels):
            while len(self.normalize)<i_level+1:
                self.normalize.append({})
            nag = _compute_cluster_features(
                i_level,
                nag,
                keys=self.keys,
                mean_keys=self.mean_keys,
                std_keys=self.std_keys,
                edge_keys=self.edge_keys,
                strict=self.strict,
                normalize = self.normalize[i_level])
            nag = _compute_edge_features(
                i_level,
                nag,
                edge_keys=self.edge_keys,
                normalize = self.normalize[i_level],
                topology_keys = self.topology_keys)
        if len(self.normalize)==i_level+1:
            self.normalize.append(torch.zeros(self.num_classes,device=nag.device))
        self.normalize[i_level+1] += nag[i_level].y.sum(dim=0)[:self.num_classes]
        return nag


def _compute_cluster_features(
        i_level,
        nag,
        keys=None,
        mean_keys=None,
        std_keys=None,
        edge_keys=None,
        strict=True,
        normalize=None): 
    """ compute SuperPixels features """
    assert isinstance(nag, NAG)
    assert i_level > 0, "Cannot compute cluster features on level-0"
    assert nag[0].num_nodes < np.iinfo(np.uint32).max, \
        "Too many nodes for `uint32` indices"
    
    keys = sanitize_keys(keys, default=SEGMENT_BASE_FEATURES)
    mean_keys = sanitize_keys(mean_keys, default=PIXEL_FEATURES)
    std_keys = sanitize_keys(std_keys, default=PIXEL_FEATURES)
    edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)

    # Recover the i_level Data object we will be working on
    data = nag[i_level]
    num_nodes = data.num_nodes
    device = nag.device

    # Compute how many level-0 pixels each level cluster contains
    sub_size = nag.get_sub_size(i_level, low=0)

    # Get the cluster index each pixel belongs to (to calculate mean and std over super_lusters, perimeter and normal)
    super_index = nag.get_super_index(i_level)

    if 'global_mean_size' in keys:
        data.global_mean_size = sub_size.float().mean().unsqueeze(0)
        if 'global_mean_size' not in normalize:
            normalize['global_mean_size'] = [averageMeter()]
        normalize['global_mean_size'][0].update(sub_size.float().mean())
    
    if 'graph_size' in keys:
        data.graph_size = torch.tensor(sub_size.shape[0],device=device).unsqueeze(0)
        if 'graph_size' not in normalize:
            normalize['graph_size'] = [averageMeter()]
        normalize['graph_size'][0].update(sub_size.shape[0])
   
    if 'node_size' in keys or 'e_size' in edge_keys:
        data.node_size = sub_size
        if 'node_size' not in normalize:
            normalize['node_size'] = [averageMeter()]
        normalize['node_size'][0].update(data.node_size)
    
    if 'log_size' in keys or 'e_log_size' in edge_keys:
        data.log_size = torch.log(sub_size + 1).view(-1, 1)
        if 'log_size' not in normalize:
            normalize['log_size'] = [averageMeter()]
        normalize['log_size'][0].update(data.log_size)
    
    if 'perimeter' in keys or 'e_perimeter' in edge_keys or 'log_perimeter' in keys or 'e_log_perimeter' in edge_keys:
        assert hasattr(nag[0],"edge_index"), "need level 0 edges to compute the perimeters"
        edge_index = nag[0].edge_index
        perimeter = torch.bincount(super_index[edge_index[:,super_index[edge_index[0]] != \
            super_index[edge_index[1]]]].flatten()).view(-1,1)

        if 'perimeter' in keys or 'e_perimeter' in edge_keys:
            data.perimeter = perimeter
            if 'perimeter' not in normalize:
                normalize['perimeter'] = [averageMeter()]
            normalize['perimeter'][0].update(data.perimeter)

        if 'log_perimeter' in keys or 'e_log_perimeter' in edge_keys:
            data.log_perimeter = torch.log(perimeter+1)
            if 'log_perimeter' not in normalize:
                normalize['log_perimeter'] = [averageMeter()]
            normalize['log_perimeter'][0].update(data.log_perimeter)

    if any(map(lambda s:s.startswith('pca'),keys)):
        assert hasattr(nag[0],'pos'), "need position at low level to compute PCA"
        pca = scatter_pca_2D(nag[0].pos,super_index)

        if 'pca_val' in keys:
            data.pca_val = pca[:,0:2]
            if 'pca_val' not in normalize:
                normalize['pca_val'] = [averageMeter() for _ in range(data[f'pca_val'].shape[1])]
            for i in range(data['pca_val'].shape[1]):
                normalize['pca_val'][i].update(data['pca_val'][:,i])

        if 'pca_ratio' in keys:
            data.pca_ratio = pca[:,0]/(pca[:,1]+1e-12) # smalest values in pca[:,1] near 1e-7
            if 'pca_ratio' not in normalize:
                normalize['pca_ratio'] = [averageMeter()]
            normalize['pca_ratio'][0].update(data.pca_ratio)

        if 'pca_dir' in keys:
            data.pca_dir = pca[:,2:4]

    # Add the mean of pixel attributes, identified by their key
    for key in mean_keys:
        if key == "hsv":
            raise NotImplementedError(f"impossible to compute mean for hsv : mean in cylinder coordinate is meaning less, transformation to cartesian and return to cylinder not implemented")
        else:
            f = getattr(nag[0], key, None)
        if f is None and strict:
            raise ValueError(f"No pixel key `{key}` to build 'mean_{key} key'")
        elif f is None:
            continue
        data[f'mean_{key}'] = scatter_mean(f.float(), super_index, dim=0)
        if data[f'mean_{key}'].dim() == 1:
            data[f'mean_{key}'] = data[f'mean_{key}'].unsqueeze(1) 
        if f'mean_{key}' not in normalize:
            normalize[f'mean_{key}'] = [averageMeter() for _ in range(data[f'mean_{key}'].shape[1])]
        for i in range(data[f'mean_{key}'].shape[1]):
            normalize[f'mean_{key}'][i].update(data[f'mean_{key}'][:,i])



    # Add the std of pixel attributes, identified by their key
    for key in std_keys:
        if key == "hsv":
            raise NotImplementedError(f"impossible to compute std for hsv : std in cylinder coordinate is meaning less, transformation to cartesian and return to cylinder not implemented")
        else:
            f = getattr(nag[0], key, None)
        if f is None and strict:
            raise ValueError(f"No pixel key `{key}` to build 'std_{key} key'")
        if f is None:
            continue
        data[f'std_{key}'] = scatter_std(nag[0][key].float(), super_index, dim=0)
        if data[f'std_{key}'].dim() == 1:
            data[f'std_{key}'] = data[f'std_{key}'].unsqueeze(1)
        if f'std_{key}' not in normalize:
            normalize[f'std_{key}'] = [averageMeter() for _ in range(data[f'std_{key}'].shape[1])]
        for i in range(data[f'std_{key}'].shape[1]):
            normalize[f'std_{key}'][i].update(data[f'std_{key}'][:,i])

    # Update the i_level Data in the NAG
    nag._list[i_level] = data
    return nag

def _compute_edge_features(
        i_level,
        nag,
        edge_keys=None,
        normalize=None,
        topology_keys=None): 
    """precompute some edge features to be saved
        result are in directed graph, data need to be duplicated for underected graph
        (see ON_THE_FLY_HORIZONTAL_FEATURES) """
    assert isinstance(nag, NAG)
    assert i_level > 0, "Cannot compute edge features on level-0"
    assert nag[0].num_nodes < np.iinfo(np.uint32).max, \
        "Too many nodes for `uint32` indices"

    edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
    complet_list = edge_keys + (tuple(topology_keys) if topology_keys is not None else ())

    # Recover the i_level Data object we will be working on
    data = nag[i_level]
    num_nodes = data.num_nodes
    device = nag.device
    
    se = data.edge_index
    super_index = nag.get_super_index(i_level)
    
    if 'same_SP' in complet_list:
        data.same_SP = ((super_index[se[0]]!=super_index[se[1]])*1).float() # logic inverse to simplify add self loop (all coordinates to 0 and same_sp is obviously true) 
    
    if 'interface_length' in complet_list:
        edge_index = nag[0].edge_index
        super_index = nag.get_super_index(i_level)
        n_node = torch.max(super_index).item()+1
        #if n_node < 5000: # check for needed memory in bincount
        count = torch.bincount((super_index[edge_index[0]]+super_index[edge_index[1]]*n_node).cpu()) # compute on CPU for memory usage 
        f = ((count.reshape([n_node,n_node])+count.reshape([n_node,n_node]).transpose(1,0))[se[0],se[1]]).to(super_index.device) # return on original device
        """else: # slower but use less memory
            print("start length loop ",n_node) 
            si = se[0]+se[1]*n_node
            i = torch.cat((super_index[edge_index[0]] + super_index[edge_index[1]]*n_node,super_index[edge_index[0]]*n_node + super_index[edge_index[1]]))
            f = torch.empty(si.shape[0],device=super_index.device).long()
            for idx in range(si.shape[0]):
                f[idx] = torch.sum(i==si[idx])
            print("end lenght loop")"""
        if 'interface_length' not in normalize:
            normalize['interface_length'] = [averageMeter()]
        normalize['interface_length'][0].update(f)
        data.interface_length = f.view(-1,1)
    
    if 'centroid_dir' in complet_list or 'centroid_dist' in complet_list:
        # Compute the distance and direction between the segments'
        # centroids
        se_centroid_dir = data.pos[se[1]] - data.pos[se[0]]
        se_centroid_dist = se_centroid_dir.norm(dim=1).view(-1, 1)
        se_centroid_dir /= se_centroid_dist.view(-1, 1)
        se_centroid_dist = se_centroid_dist.sqrt()

        # Sanity checks on normalized directions
        se_centroid_dir[se_centroid_dir.isnan()] = 0
        se_centroid_dir = se_centroid_dir.clip(-1, 1)

        if 'centroid_dir' in complet_list: # no need to compute normalize values for a normalized direction
            data.centroid_dir = se_centroid_dir 

        if 'centroid_dist' in complet_list:
            if 'centroid_dist' not in normalize:
                normalize['centroid_dist'] = [averageMeter()]
            normalize['centroid_dist'][0].update(se_centroid_dist)
            data.centroid_dist = se_centroid_dist
    
    # remove all in edge_attr
    if data.edge_attr is not None:
        data.edge_attr = None

    return nag

class ShapeInterfacePolygones(Transform): 
    """Compute the polygones and polylines for all super pixels at each level.

    :param straight_line_tol: float
        precision of the aproximation (cf. https://gitlab.com/1a7r0ch3/multilabel-potrace)
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    _NO_REPR = ['strict']

    def __init__(
            self,
            straight_line_tol=1.0,
            polyline_hf=[],
            use_local_image_feature=False):
        super().__init__()
        self.straight_line_tol = straight_line_tol
        self.polyline_hf = polyline_hf
        self.use_local_image_feature = use_local_image_feature

    def _process(self, nag):
        w,h = nag[0].image.shape[-2:]
        for i_level in range(1, nag.num_levels):
            data = nag[i_level]

            # edges liste to forward star (CSR)
            source_csr, target, reindex = edge_list_to_forward_star(
                data.num_nodes, data.edge_index.T.contiguous().cpu().numpy())
            source_csr = source_csr.astype(np.uint32)
            target = target.astype(np.uint32)
            
            # compute shapes and interfaces aproximations
            Shapes,Interfaces = multilabel_potrace_polygraph(np.asfortranarray(np.copy(nag.get_super_index(i_level).reshape(w,h).numpy().astype(np.uint32))), source_csr, target, straight_line_tol=self.straight_line_tol)
            
            shape_vertex = torch.from_numpy(Shapes[0].astype(np.float32)).to(data.device).transpose(0,1)
            shape_edges = torch.from_numpy(Shapes[1].astype(np.int64)).to(data.device)
            shape_index = torch.from_numpy(Shapes[2].astype(np.int64)).to(data.device)

            interface_vertex = torch.from_numpy(Interfaces[0].astype(np.float32)).to(data.device).transpose(0,1)
            interface_edges = torch.from_numpy(Interfaces[1].astype(np.int64)).to(data.device)
            interface_index = torch.from_numpy(Interfaces[2].astype(np.int64)).to(data.device)
            
            if self.use_local_image_feature: # compute closest pixels for all vertex
                rounded_shape_vertex = shape_vertex.round().long()
                rounded_shape_vertex[:,1] = (w-rounded_shape_vertex[:,1]).clip(0,w-1) # move origine from botom left to top left, set all coords inside image
                rounded_shape_vertex[:,0] = rounded_shape_vertex[:,0].clip(0,h-1) # set all coords inside image
                rounded_interface_vertex = interface_vertex.round().long()
                rounded_interface_vertex[:,1] = (w-rounded_interface_vertex[:,1]).clip(0,w-1) # move origine from botom left to top left, set all coords inside image
                rounded_interface_vertex[:,0] = rounded_interface_vertex[:,0].clip(0,h-1) # set all coords inside image

            # Normalize shapes and interfaces
            ## each polygones or polylines is centered on it's centroïd, scaled in the unit sphere and rotate such that the farthest points from the center is on the abscissa axis
            centers = scatter_mean(shape_vertex.float(),shape_index,dim=0)
            shape_vertex = shape_vertex-centers[shape_index]
            
            distances = torch.sqrt(torch.sum(torch.pow(shape_vertex,2),dim=1))
            max_dist,max_dist_index = scatter_max(distances.float(),shape_index)
            shape_vertex = shape_vertex/max_dist[shape_index].unsqueeze(1)

            if "norm_dist" in self.polyline_hf:
                shape_norm_dist = max_dist[shape_index]
                
            if "norm_angle" in self.polyline_hf: 
                #angle with vector [1,0]
                shape_norm_angle = torch.arccos(torch.clamp(shape_vertex[max_dist_index,0],min=-1,max=1))
                # sign(sin) = sign(y_coord)
                shape_norm_angle[shape_vertex[max_dist_index,1]<0] = -shape_norm_angle[shape_vertex[max_dist_index,1]<0]
                shape_norm_angle = shape_norm_angle[shape_index]

            shape_vertex = torch.stack((torch.sum(shape_vertex*shape_vertex[max_dist_index][shape_index], dim=1), 
                torch.sum(shape_vertex*(torch.flip(shape_vertex[max_dist_index][shape_index], 
                    (1,))*torch.tensor([-1,1])), dim=1)), dim=1)
            
            centers = scatter_mean(interface_vertex.float(),interface_index,dim=0)
            interface_vertex = interface_vertex-centers[interface_index]
            distances = torch.sqrt(torch.sum(torch.pow(interface_vertex,2),dim=1))
            max_dist,max_dist_index = scatter_max(distances.float(),interface_index)
            max_dist_index[max_dist_index==interface_vertex.shape[0]] = 0 # remove values out of bound (this values will not be used)
            interface_vertex = interface_vertex/max_dist[interface_index].unsqueeze(1)

            if "norm_dist" in self.polyline_hf:
                interface_norm_dist = max_dist[interface_index]
            
            if "norm_angle" in self.polyline_hf:
                interface_norm_angle = torch.arccos(torch.clamp(interface_vertex[max_dist_index,0],min=-1,max=1))
                interface_norm_angle[interface_vertex[max_dist_index,1]<0] = -interface_norm_angle[interface_vertex[max_dist_index,1]<0]
                interface_norm_angle = interface_norm_angle[interface_index]

            interface_vertex = torch.stack((torch.sum(interface_vertex*interface_vertex[max_dist_index][interface_index], dim=1), 
                torch.sum(interface_vertex*(torch.flip(interface_vertex[max_dist_index][interface_index], 
                    (1,))*torch.tensor([-1,1])), dim=1)), dim=1)
            
            if "angle" in self.polyline_hf:
                # Compute interiore angle of all vertices
                vecteurs = shape_vertex[shape_edges[1]]-shape_vertex[shape_edges[0]] # compute vectors of each edge
                vecteurs_norm = torch.sqrt(torch.sum(torch.square(vecteurs),dim=1)).unsqueeze(1)
                
                # remove vector with norm null
                index_null = (vecteurs_norm==0).nonzero(as_tuple=True)[0] # index of null vector and unnecesary edges
                index_node = shape_edges[0,index_null] # index of duplicate nodes
                index = torch.ones(shape_vertex.shape[0], dtype=bool) 
                index[index_node] = False
                shape_vertex = shape_vertex[index] # select all non duplicate nodes
                shape_index = shape_index[index]
                shape_norm_dist = shape_norm_dist[index]
                shape_norm_angle = shape_norm_angle[index]
                index = torch.ones(shape_edges.shape[1], dtype=bool)
                index[index_null] = False
                shape_edges = shape_edges[:,index] # select all necesary edge
                vecteurs = vecteurs[index]
                vecteurs_norm = vecteurs_norm[index]
                shape_edges = shape_edges - torch.sum(shape_edges>index_node.repeat_interleave(
                    shape_edges.numel()).reshape((index_node.numel(),*shape_edges.shape)),dim=0) # update node index in edges
                vecteurs = vecteurs/vecteurs_norm
            
                vect_s = torch.zeros(vecteurs.shape)
                vect_t = torch.zeros(vecteurs.shape)
                vect_s[shape_edges[0]] = vecteurs # for each vertices, keep the vector of witch it is the source
                vect_t[shape_edges[1]] = -vecteurs # for each vertices, keep the vector of witch it is the target
            
                shape_angle = torch.arccos(torch.clamp(torch.sum(vect_s * vect_t,dim=1),min=-1,max=1))
                # compute angle for all points (in [0,pi]), clamp for float imprecision that cause nan values
                sin = torch.sum((vect_s * torch.flip(vect_t,(1,)))*torch.tensor([1,-1]),dim=1).squeeze() 
                # check if angles in [pi,2*pi] (sign of sinus)
                shape_angle[sin<0] = -shape_angle[sin<0] # change angles in [pi,2*pi]

                vecteurs = interface_vertex[interface_edges[0]]-interface_vertex[interface_edges[1]]
                vecteurs_norm = torch.sqrt(torch.sum(torch.square(vecteurs),dim=1)).unsqueeze(1)
                # remove vector with norm null
                index_null = (vecteurs_norm==0).nonzero(as_tuple=True)[0] # index of null vector and unnecesary edges
                index_node = interface_edges[0,index_null] # index of duplicate nodes
                index = torch.ones(interface_vertex.shape[0], dtype=bool) 
                index[index_node] = False
                interface_vertex = interface_vertex[index] # select all non duplicate nodes
                interface_index = interface_index[index]
                interface_norm_dist = interface_norm_dist[index]
                interface_norm_angle = interface_norm_angle[index]
                index = torch.ones(interface_edges.shape[1], dtype=bool)
                index[index_null] = False
                interface_edges = interface_edges[:,index] # select all necesary edge
                vecteurs = vecteurs[index]
                vecteurs_norm = vecteurs_norm[index]
                interface_edges = interface_edges - torch.sum(interface_edges>index_node.repeat_interleave(
                    interface_edges.numel()).reshape((index_node.numel(),*interface_edges.shape)),dim=0) # update node index in edges       
                vecteurs = vecteurs/vecteurs_norm
         
                vect_s = torch.zeros([interface_vertex.shape[0],2])
                vect_t = torch.zeros([interface_vertex.shape[0],2])
                vect_s[interface_edges[0]] = vecteurs # for each vertices, keep the vector of witch it is the source
                vect_t[interface_edges[1]] = -vecteurs # for each vertices, keep the vector of witch it is the target

                interface_angle = torch.arccos(torch.clamp(torch.sum(vect_s * vect_t,dim=1),min=-1,max=1))
                sin = torch.sum((vect_s * torch.flip(vect_t,(1,)))*torch.tensor([1,-1]),dim=1).squeeze()
                interface_angle[sin<0] = -interface_angle[sin<0]
                interface_angle[torch.logical_or(torch.logical_and(vect_t[:,0] == 0,vect_t[:,1]==0),
                    torch.logical_and(vect_s[:,0] == 0,vect_s[:,1]==0))] = 0 # set all point in extremity to 0
            
            # add features to vertices
            
            if "norm_dist" in self.polyline_hf: # distance between [0,1], fraction of the bigest length of the image
                shape_norm_dist = shape_norm_dist/max(w,h)
                shape_vertex = torch.cat((shape_vertex,shape_norm_dist.unsqueeze(1)),dim=1)
                interface_norm_dist = interface_norm_dist/max(w,h)
                interface_vertex = torch.cat((interface_vertex,interface_norm_dist.unsqueeze(1)),dim=1)
            
            if "norm_angle" in self.polyline_hf: # angle between [-pi,pi]
                shape_vertex = torch.cat((shape_vertex,shape_norm_angle.unsqueeze(1)),dim=1)
                interface_vertex = torch.cat((interface_vertex,interface_norm_angle.unsqueeze(1)),dim=1)
            
            if "angle" in self.polyline_hf: # angle between [-pi,pi]
                shape_vertex = torch.cat((shape_vertex,shape_angle.unsqueeze(1)),dim=1)
                interface_vertex = torch.cat((interface_vertex,interface_angle.unsqueeze(1)),dim=1)

            # add missing interface (interface of length null)
            mask = torch.ones(target.shape[0],dtype=interface_index.dtype)
            mask[interface_index] = 0
            missing_interface = torch.nonzero(mask).squeeze(1) # target.shape[0] = nb edges
            interface_edges = torch.cat((interface_edges, 
                (torch.arange(2*missing_interface.numel())+interface_vertex.shape[0]).reshape([2,missing_interface.numel()])), dim=1)
            interface_vertex = torch.cat((interface_vertex,torch.zeros([2*missing_interface.numel(),interface_vertex.shape[1]])),dim=0) # add missing interface
            interface_index = torch.cat((interface_index,missing_interface.repeat_interleave(2)))

            # store the new data  ### torch.cat((shape_edges,shape_edges.flip(0)),dim=1)
            data.shapes = Data(edge_index=shape_edges, pos=shape_vertex, batch_node=shape_index) # batch in attribute for batch creation
            data.interfaces = Data(edge_index=interface_edges, pos=interface_vertex, batch_node=interface_index)
            if self.use_local_image_feature:
                data.shapes["round_pos"] = rounded_shape_vertex
                data.interfaces["round_pos"] = torch.cat((rounded_interface_vertex,torch.zeros([2*missing_interface.numel(),2],dtype=rounded_interface_vertex.dtype)))
                data.interfaces["null_interfaces"] = torch.arange(data.interfaces["round_pos"].shape[0])>=rounded_interface_vertex.shape[0]
        
        return nag

class OnTheFlyNodeFeatures(Transform):
    """ remove unused image dimentions
            
    compute or duplicate node value to reduce load charges"""

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(
            self, keys=None, image_keys=["rgb"]):
        super().__init__()
        self.keys = sanitize_keys(keys, default=SEGMENT_BASE_FEATURES)
        self.image_keys = image_keys
    
    def _process(self, nag):
        nano = not hasattr(nag[0],'image')
        if not nano:
            # select the image features
            if self.image_keys is not None and len(self.image_keys)>0:
                for k in self.image_keys:
                    dim = torch.empty(0,device=nag.device,dtype=torch.long)
                    if getattr(nag[0],f'{k}_dim',None) is None:
                        raise AttributeError(f"missing {k}_dim in nag[0]")
                    else:
                        dim = torch.cat((dim,nag[0][f'{k}_dim']))
                if nag[0].image.dim() == 4:
                    nag[0].image = nag[0].image[:,dim]
                else:
                    nag[0].image = nag[0].image[dim]
        for i_level in range(1-nano, nag.num_levels):
            nag._list[i_level] = _on_the_fly_node_features(
                nag,
                i_level,
                keys = self.keys)
        return nag

def _on_the_fly_node_features(nag,i_level, keys=None): # , norm_val=None):
    """ Compute and duplicate global node features """
    
    data = nag._list[i_level]
    
    if 'global_mean_size' in keys:
        assert getattr(data,'global_mean_size',None) is not None, \
            "Expected input Data to have 'global_mean_size' attribute"
        if getattr(data,'batch',None) is not None:
            data.global_mean_size = data.global_mean_size[data.batch]
        else:
            data.global_mean_size = torch.full((data.num_pixels,),data.global_mean_size.item(),device=data.device)

    if 'graph_size' in keys:
        assert getattr(data,'graph_size',None) is not None, \
            "Expected input Data to have 'graph_size' attribute"
        if getattr(data,'batch',None) is not None:
            data.graph_size = data.graph_size[data.batch]
        else: 
            data.graph_size = torch.full((data.num_pixels,),data.graph_size.item(),device=data.device)

    return data

class OnTheFlyHorizontalEdgeFeatures(Transform):
    """Compute edge features "on-the-fly" for all i->j and j->i
    horizontal edges of the NAG levels except its first (ie the
    0-level).

    Expects only trimmed edges as input, along with some edge-specific
    attributes that cannot be recovered from the corresponding source
    and target node attributes (see `src.utils.to_trimmed`).

    Accepts input edge_attr to be float16, to alleviate memory use and
    accelerate data loading and transforms. Output edge_<key> will,
    however, be in float32.

    Optionally adds some edge features that can be recovered from the
    source and target node attributes.

    Builds the j->i edges and corresponding features based on their i->j
    counterparts in the trimmed graph.

    Equips the output NAG with all i->j and j->i nodes and corresponding
    features.

    Note: this transform is intended to be called after all sampling
    transforms, to mitigate compute and memory impact of horizontal
    edges.

    The supported feature keys are the following:
            Compute these features
      - e_size: size of the source and the target
      - e_perimeter: perimeter of the source and the target

      - e_log_size: log size of the source and the target
      - e_log_perimeter: log perimeter of the source and the target

            Duplicate these features for undirected graph
      - same_SP
      - interface_length
      - centroid_dir
      - centroid_dist
    
    :param keys: List(str)
        Features to be computed. Attributes will be saved under `<key>`
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(
            self, keys=None, topology_keys=None):
        super().__init__()
        self.keys = sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
        self.global_file_name = None
        self.topology_keys = topology_keys
    
    def _process(self, nag):
        nano = not hasattr(nag[0],'image')
        if self.global_file_name is not None:
            name,ext = osp.splitext(self.global_file_name)
            norm_val = torch.load(name+"_"+nag.device.type+ext)
            if nano:
                norm_val = norm_val[1:]
        else:
            norm_val = [None] * nag.num_levels

        for i_level in range(1-nano, nag.num_levels):
            nag._list[i_level] = _on_the_fly_horizontal_edge_features(
                nag,
                i_level,
                keys=self.keys,
                topology_keys=self.topology_keys,
                norm_val=norm_val[i_level])
        return nag

def _on_the_fly_horizontal_edge_features(
        nag,i_level, keys=None, topology_keys=None, norm_val=None):
    """Compute all edges and edge features for a horizontal graph, given
    a trimmed graph and some precomputed edge attributes.
    """
    if topology_keys is None:
        topology_keys = []
    keys = list(sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES))
    complet_list = keys + topology_keys
    data = nag._list[i_level]

    # Recover the edges between the segments
    se = data.edge_index

    assert is_trimmed(se), \
        "Expects the graph to be trimmed, consider using " \
        "`src.utils.to_trimmed()` before computing the features"
    
    if 'e_size' in complet_list:
        assert getattr(data, 'node_size', None) is not None, \
            "Expected input Data to have 'node_size' attribute"
    if 'e_log_size' in complet_list:
        assert getattr(data, 'log_size', None) is not None, \
            "Expected input Data to have a 'log_size' attribute"
    if 'e_perimeter' in complet_list:
        assert getattr(data, 'perimeter', None) is not None, \
            "Expected input Data to have a 'perimeter' attribute"
    if 'e_log_perimeter' in complet_list:
        assert getattr(data, 'log_perimeter', None) is not None, \
            "Expected input Data to have a 'log_perimeter' attribute"
    
    if 'same_SP' in complet_list:
        assert getattr(data, 'same_SP', None) is not None, \
            "Expected input Data to have a 'same_SP' attribute"
    if 'interface_length' in complet_list:
        assert getattr(data, 'interface_length', None) is not None, \
            "Expected input Data to have a 'interface_length' attribute"
    if 'centroid_dir' in complet_list:
        assert getattr(data, 'centroid_dir', None) is not None, \
            "Expected input Data to have a 'centroid_dir' attribute"
    if 'centroid_dist' in complet_list:
        assert getattr(data, 'centroid_dist', None) is not None, \
            "Expected input Data to have a 'centroid_dist' attribute"

    f_list = []
    topology_f_list = []
    edge_mask = getattr(data,"mask_edge",None)
    if 'e_size' in complet_list:
        f = torch.cat((data.node_size[se].squeeze().transpose(1,0),
            data.node_size[se].squeeze().flip(0).transpose(1,0)),dim=0)
        if norm_val is not None:
            f = (f-norm_val["node_size"][0])/(norm_val["node_size"][1]+1e-8)
        f_list.append(f)
        if 'e_size' in topology_keys:
            topology_f_list.append(f)
    if 'e_log_size' in complet_list:
        f = torch.cat((data.log_size[se].squeeze().transpose(1,0),
            data.log_size[se].squeeze().flip(0).transpose(1,0)), dim=0)
        if norm_val is not None:
            f = (f-norm_val["log_size"][0])/(norm_val["log_size"][1]+1e-8)
        f_list.append(f)
        if 'e_log_size' in topology_keys:
            topology_f_list.append(f)

    if 'same_SP' in complet_list:
        if edge_mask is not None:
            data.same_SP = data.same_SP[edge_mask]
        f_list.append(torch.cat((data.same_SP,data.same_SP),dim=0).unsqueeze(1))
        if 'same_SP' in topology_keys:
            topology_f_list.append(f)
        data.same_SP = None

    if 'e_perimeter' in complet_list:
        f = torch.cat((data.perimeter[se].squeeze().transpose(1,0),
            data.perimeter[se].squeeze().flip(0).transpose(1,0)), dim=0)
        if norm_val is not None:
            f = (f-norm_val["perimeter"][0])/(norm_val["perimeter"][1]+1e-8)
        f_list.append(f)
        if 'e_perimeter' in topology_keys:
            topology_f_list.append(f)

    if 'e_log_perimeter' in complet_list:
        f = torch.cat((data.log_perimeter[se].squeeze().transpose(1,0),
            data.log_perimeter[se].squeeze().flip(0).transpose(1,0)), dim=0)
        if norm_val is not None:
            f = (f-norm_val["log_perimeter"][0])/(norm_val["log_perimeter"][1]+1e-8)
        f_list.append(f)
        if 'e_log_perimeter' in topology_keys:
            topology_f_list.append(f)
    
    if 'interface_length' in complet_list:
        if edge_mask is not None:
            data.interface_length = data.interface_length[edge_mask]
        f = torch.cat((data.interface_length,data.interface_length),dim=0)
        if f.dim()==1: # packing and unpacking squeeze the tensors
            f = f.unsqueeze(1)
        if norm_val is not None:
            f = (f-norm_val["interface_length"][0])/(norm_val["interface_length"][1]+1e-8)
        f_list.append(f)
        if 'interface_length' in topology_keys:
            topology_f_list.append(f)
        data.interface_length = None

    if 'centroid_dir' in complet_list:
        if edge_mask is not None:
            data.centroid_dir = data.centroid_dir[edge_mask]
        f = torch.cat((data.centroid_dir, -data.centroid_dir), dim=0)
        f_list.append(f)
        if 'centroid_dir' in topology_keys:
            topology_f_list.append(f)
        data.centroid_dir = None

    if 'centroid_dist' in complet_list:
        if edge_mask is not None:
            data.centroid_dist = data.centroid_dist[edge_mask]
        f = torch.cat((data.centroid_dist, data.centroid_dist), dim=0)
        if f.dim()==1:# packing and unpacking squeeze the tensors
            f = f.unsqueeze(1)
        if norm_val is not None:
            f = (f-norm_val["centroid_dist"][0])/(norm_val["centroid_dist"][1]+1e-8)
        f_list.append(f)
        if 'centroid_dist' in topology_keys:
            topology_f_list.append(f)
        data.centroid_dist = None

    # Update the edge_index with j->i edges
    #data.edge_index = torch.cat((se, se.flip(0)), dim=1)
    data = to_undirected(data)
    data.mask_edge = None

    # Update all edge features into edge_attr and remove all other
    # edge_<key> to save memory
    for k in ['edge_attr'] + data.edge_keys:
        data[k] = None
    if len(f_list) > 0:
        data.edge_attr = torch.cat(f_list, dim=1)
    if len(topology_f_list) > 0:
        data.topology = torch.cat(topology_f_list,dim=1)
    return data

def to_undirected(data):
    data.edge_index = torch.cat((data.edge_index, data.edge_index.flip(0)), dim=1)
    for k,val in data.items():
        if isinstance(val,Data):
            val = to_undirected(val)
    return data

class OnTheFlyVerticalEdgeFeatures(Transform):
    """Compute edge features "on-the-fly" for all vertical edges of the
    NAG levels.

    Optionally build some edge features that can be recovered from the
    source and target node attributes.

    Note: this transform is intended to be called after all sampling
    transforms, to mitigate compute and memory impact of vertical
    edges.

    The supported feature keys are the following:
      - centroid_dir: unit-normalized direction between the child
        centroid and the parent centroid
      - centroid_dist: distance between the child and parent centroids
      - normal_angle: cosine of the angle between the child and parent
        normals
      - log_length: parent/child log length ratio
      - log_surface: parent/child log surface ratio
      - log_volume: parent/child log volume ratio
      - log_size: parent/child log size ratio

    :param keys: List(str)
        Features to be computed. Attributes will be saved under `<key>`
    :param use_mean_normal: bool
        Whether the 'normal' or the 'mean_normal' segment attribute
        should be used for computing normal-related edge features
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, keys=None, use_mean_normal=False):
        raise NotImplementedError("Not updated for image use")
        super().__init__()
        self.keys = sanitize_keys(keys, default=ON_THE_FLY_VERTICAL_FEATURES)
        self.use_mean_normal = use_mean_normal

    def _process(self, nag):
        for i_level in range(1, nag.num_levels):
            data_child = nag[i_level - 1]
            data_parent = nag[i_level]

            # For level-0 pixels, we artificially set 'mean_normal' from
            # 'normal', if need be
            if self.use_mean_normal and i_level == 1:
                if getattr(data_child, 'mean_normal', None):
                    data_child.mean_normal = getattr(data_child, 'normal', None)

            nag._list[i_level - 1] = _on_the_fly_vertical_edge_features(
                data_child,
                data_parent,
                keys=self.keys,
                use_mean_normal=self.use_mean_normal)
        return nag


def _on_the_fly_vertical_edge_features(
        data_child, data_parent, keys=None, use_mean_normal=False):
    """Compute edge features for a vertical graph, given child and
    parent nodes.
    """
    keys = sanitize_keys(keys, default=ON_THE_FLY_VERTICAL_FEATURES)

    if len(keys) == 0:
        return data_child

    normal_key = 'mean_normal' if use_mean_normal else 'normal'

    # Recover the parent index of each child node
    idx = data_child.super_index
    assert idx is not None, \
        "Expected input child Data to have a 'super_index' attribute"

    for d in [data_child, data_parent]:
        if 'normal_angle' in keys:
            assert getattr(d, normal_key, None) is not None, \
                f"Expected input Data to have a '{normal_key}' attribute"
        if 'log_length' in keys:
            assert getattr(d, 'log_length', None) is not None, \
                "Expected input Data to have a 'log_length' attribute"
        if 'log_surface' in keys:
            assert getattr(d, 'log_surface', None) is not None, \
                "Expected input Data to have a 'log_surface' attribute"
        if 'log_volume' in keys:
            assert getattr(d, 'log_volume', None) is not None, \
                "Expected input Data to have a 'log_volume' attribute"
        if 'log_size' in keys:
            assert getattr(d, 'log_size', None) is not None, \
                "Expected input Data to have a 'log_size' attribute"

    f_list = []

    if 'centroid_dir' in keys or 'centroid_dist' in keys:
        # Compute the distance and direction between the child and
        # parent segments' centroids
        ve_centroid_dir = data_parent.pos[idx] - data_child.pos
        ve_centroid_dist = ve_centroid_dir.norm(dim=1)
        ve_centroid_dir /= ve_centroid_dist.view(-1, 1)
        ve_centroid_dist = ve_centroid_dist.sqrt()

        # Sanity checks on normalized directions
        ve_centroid_dir[ve_centroid_dir.isnan()] = 0
        ve_centroid_dir = ve_centroid_dir.clip(-1, 1)

        if 'centroid_dir' in keys:
            f_list.append(ve_centroid_dir)

        if 'centroid_dist' in keys:
            f_list.append(ve_centroid_dist.view(-1, 1))

    if 'normal_angle' in keys:
        child_normal = getattr(data_child, normal_key, None)
        parent_normal = getattr(data_parent, normal_key, None)
        f = (child_normal * parent_normal[idx]).sum(dim=1).abs()
        f_list.append(f.view(-1, 1))

    if 'log_length' in keys:
        f = data_parent.log_length[idx] - data_child.log_length
        f_list.append(f.view(-1, 1))

    if 'log_surface' in keys:
        f = data_parent.log_surface[idx] - data_child.log_surface
        f_list.append(f.view(-1, 1))

    if 'log_volume' in keys:
        f = data_parent.log_volume[idx] - data_child.log_volume
        f_list.append(f.view(-1, 1))

    if 'log_size' in keys:
        f = data_parent.log_size[idx] - data_child.log_size
        f_list.append(f.view(-1, 1))

    # Stack all the vertical edge features into the child 'v_edge_attr'
    data_child.v_edge_attr = None
    if len(f_list) > 0:
        data_child.v_edge_attr = torch.cat(f_list, dim=1)

    return data_child

class NAGAddHopEdges(Transform):
    """ Add n hop edges to all NAG levels (but the image level).
    if the edges have attributes, the new edge will receive 0-features.

    Add directed graph (source < target) (assumed to be already true un edge_index)
    
    :param nb_hops: int or list of ints
        distances to witch search new neihgbors
        the distance is define as the shortest path between two node
        if 3 given will search for all node exacly at distance 3 and not 2
    """
    
    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, nb_hops=2):
        super().__init__()
        if isinstance(nb_hops,int):
            nb_hops = [nb_hops]    
        self.nb_hops = nb_hops

    def _process(self,nag):
        if max(self.nb_hops)<2:
            return nag

        nano = getattr(nag[0],'image',None) is None

        for i_level in range(1-nano, nag.num_levels):
            
            data = nag[i_level]
            
            if not data.has_edges:
                continue

            # Recover edges and attributes
            num_nodes = data.num_nodes
            edge_index = data.edge_index

            new_edges = all_hops_neihgbors(edge_index,self.nb_hops)
            
            # Update the edges and attributes
            data.edge_index = torch.cat((edge_index,new_edges),dim=1)
            nb_new_edges = new_edges.shape[1]
            if getattr(data,'edge_attr',None) is not None:
                if data.edge_attr.dim() == 1:
                    data.edge_attr = data.edge_attr.unsqueeze(1)
                data.edge_attr = torch.cat([data.edge_attr,torch.zeros((nb_new_edges,data.edge_attr.shape[1]),device=data.device,dtype=data.edge_attr.dtype)],dim=0)
            if getattr(data,'topology',None) is not None:
                if data.topology.dim() == 1:
                    data.topology = data.topology.unsqueeze(1)
                data.topology = torch.cat([data.topology,torch.zeros((nb_new_edges,data.topology.shape[1]),device=data.device,dtype=data.topology.dtype)])
            if getattr(data,'mask_edge',None) is not None:
                data.mask_edge = torch.cat([data.mask_edge,torch.ones((nb_new_edges,),device=data.device,dtype=torch.bool)])
        
        return nag

def all_hops_neihgbors(edge_index,N):
    if isinstance(N,int):
        N = [N]
    max_dist = max(N)
    if max_dist<2:
        return torch.empty((2,0),dtype=edge_index.dtype,device=edge_index.device)
    vertices = torch.unique(edge_index)
    nb_vertices = len(vertices)
    neighbors = [[] for _ in range(nb_vertices)]
    for i in range(edge_index.shape[1]):
        neighbors[edge_index[0,i]].append(edge_index[1,i].item())
        neighbors[edge_index[1,i]].append(edge_index[0,i].item())
    out = torch.empty((2,1000),dtype=edge_index.dtype,device=edge_index.device)
    idx = 0
    mask = torch.ones(nb_vertices,dtype=torch.bool)
    neighbor = [0 for _ in range(nb_vertices)]
    dist = [0 for _ in range(nb_vertices)]
    tete=0
    queue=0
    for v in vertices:
        mask[:] = 1
        mask[v] = 0
        for n in neighbors[v]:
            neighbor[tete] = n
            dist[tete] = 1
            tete += 1
            mask[n] = 0
        while(tete>queue):
            n = neighbor[queue]
            d = dist[queue]
            queue += 1
            for new_n in neighbors[n]:
                if mask[new_n]:
                    mask[new_n] = 0
                    if d+1 < max_dist:
                        neighbor[tete] = new_n
                        dist[tete] = d+1
                        tete += 1
                    if d+1 in N and new_n > v:
                        out[0,idx]=v
                        out[1,idx]=new_n
                        idx += 1;
                        if idx >= out.shape[1]:
                            out = torch.cat((out,torch.empty((2,out.shape[1]),dtype=edge_index.dtype,device=edge_index.device)),dim=1)
        tete = 0
        queue = 0
    out = out[:,:idx]
    return out

class NAGAddSelfLoops(Transform):
    """Add self-loops to all NAG levels having a horizontal graph. If
    the edges have attributes, the self-loops will receive 0-features.
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    
    def __init__(self, keys=None):
        super().__init__()
        self.keys = sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
        self.global_file_name = None

    def _process(self, nag):
        nano = getattr(nag[0],'image',None) is None 
        if self.global_file_name is not None:
            name,ext = osp.splitext(self.global_file_name)
            norm_val = torch.load(name+"_"+nag.device.type+ext)
            if nano:
                norm_val = norm_val[1:]
        else:
            norm_val = [None] * nag.num_levels

        for i_level in range(1-nano, nag.num_levels):
            # Skip if the level has no horizontal graph
            
            data = nag[i_level]
            
            if not data.has_edges:
                continue

            # Recover edges and attributes
            num_nodes = data.num_nodes
            edge_index = data.edge_index

            data.raise_if_edge_keys()

            # Add self-loops
            edge_index,_ = add_self_loops(
                edge_index,
                edge_attr=None,
                num_nodes=num_nodes,
                fill_value=0)
            
            # Update the edges and attributes
            data.edge_index = edge_index
            if getattr(data,'edge_attr',None) is not None:
                fill_value = self_loop_features(data,self.keys,norm_val[i_level])
                data.edge_attr = torch.cat([data.edge_attr,fill_value],dim=0)
            if getattr(data,'topology',None) is not None:
                data.topology = torch.cat([data.topology,torch.zeros((num_nodes,data.topology.shape[1]),device=data.device,dtype=data.topology.dtype)])
            if getattr(data,'mask_edge',None) is not None:
                data.mask_edge = torch.cat([data.mask_edge,torch.ones((num_nodes,),device=data.device,dtype=torch.bool)])
        return nag

def self_loop_features(data,keys=None,norm_val=None):
    """ create the tensor of feature for self loop edges """

    keys = sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)

    # Recover the edges between the segments
    if 'e_size' in keys:
        assert getattr(data, 'node_size', None) is not None, \
            "Expected input Data to have 'node_size' attribute to put it in edge_attr"
    if 'e_log_size' in keys:
        assert getattr(data, 'log_size', None) is not None, \
            "Expected input Data to have a 'log_size' attribute to put it in edge_attr"
    if 'e_perimeter' in keys:
        assert getattr(data, 'perimeter', None) is not None, \
            "Expected input Data to have a 'perimeter' attribute to put it in edge_attr"
    if 'e_log_perimeter' in keys:
        assert getattr(data, 'log_perimeter', None) is not None, \
            "Expected input Data to have a 'log_perimeter' attribute to put it in edge_attr"
    
    #if 'interface_length' in keys:
    #    assert getattr(data, 'perimeter', None) is not None, \
    #        "Expected input Data to have a 'perimeter' attribute to put 'interface_length' in self loop edge_attr"

    f_list = []
    if 'e_size' in keys:
        f = torch.cat((data.node_size.unsqueeze(1),data.node_size.unsqueeze(1)),dim=1)
        if norm_val is not None:
            f = (f-norm_val["node_size"][0])/(norm_val["node_size"][1]+1e-8)
        f_list.append(f)

    if 'e_log_size' in keys:
        f = torch.cat((data.log_size,data.log_size), dim=1)
        if norm_val is not None:
            f = (f-norm_val["log_size"][0])/(norm_val["log_size"][1]+1e-8)
        f_list.append(f)

    if 'same_SP' in keys: # a super pixel is allways fused with itself
        f_list.append(torch.zeros(data.num_nodes,device=data.device).unsqueeze(1))
    
    if 'e_perimeter' in keys:
        f = torch.cat((data.perimeter,data.perimeter), dim=1)
        if norm_val is not None:
            f = (f-norm_val["perimeter"][0])/(norm_val["perimeter"][1]+1e-8)
        f_list.append(f)

    if 'e_log_perimeter' in keys:
        f = torch.cat((data.log_perimeter,data.log_perimeter), dim=1)
        if norm_val is not None:
            f = (f-norm_val["log_perimeter"][0])/(norm_val["log_perimeter"][1]+1e-8)
        f_list.append(f)
    
    if 'interface_length' in keys: # the interface of a superpixel and itself is its perimeter
        if hasattr(data,"perimeter"):
            f = data.perimeter
            if f.dim()==1: # packing and unpacking squeeze the tensors
                f = f.unsqueeze(1)
            if norm_val is not None: # no perimeter normalization value
                f = (f-norm_val["perimeter"][0])/(norm_val["perimeter"][1]+1e-8)
        else:
            f = torch.zeros([data.num_nodes,1],device=data.device)
        f_list.append(f)
    
    # centroid distance and direction are null
    if 'centroid_dir' in keys: 
        f = torch.zeros((data.num_nodes,2),device=data.device)
        f_list.append(f)

    if 'centroid_dist' in keys:
        f = torch.zeros(data.num_nodes,device=data.device).unsqueeze(1)
        f_list.append(f)

    if len(f_list)>0:
        out = torch.cat(f_list,dim=1)
    else:
        out = None

    return out

class NodeSize(Transform):
    """Compute the number of `low`-level elements are contained in each
    segment, at each above-level. Results are save in the `node_size`
    attribute of the corresponding Data objects.

    Note: `low=-1` is accepted when level-0 has a `sub` attribute
    (ie level-0 points are themselves segments of `-1` level absent
    from the NAG object).

    :param low: int
        Level whose elements we want to count
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, low=0):
        super().__init__()
        assert isinstance(low, int) and low >= -1
        self.low = low

    def _process(self, nag):
        for i_level in range(self.low + 1, nag.num_levels):
            nag[i_level].node_size = nag.get_sub_size(i_level, low=self.low)
        return nag

class DropEdges(Transform):
    """ randomly drop edges at each level

    :param p: float or list of float in [0,1] (default=0.5)
        probability of drop (len(list) = number of level)
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, p=0.5):
        super().__init__()
        self.p = p

    def _process(self, nag):
        if isinstance(self.p,float):
            self.p = [self.p]*nag.num_levels

        for i_level in range(nag.num_levels):
            data = nag[i_level]
            # assert not is_trimmed(data.edge_index), "edge drop has to be done before create undirected graph (the mask is saved for future filter in data.mask_edge)"
            edge_index,edge_mask = dropout_edge(data.edge_index,p=self.p[i_level]) # booleen use less memory 
            data.edge_index = edge_index # update new_edge
            data.mask_edge = edge_mask # save mask for future filter
            
            if data.edge_attr is not None:
                data.edge_attr = data.edge_attr[edge_mask]
                if getattr(data,'topology',None) is not None:
                    data.topology = data.topology[edge_mask]
            if hasattr(data,"interfaces"):
                edge_mask = edge_mask.nonzero().squeeze()
                data.interfaces = update_shapes(edge_mask,data.interfaces)
        return nag

class DropNodes(Transform):
    """ randomly drop nodes at each level 
        except the ones with image attribute

    :param p: float or list of float in [0,1] (default=0.5)
        fraction of node to remove (len(list) = number of level)
        Put null or negative to avoid drop
    :param by_size: boolean
        weight probability by node size
    :param by_class: boolean
        weight probability by inverse classe frecency
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, p=0.5, by_size=False, by_class=False):
        super().__init__()
        self.p = p
        self.by_size = by_size
        self.by_class = by_class

    def _process(self, nag):
        if isinstance(self.p,float):
            self.p = [self.p]*(nag.num_levels-1)
        
        assert len(self.p) == (nag.num_levels-1)

        device = nag.device

        for i_level in range(nag.num_levels-1,0,-1):

            if self.p[i_level - 1] <= 0 or hasattr(nag[i_level],'image') or hasattr(nag[i_level],'label'):
                continue

            # Prepare sampling
            num_nodes = nag[i_level].num_nodes
            num_keep = num_nodes - int(num_nodes * self.p[i_level - 1])
            
            # Initialize all segments with the same weights
            weights = torch.ones(num_nodes, device=device)


            # Compute per-segment weights solely based on the segment
            # size. This is biased towards preserving large segments in
            # the sampling
            if self.by_size:
                node_size = nag.get_sub_size(i_level, low=0)
                size_weights = node_size ** 0.333
                size_weights /= size_weights.sum()
                weights += size_weights

            # Compute per-class weights based on class frequencies in
            # the current NAG and give a weight to each segment
            # based on the rarest class it contains. This is biased
            # towards sampling rare classes
            if self.by_class and nag[i_level].y is not None:
                counts = nag[i_level].y.sum(dim=0).sqrt()
                scores = 1 / (counts + 1)
                scores /= scores.sum()
                mask = nag[i_level].y.gt(0)
                class_weights = (mask * scores.view(1, -1)).max(dim=1).values
                class_weights /= class_weights.sum()
                weights += class_weights.squeeze()

            # Normalize the weights again, in case size or class weights
            # were added
            weights /= weights.sum()

            # Generate sampling indices
            idx = torch.multinomial(weights, num_keep, replacement=False)
            
            # Select the nodes and update the NAG structure accordingly
            nag = nag.select(i_level, idx)

        return nag

