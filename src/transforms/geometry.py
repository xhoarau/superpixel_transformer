import torch
from src.data import NAG
from src.transforms import Transform
from src.utils.geometry import rodrigues_rotation_matrix
from torch_scatter import scatter


__all__ = [
    'CenterPosition', 'RandomRotateImage', 'RandomAnisotropicScale',
    'RandomAxisFlip', 'Diameter']

class Diameter(Transform):
    """ cumpute the diameter of each superpixel for each level
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    
    def _process(self,nag):

        for i_level in range(nag.num_levels):
            is_last_level = (i_level == nag.num_levels -1)
            data = nag[i_level]
            if not is_last_level:
                min_segment = scatter(data.pos, data.super_index, dim=0, dim_size=data.super_index.max()+1, reduce='min')
                max_segment = scatter(data.pos, data.super_index, dim=0, dim_size=data.super_index.max()+1, reduce='max')
                nag[i_level].diameter_parent = (max_segment - min_segment).max(dim=1).values
                nag[i_level+1].diameter = (max_segment - min_segment).max(dim=1).values
            else:
                min_ = data.pos.min(dim=0).values
                max_ = data.pos.max(dim=0).values
                nag[i_level].diameter_parent = (max_ - min_).max().unsqueeze(0)

        return nag
    

class CenterPosition(Transform):
    """Center the position of all nodes of all levels of a NAG around
    their level-0 centroid.
    """
    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def _process(self, nag):
        offset = nag[0].pos.mean(dim=0)
        for i_level in range(nag.num_levels):
            nag[i_level].pos -= offset
            if getattr(nag[i_level],'bounding_box',None) is not None:
                nag[i_level].bounding_box -= offset
        return nag


class RandomRotateImage(Transform):
    """Rotate the image of 90°,180° or 270° randomly for the convolution augmentation
    Rotate the NAG around the vertical axis, with a random angle (not only 90°,180° or 270°). 
    The angle is picked following a uniform distribution within a specified
    range.

    If the nodes have a 'centroid_dir' attribute, we also rotate those accordingly.

    Warning: any other absolute orientation-related attributes beside
    `pos`, 'centroid_dir' may be broken by this transform.

    :param theta: float (degrees)
        The random rotation angle will be uniformly picked within
        [-abs(theta), abs(theta)]
    :param image: whether the image should be rotate
    :param graph: whether the graph should be rotate
    :param as_image: whether the graph should rotate the same angle as the image
        if True ignore the parameter theta
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, theta=180, image=True, graph=True, as_image=False):
        super().__init__()
        assert isinstance(theta, (int, float))
        self.theta = float(abs(theta))
        self.image = image
        self.graph = graph
        self.as_image = as_image and image and graph

    def _process(self, nag):
        device = nag.device

        # Generate random number of rotation for the image
        rotation = torch.randint(4,(1,),device=device)

        # Generate the random rotation angle
        if self.as_image:
            theta = torch.pi * rotation / 2 
        else:
            theta = torch.pi * (torch.rand(1, device=device) * 2 * self.theta - self.theta) / 180
        
        # Generate Rotation matrix
        cos,sin = torch.cos(theta),torch.sin(theta)
        R = torch.tensor([[cos,sin],[-sin,cos]],device=device)
        
        # select rotation center for all levels
        center = nag[0].pos.mean(0)

        # Rotate the nodes at each level and image if needed
        for i_level in range(nag.num_levels):
            if hasattr(nag[i_level],"flip"):
                raise Warning("flip should be done after rotate for image to superpixel consitency")
            if self.image and hasattr(nag[i_level],'image'):
                nag[i_level].image = nag[i_level].image.rot90(k=rotation.item(),dims=[-2,-1])
                if hasattr(nag[i_level],'label'):
                    nag[i_level].label = nag[i_level].label.rot90(k=rotation.item())
                nag[i_level].rotation = rotation.item()
            
            if self.graph:
                nag[i_level].pos = ((nag[i_level].pos-center) @ R.T) + center

                # If the nodes have a `normal` or 'mean_normal' attribute,
                # we also adapt their orientations accordingly
                for k in ['centroid_dir']:
                    if getattr(nag[i_level], k, None) is not None:
                        nag[i_level][k] = nag[i_level][k] @ R.T

                if getattr(nag[i_level],'bounding_box', None) is not None:
                    nag[i_level].bounding_box[:,:2] = ((nag[i_level].bounding_box[:,:2]-center) @ R.T) + center 
                    nag[i_level].bounding_box[:,2:] = ((nag[i_level].bounding_box[:,2:]-center) @ R.T) + center        
        return nag

class RandomAnisotropicScale(Transform):
    """Scales node positions by a randomly sampled factor ``s1, s2, s3``
    within a given interval, *e.g.*, resulting in the following
    transformation matrix

    .. math::
        \left[
        \begin{array}{ccc}
            s1 & 0 & 0 \\
            0 & s2 & 0 \\
            0 & 0 & s3 \\
        \end{array}
        \right]

    for three-dimensional positions.

    If the nodes have a `normal` attribute, we also reorient those
    accordingly, while preserving their unit-norm.

    Warning: any other absolute orientation-related attributes beside
    `pos` and `normal` may be broken by this transform.

    Credit: https://github.com/torch-points3d/torch-points3d

    :param delta: float or List(float)
        Scaling will be uniformly sampled in [-delta, delta]. If a
        3-element list may be passed to scale X, Y and Z differently.
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, delta=0.2):
        raise NotImplementedError(f"transform {self.__class__.__name__} not updated for images")
        super().__init__()
        assert isinstance(delta, (float, int)) or isinstance(delta, (tuple, list))
        if isinstance(delta, (float, int)):
            delta = [float(delta)] * 3
        assert len(delta) == 3
        self.delta = torch.tensor(delta).abs().view(1, -1)

    def _process(self, nag):
        # Generate the random scales
        scale = 1 + (torch.rand(1) * 2 * self.delta - self.delta).to(nag.device)

        for i_level in range(nag.num_levels):
            nag[i_level].pos = nag[i_level].pos * scale

            # If the nodes have a `normal` or 'mean_normal' attribute,
            # we also adapt their orientations accordingly
            for k in ['normal', 'mean_normal']:
                if getattr(nag[i_level], k, None) is not None:
                    nag[i_level][k] = self._scale_normal(nag[i_level][k], scale)

            if getattr(nag[i_level], 'edge_attr', None) is not None:
                edge_attr = nag[i_level].edge_attr
                assert edge_attr.shape[1] == 7, \
                    "Expected exactly 7 features in `edge_attr`, generated " \
                    "with `_minimalistic_horizontal_edge_features`"
                edge_attr[:, :3] *= scale
                edge_attr[:, 3:] *= scale.norm()  # std_off and mean_dist are scaled by the scaling norm, slightly incorrect for std_off...
                nag[i_level].edge_attr = edge_attr

        return nag

    @staticmethod
    def _scale_normal(normal, scale):
        return torch.nn.functional.normalize(normal * scale, dim=1)


class RandomAxisFlip(Transform):
    """Flip the node positions and the image wrt one of the XY axes, with a specified
    probability. This transform is not very modular because it is
    intended to be composed with `RandomTiltAndRotate` for richer
    geometric augmentations.

    If the nodes have a `centroid_dir' attribute, we also
    flip those accordingly.

    Warning: any other absolute orientation-related attributes beside
    `pos`, 'centroid_dir' may be broken by this transform.

    :param axis: int in {0,1}
        axis to flip by
    :param p: float
        Probability of flip
    :param image: whether the image should be fliped
    :param graph: whether the graph should be fliped
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, axis=0, p=0.5, image=True, graph=True):
        super().__init__()
        assert isinstance(axis, int) and (axis==0 or axis==1)
        assert isinstance(p, float)
        self.axis = axis
        self.p = p
        self.image = image
        self.graph = graph

    def _process(self, nag):
        if torch.rand(1, device=nag.device) < self.p:
            return nag

        center = nag[0].pos.mean(0)

        for i_level in range(nag.num_levels):

            if self.image and hasattr(nag[i_level],'image'):
                nag[i_level].image = nag[i_level].image.flip(self.axis+1)
                if hasattr(nag[i_level],'label'):
                    nag[i_level].label = nag[i_level].label.flip(self.axis)
                if getattr(nag[i_level],'flip',None) is None:
                    nag[i_level].flip = [self.axis]
                else:
                    nag[i_level].flip.append(self.axis)
            
            if self.graph:
                nag[i_level].pos[:, self.axis] = (-1 * (nag[i_level].pos[:, self.axis]-center[self.axis]))+center[self.axis]

                # If the nodes have a `normal` or 'mean_normal' attribute,
                # we also adapt their orientations accordingly
                for k in ['centroid_dir']:
                    if getattr(nag[i_level], k, None) is not None:
                        nag[i_level][k] = nag[i_level][k][:,self.axis] * -1

        return nag
