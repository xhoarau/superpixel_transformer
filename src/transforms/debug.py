import logging
from src.data import NAG
from src.transforms import Transform


log = logging.getLogger(__name__)


__all__ = ['HelloWorld','Debug']

class Debug(Transform):
    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, msg=None, check_nan=False,check_inf=False):
        super().__init__()
        self.msg = msg
        self.check_nan = check_nan
        self.check_inf = check_inf

    def _process(self, nag):
        if self.msg is not None:
            print("debuging : ",self.msg)
        nag.debug(self.check_nan,self.check_inf)
        return nag

class HelloWorld(Transform):
    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def _process(self, nag):
        log.info("\n**** Hello World ! ****\n")
        return nag
