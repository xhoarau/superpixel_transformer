import sys
import os.path as osp
import torch
import numpy as np
from torch_scatter import scatter_sum, scatter_mean
from src.transforms import Transform
from src.data import Data, Cluster, NAG
from src.utils.cpu import available_cpu_count

dependencies_folder = osp.dirname(osp.dirname(osp.abspath(__file__)))
sys.path.append(dependencies_folder)
sys.path.append(osp.join(dependencies_folder, "dependencies/grid_graph/python/bin"))
sys.path.append(osp.join(dependencies_folder, "dependencies/parallel_cut_pursuit/python/wrappers"))

from grid_graph import edge_list_to_forward_star
from cp_d0_dist import cp_d0_dist

from fast_slic.avx2 import SlicAvx2
from src.utils import assign2graph

__all__ = ['CutPursuitPartition','Hybrid_SLICPartition','SLICPartition']


class CutPursuitPartition(Transform):
    """Partition Data using cut-pursuit.

    :param regularization: float or List(float)
    :param spatial_weight: float or List(float)
        Weight used to mitigate the impact of the pixel position in the
        partition. The larger, the less spatial coordinates matter. This
        can be loosely interpreted as the inverse of a maximum
        superpixel radius. If a list is passed, it must match the length
        of `regularization`
    :param cutoff: float or List(float)
        Minimum number of pixels in each cluster. If a list is passed,
        it must match the length of `regularization`
    :param parallel: bool
        Whether cut-pursuit should run in parallel
    :param iterations: int
        Maximum number of iterations for each partition
    :param verbose: bool
    """

    _IN_TYPE = Data
    _OUT_TYPE = NAG
    _MAX_NUM_EGDES = 4294967295 # 2^32 - 1
    _NO_REPR = ['verbose', 'parallel']

    def __init__(
            self, regularization=5e-2, spatial_weight=1, feature_weight=1, cutoff=10,
            parallel=True, iterations=10, verbose=False): #, size_modulation=False):
        super().__init__()
        self.regularization = regularization
        self.spatial_weight = spatial_weight
        self.feature_weight = feature_weight
        self.cutoff = cutoff
        self.parallel = parallel
        self.iterations = iterations
        self.verbose = verbose
        #self.size_modulation = size_modulation

    def _process(self, data):
        # Sanity checks
        assert data.has_edges, \
            "Cannot compute partition, no edges in Data"
        assert data.num_nodes < np.iinfo(np.uint32).max, \
            "Too many nodes for `uint32` indices"
        assert data.num_edges < np.iinfo(np.uint32).max, \
            "Too many edges for `uint32` indices"
        assert isinstance(self.regularization, (int, float, list)), \
            "Expected a scalar or a List"
        assert isinstance(self.cutoff, (int, list)), \
            "Expected an int or a List"
        assert isinstance(self.spatial_weight, (int, float, list)), \
            "Expected a scalar or a List"
        assert isinstance(self.feature_weight, (int, float, list)), \
            "Expected a scalar or a List"

        # Trim the graph
        data = data.to_trimmed()

        # Initialize the hierarchical partition parameters. In particular,
        # prepare the output as list of Data objects that will be stored in
        # a NAG structure
        num_threads = available_cpu_count() if self.parallel else 1
        data.node_size = torch.ones(
            data.num_nodes, device=data.device, dtype=torch.long)  # level-0 pixels all have the same importance
        data_list = [data]
        regularization = self.regularization
        if not isinstance(regularization, list):
            regularization = [regularization]
        cutoff = self.cutoff
        if isinstance(cutoff, int):
            cutoff = [cutoff] * len(regularization)
        spatial_weight = self.spatial_weight
        if isinstance(spatial_weight, (float, int)):
            spatial_weight = [spatial_weight] * len(regularization)
        assert len(regularization) == len(cutoff) == len(spatial_weight)
        n_dim = data.pos.shape[1]
        n_feat = data.x.shape[1] if data.x is not None else 0
        feature_weight = self.feature_weight
        if isinstance(feature_weight, (float, int)):
            feature_weight = [feature_weight] * n_feat

        # Iteratively run the partition on the previous partition level
        for level, (reg, cut, sw) in enumerate(zip(
                regularization, cutoff, spatial_weight)):

            if self.verbose:
                print(
                    f'Launching partition level={level} reg={reg}, '
                    f'cutoff={cut}')

            # Recover the Data object on which we will run the partition
            d1 = data_list[level]

            # Exit if the graph contains only one node
            if d1.num_nodes < 2:
                break

            # User warning if the number of edges exceeds uint32 limits
            if d1.edge_index.shape[1] > self._MAX_NUM_EGDES and self.verbose:
                print(
                    f"WARNING: number of edges {d1.edge_index.shape[1]} "
                    f"exceeds the uint32 limit {self._MAX_NUM_EGDES}. Please"
                    f"update the cut-pursuit source code to accept a larger "
                    f"data type for `index_t`.")

            # Convert edges to forward-star (or CSR) representation
            source_csr, target, reindex = edge_list_to_forward_star(
                d1.num_nodes, d1.edge_index.T.contiguous().cpu().numpy())
            source_csr = source_csr.astype('uint32')
            target = target.astype('uint32')
            edge_weights = d1.edge_attr.cpu().numpy()[reindex] * reg \
                if d1.edge_attr is not None else reg
            # Recover attributes features from Data object
            pos_offset = d1.pos.mean(dim=0) 
            if d1.x is not None:
                x = torch.cat((d1.pos - pos_offset, d1.x), dim=1)
            else:
                x = d1.pos - pos_offset
            x = x.cpu().numpy().T
            node_size = d1.node_size.float().cpu().numpy()
            coor_weights = np.ones(n_dim + n_feat, dtype=np.float32)
            coor_weights[:n_dim] *= sw
            coor_weights[n_dim:] *= feature_weight
            
            # Normalise weight with std of data
            edge_weights = edge_weights * np.std(x)
            
            # Partition computation
            super_index, x_c, cluster, edges, times = cp_d0_dist(
                n_dim + n_feat, x, source_csr, target, edge_weights=edge_weights,
                vert_weights=node_size, coor_weights=coor_weights,
                min_comp_weight=cut, cp_dif_tol=1e-2, cp_it_max=self.iterations,
                split_damp_ratio=0.7, verbose=self.verbose,
                max_num_threads=num_threads, balance_parallel_split=False,
                compute_Time=True, compute_List=True, compute_Graph=True)
            
            if self.verbose:
                delta_t = (times[1:] - times[:-1]).round(2)
                print(f'Level {level} iteration times: {delta_t}')
                print(f'partition {level} done')

            # Save the super_index for the i-level
            super_index = torch.from_numpy(super_index.astype('int64'))
            d1.super_index = super_index

            # Save cluster information in another Data object. Convert
            # cluster-to-pixel indices in a CSR format
            size = torch.LongTensor([c.shape[0] for c in cluster])
            pointer = torch.cat([torch.LongTensor([0]), size.cumsum(dim=0)])
            value = torch.cat([
                torch.from_numpy(x.astype('int64')) for x in cluster])
            pos = torch.from_numpy(x_c[:n_dim].T) + pos_offset.cpu()
            x = torch.from_numpy(x_c[n_dim:].T)
            s = torch.arange(edges[0].shape[0] - 1).repeat_interleave(
                torch.from_numpy((edges[0][1:] - edges[0][:-1]).astype("int64")))
            t = torch.from_numpy(edges[1].astype("int64"))
            edge_index = torch.vstack((s, t))
            edge_attr = torch.from_numpy(edges[2] / reg)
            node_size = torch.from_numpy(node_size)
            node_size_new = scatter_sum(
                node_size.cuda(), super_index.cuda(), dim=0).cpu().long()
            d2 = Data(
                pos=pos, x=x, edge_index=edge_index, edge_attr=edge_attr,
                sub=Cluster(pointer, value), node_size=node_size_new)

            # Trim the graph
            d2 = d2.to_trimmed()

            # Aggregate some pixel attributes into the clusters. This
            # is not performed dynamically since not all attributes can
            # be aggregated (eg 'edge_index', 'edge_attr'...)
            if 'y' in d1.keys:
                assert d1.y.dim() == 2, \
                    "Expected Data.y to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.y = scatter_sum(
                    d1.y.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            if 'pred' in d1.keys:
                assert d1.pred.dim() == 2, \
                    "Expected Data.pred to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.pred = scatter_sum(
                    d1.pred.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            # Add the l+1-level Data object to data_list and update the
            # l-level after super_index has been changed
            data_list[level] = d1
            data_list.append(d2)

            if self.verbose:
                print('\n' + '-' * 64 + '\n')

        # Create the NAG object
        nag = NAG(data_list)
        return nag

def extract_cluster_feature(cluster):
    return cluster["yx"],cluster["color"]

def extract_cluster(cluster_list):
    feature_list = list(map(extract_cluster_feature,cluster_list))
    pos,color = [[item[i] for item in feature_list] for i in range(2)]
    return torch.tensor(pos),torch.tensor(color)

class Hybrid_SLICPartition(Transform):
    """ operate SLIC for the first level and Cut Pursuit for all other level

    Slic implementation takes lab feature only
    Use edges at level 0 to create higher level edges, 
        if there exite an edge between a pixel of super-pixel i and an other pixel of superpixel j then an egde between i and j is created

    takes identical parameters as CutPursuitPartition but ignore first element in list

    :param regularization: float or List(float)
    :param spatial_weight: float or List(float)
        Weight used to mitigate the impact of the pixel position in the
        partition. The larger, the less spatial coordinates matter. This
        can be loosely interpreted as the inverse of a maximum
        superpixel radius. If a list is passed, it must match the length
        of `regularization`
    :param cutoff: float or List(float)
        Minimum number of pixels in each cluster. If a list is passed,
        it must match the length of `regularization`
    :param parallel: bool
        Whether cut-pursuit should run in parallel
    :param iterations: int
        Maximum number of iterations for each partition
    :param verbose: bool

    :param num_nodes: number of cluster done by slic
    :param compactness: shape of slic super-pixels (higher means more regular, square)
    """

    _IN_TYPE = Data
    _OUT_TYPE = NAG
    _MAX_NUM_EGDES = 4294967295 # 2^32 - 1
    _NO_REPR = ['verbose', 'parallel', 'slic']

    def __init__(
            self, regularization=5e-2, spatial_weight=1, feature_weight=1, cutoff=10,
            parallel=True, iterations=10, verbose=False, num_nodes=256, compactness=10): #, size_modulation=False):
        super().__init__()
        self.regularization = regularization
        self.spatial_weight = spatial_weight
        self.feature_weight = feature_weight
        self.cutoff = cutoff
        self.parallel = parallel
        self.iterations = iterations
        self.verbose = verbose

        self.num_nodes = num_nodes
        self.compactness = compactness

        self.slic = SlicAvx2(num_components=num_nodes, compactness=compactness, convert_to_lab=False)
        #self.size_modulation = size_modulation
 
    def _process(self, data):
        # Sanity checks
        assert data.has_edges, \
            "Cannot compute partition, no edges in Data"
        assert data.num_nodes < np.iinfo(np.uint32).max, \
            "Too many nodes for `uint32` indices"
        assert data.num_edges < np.iinfo(np.uint32).max, \
            "Too many edges for `uint32` indices"
        assert isinstance(self.regularization, (int, float, list)), \
            "Expected a scalar or a List"
        assert isinstance(self.cutoff, (int, list)), \
            "Expected an int or a List"
        assert isinstance(self.spatial_weight, (int, float, list)), \
            "Expected a scalar or a List"
        assert isinstance(self.feature_weight, (int, float, list)), \
            "Expected a scalar or a List"
        
        data = data.to_trimmed()
        
        low_edges = data.edge_index

        num_threads = available_cpu_count() if self.parallel else 1
        data.node_size = torch.ones(
            data.num_nodes, device=data.device, dtype=torch.long)  # level-0 pixels all have the same importance
        data_list = [data]
        regularization = self.regularization
        if not isinstance(regularization, list):
            regularization = [regularization]
        cutoff = self.cutoff
        if isinstance(cutoff, int):
            cutoff = [cutoff] * len(regularization)
        spatial_weight = self.spatial_weight
        if isinstance(spatial_weight, (float, int)):
            spatial_weight = [spatial_weight] * len(regularization)
        assert len(regularization) == len(cutoff) == len(spatial_weight)
        n_dim = data.pos.shape[1]
        n_feat = data.x.shape[1] if data.x is not None else 0
        feature_weight = self.feature_weight
        if isinstance(feature_weight, (float, int)):
            feature_weight = [feature_weight] * n_feat

        # Iteratively run the partition on the previous partition level
        for level, (reg, cut, sw) in enumerate(zip(
                regularization, cutoff, spatial_weight)):

            if level == 0:
                if self.verbose:
                    print(
                        f'Launching partition level={level} num_nodes={self.num_nodes}, '
                        f'compactness={self.compactness}')

                # Recover the Data object on which we will run the partition
                d1 = data_list[level]

                h,w = d1.image.shape[-2:]

                assignement = self.slic.iterate(((d1.x.reshape(h,w,3))*255+torch.tensor([0,128,128])).to(torch.uint8).numpy(),max_iter=self.iterations)
                assignement = torch.from_numpy(assignement.astype(np.int64))
                vertices,edge_index,size,edge_attr = assign2graph(assignement,low_edges)

                edge_index = edge_index.long()
                size = size.long()
                edge_attr = edge_attr.float()
                
                # check for too small super-pixels
                too_small = torch.where(size<cut)[0]
                if too_small.numel()>0:
                    step = torch.sqrt(torch.tensor((h*w)/self.num_nodes))
                    x = scatter_mean(d1.x,assignement.flatten().long(),0)
                    pos = scatter_mean(d1.pos,assignement.flatten().long(),0)
                    neighbors = [torch.cat((edge_index[0,edge_index[1]==idx],edge_index[1,edge_index[0]==idx])) for idx in too_small]
                    for idx,neighbor in zip(too_small,neighbors):
                        new_idx = (((x[idx]-x[neighbor])**2).sum(1)**(1/2) + (self.compactness/step) * ((pos[idx]-pos[neighbor])**2).sum(1)**(1/2)).argmin()
                        new_idx = neighbor[new_idx]
                        assignement[assignement == idx] = new_idx
                        assignement[assignement>idx] -= 1
                    vertices,edge_index,size,edge_attr = assign2graph(assignement,low_edges)
                    edge_index = edge_index.long()
                    size = size.long()
                    edge_attr = edge_attr.float()

                d1.super_index = assignement.flatten()

                edge_attr = edge_attr*d1.x.std() #edge attribute equivalent to CutPursuit

                idx = torch.argsort(assignement.flatten())
                value = torch.arange(h*w)[idx] 
                pointer = torch.cat([torch.LongTensor([0]), size.cumsum(dim=0)])

                #pos,x = extract_cluster(slic.slic_model.clusters[:num_vertices])
                x = scatter_mean(d1.x,assignement.flatten().long(),0)
                pos = scatter_mean(d1.pos,assignement.flatten().long(),0)
                node_size_new = size
            else:
                if self.verbose:
                    print(
                        f'Launching partition level={level} reg={reg}, '
                        f'cutoff={cut}')

                # Recover the Data object on which we will run the partition
                d1 = data_list[level]

                # Exit if the graph contains only one node
                if d1.num_nodes < 2:
                    break

                # User warning if the number of edges exceeds uint32 limits
                if d1.edge_index.shape[1] > self._MAX_NUM_EGDES and self.verbose:
                    print(
                        f"WARNING: number of edges {d1.edge_index.shape[1]} "
                        f"exceeds the uint32 limit {self._MAX_NUM_EGDES}. Please"
                        f"update the cut-pursuit source code to accept a larger "
                        f"data type for `index_t`.")

                # Convert edges to forward-star (or CSR) representation
                source_csr, target, reindex = edge_list_to_forward_star(
                    d1.num_nodes, d1.edge_index.T.contiguous().cpu().numpy())
                source_csr = source_csr.astype('uint32')
                target = target.astype('uint32')
                edge_weights = d1.edge_attr.cpu().numpy()[reindex] * reg \
                    if d1.edge_attr is not None else reg
                # Recover attributes features from Data object
                pos_offset = d1.pos.mean(dim=0) 
                if d1.x is not None:
                    x = torch.cat((d1.pos - pos_offset, d1.x), dim=1)
                else:
                    x = d1.pos - pos_offset
                x = x.cpu().numpy().T
                node_size = d1.node_size.float().cpu().numpy()
                coor_weights = np.ones(n_dim + n_feat, dtype=np.float32)
                coor_weights[:n_dim] *= sw
                coor_weights[n_dim:] *= feature_weight
                
                # Normalise weight with std of data
                edge_weights = edge_weights * np.std(x)
                
                # Partition computation
                super_index, x_c, cluster, edges, times = cp_d0_dist(
                    n_dim + n_feat, x, source_csr, target, edge_weights=edge_weights,
                    vert_weights=node_size, coor_weights=coor_weights,
                    min_comp_weight=cut, cp_dif_tol=1e-2, cp_it_max=self.iterations,
                    split_damp_ratio=0.7, verbose=self.verbose,
                    max_num_threads=num_threads, balance_parallel_split=False,
                    compute_Time=True, compute_List=True, compute_Graph=True)
            
                if self.verbose:
                    delta_t = (times[1:] - times[:-1]).round(2)
                    print(f'Level {level} iteration times: {delta_t}')
                    print(f'partition {level} done')

                # Save the super_index for the i-level
                super_index = torch.from_numpy(super_index.astype('int64'))
                d1.super_index = super_index

                # Save cluster information in another Data object. Convert
                # cluster-to-pixel indices in a CSR format
                size = torch.LongTensor([c.shape[0] for c in cluster])
                pointer = torch.cat([torch.LongTensor([0]), size.cumsum(dim=0)])
                value = torch.cat([
                    torch.from_numpy(x.astype('int64')) for x in cluster])
                pos = torch.from_numpy(x_c[:n_dim].T) + pos_offset.cpu()
                x = torch.from_numpy(x_c[n_dim:].T)
                s = torch.arange(edges[0].shape[0] - 1).repeat_interleave(
                    torch.from_numpy((edges[0][1:] - edges[0][:-1]).astype("int64")))
                t = torch.from_numpy(edges[1].astype("int64"))
                edge_index = torch.vstack((s, t))
                edge_attr = torch.from_numpy(edges[2] / reg)
                node_size = torch.from_numpy(node_size)
                node_size_new = scatter_sum(
                    node_size.cuda(), super_index.cuda(), dim=0).cpu().long()
            
            # creat new data

            d2 = Data(
                pos=pos, x=x, edge_index=edge_index, edge_attr=edge_attr,
                sub=Cluster(pointer, value), node_size=node_size_new)

            # Trim the graph
            d2 = d2.to_trimmed()

            # Aggregate some pixel attributes into the clusters. This
            # is not performed dynamically since not all attributes can
            # be aggregated (eg 'edge_index', 'edge_attr'...)
            if 'y' in d1.keys:
                assert d1.y.dim() == 2, \
                    "Expected Data.y to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.y = scatter_sum(
                    d1.y.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            if 'pred' in d1.keys:
                assert d1.pred.dim() == 2, \
                    "Expected Data.pred to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.pred = scatter_sum(
                    d1.pred.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            # Add the l+1-level Data object to data_list and update the
            # l-level after super_index has been changed
            data_list[level] = d1
            data_list.append(d2)

            if self.verbose:
                print('\n' + '-' * 64 + '\n')

        # Create the NAG object
        nag = NAG(data_list)
        return nag


class SLICPartition(Transform):
    """ operate SLIC for the first level and a substitut of SLIC taking advantage of the pixel assignement for the other levels

    Slic implementation takes lab feature only

    Use edges at level 0 to create higher level edges, 
        if there exite an edge between a pixel of super-pixel i and an other pixel of superpixel j then an egde between i and j is created

    :param num_nodes: number of cluster done by slic
    :param compactness: shape of slic super-pixels (higher means more regular, square)
    :param iterations: int
        Maximum number of iterations for each partition
    :param cutoff: list[int]
        Minimum size of super-pixels per level
    :param verbose: bool
    """

    _IN_TYPE = Data
    _OUT_TYPE = NAG
    _MAX_NUM_EGDES = 4294967295 # 2^32 - 1
    _NO_REPR = ['verbose', 'parallel', 'slic']

    def __init__(self, num_nodes=[256,64], compactness=10, thres=0.05, iterations=10, cutoff=[10,30], verbose=False): #, size_modulation=False):
        super().__init__()
        self.iterations = iterations
        self.verbose = verbose
        self.thres = thres
        self.cutoff = cutoff
        
        if isinstance(num_nodes,int):
            self.num_nodes = [num_nodes]
        else:
            assert isinstance(num_nodes,list) and isinstance(num_nodes[0],int), "num_nodes has to be int or list[int]"
            self.num_nodes = num_nodes

        self.compactness = compactness

        self.slic = SlicAvx2(num_components=num_nodes[0], compactness=compactness, convert_to_lab=False)
        #self.size_modulation = size_modulation
 
    def _process(self, data):
        # Sanity checks
        assert data.has_edges, \
            "Cannot compute partition, no edges in Data"
        assert data.num_nodes < np.iinfo(np.uint32).max, \
            "Too many nodes for `uint32` indices"
        assert data.num_edges < np.iinfo(np.uint32).max, \
            "Too many edges for `uint32` indices"
        assert isinstance(self.cutoff, (int, list)), \
            "Expected an int or a List"
        cutoff = self.cutoff
        if isinstance(cutoff, int):
            cutoff = [cutoff] * len(self.num_nodes)
        
        data = data.to_trimmed()

        data.node_size = torch.ones(
            data.num_nodes, device=data.device, dtype=torch.long)  # level-0 pixels all have the same importance
        data_list = [data]

        low_edges = data.edge_index
        
        w,h = data.image.shape[-2:]
        length = np.max((h,w))

        # Iteratively run the partition on the previous partition level
        for level,(nb_cluster,cut) in enumerate(zip(self.num_nodes,self.cutoff)):

            if level == 0:
                if self.verbose:
                    print(
                        f'Launching partition level={level} num_nodes={nb_cluster}, '
                        f'compactness={self.compactness}')

                # Recover the Data object on which we will run the partition
                d1 = data_list[level]

                assignement = self.slic.iterate(((d1.x.reshape(w,h,3))*255+torch.tensor([0,128,128])).to(torch.uint8).numpy(),max_iter=self.iterations)
                assignement = torch.from_numpy(assignement).long()
                vertices,edge_index,size,edge_attr = assign2graph(assignement,low_edges)
                edge_index = edge_index.long()
                size = size.long()
                edge_attr = edge_attr.float()
                
                # check for too small super-pixels
                too_small = torch.where(size<cut)[0]
                if too_small.numel()>0:
                    step = torch.sqrt(torch.tensor((h*w)/nb_cluster))
                    x = scatter_mean(d1.x,assignement.flatten().long(),0)
                    pos = scatter_mean(d1.pos,assignement.flatten().long(),0)
                    neighbors = [torch.cat((edge_index[0,edge_index[1]==idx],edge_index[1,edge_index[0]==idx])) for idx in too_small]
                    for idx,neighbor in zip(too_small,neighbors):
                        new_idx = (((x[idx]-x[neighbor])**2).sum(1)**(1/2) + (self.compactness/step) * ((pos[idx]-pos[neighbor])**2).sum(1)**(1/2)).argmin()
                        new_idx = neighbor[new_idx]
                        assignement[assignement == idx] = new_idx
                        assignement[assignement>idx] -= 1
                    vertices,edge_index,size,edge_attr = assign2graph(assignement,low_edges)
                    edge_index = edge_index.long()
                    size = size.long()
                    edge_attr = edge_attr.float()

                d1.super_index = assignement.flatten()

                edge_attr = edge_attr*d1.x.std() #edge attribute equivalent to computation with CutPursuit

                idx = torch.argsort(assignement.flatten())
                value = torch.arange(h*w)[idx] 
                pointer = torch.cat([torch.LongTensor([0]), size.cumsum(dim=0)])

                #pos,x = extract_cluster(slic.slic_model.clusters[:num_vertices])
                x = scatter_mean(d1.x,assignement.flatten().long(),0)
                pos = scatter_mean(d1.pos,assignement.flatten().long(),0)
                node_size_new = size
            else:
                if self.verbose:
                    print(
                        f'Launching partition level={level} num_nodes={nb_cluster}, '
                        f'compactness={self.compactness}')

                # Recover the Data object on which we will run the partition
                d1 = data_list[level]
                
                # init slic clusters 
                nb_node = d1.pos.shape[0]
                step = torch.sqrt(torch.tensor((h*w)/nb_cluster))
                x = torch.arange(step/2,h,step)
                y = torch.arange(step/2,w,step)
                super_node_pos = torch.round(torch.stack(torch.meshgrid(x,y)).flatten(-2)).long().transpose(1,0)
                
                nb_super_nodes = super_node_pos.shape[0]

                assignement = data_list[0].super_index
                for i in range(1,level):
                    assignement = data_list[i].super_index[assignement]
                assignement = assignement.reshape(w,h)
                
                cluster_color = d1.x[assignement[super_node_pos[:,1],super_node_pos[:,0]]]
                cluster_pos = d1.pos[assignement[super_node_pos[:,1],super_node_pos[:,0]]]
                
                step = step.long()
                for _ in range(self.iterations):
                    old_clusters = [cluster_color,cluster_pos]
                    low_windows = torch.maximum(super_node_pos-(2*step),torch.zeros(super_node_pos.shape,dtype=torch.long))
                    high_windows = torch.minimum(super_node_pos+(2*step),torch.ones(super_node_pos.shape,dtype=torch.long)*torch.tensor([h-1,w-1]).unsqueeze(0))
                    cluster_neighbor = [torch.unique(assignement[low_windows[i,1]:high_windows[i,1],low_windows[i,0]:high_windows[i,0]]) for i in range(nb_super_nodes)]
                    cluster_distances = [((cluster_color[i] - d1.x[cluster_neighbor[i]])**2).sum(1)**(1/2) + (self.compactness/step) * ((cluster_pos[i] - d1.pos[cluster_neighbor[i]])**2).sum(1)**(1/2) for i in range(nb_super_nodes)]

                    distances = torch.full((nb_node*nb_super_nodes,),torch.finfo(torch.float).max)
                    distances[torch.cat([cluster_neighbor[i] + i*nb_node for i in range(nb_super_nodes)])] = torch.cat(cluster_distances)

                    super_assignement = distances.reshape([nb_super_nodes,nb_node]).argmin(0)
                    unique = torch.unique(super_assignement)
                    old_clusters[0] = old_clusters[0][unique]
                    old_clusters[1] = old_clusters[1][unique]
                    nb_super_nodes = unique.shape[0]
                    if nb_super_nodes != unique.max()+1:
                        trad = torch.zeros(unique.max()+1,dtype=super_assignement.dtype)
                        trad[unique] = torch.arange(unique.shape[0])
                        super_assignement = trad[super_assignement]

                    cluster_color = scatter_mean(d1.x,super_assignement,0)
                    cluster_pos = scatter_mean(d1.pos,super_assignement,0)

                    super_node_pos = torch.round(cluster_pos*length).long()
                    if torch.abs(old_clusters[0]-cluster_color).sum() + torch.abs(old_clusters[1]-cluster_pos).sum() < self.thres:
                        break
                
                # Enforce connexity
                visited = torch.zeros(nb_node,dtype=torch.bool)
                new_assignement = torch.full_like(super_assignement,-1)
                edge_index = d1.edge_index
                new_comp_num = -1
                for i in range(nb_node):
                    if not visited[i]:
                        visited[i] = True
                        new_comp_num += 1
                        old_comp_num = super_assignement[i]
                        new_assignement[i] = new_comp_num
                        idx_list = [elem.item() for elem in edge_index[1][edge_index[0]==i]]
                        idx_list += [elem.item() for elem in edge_index[0][edge_index[1]==i]]
                        while len(idx_list)>0:
                            idx = idx_list.pop(0)
                            if super_assignement[idx]==old_comp_num and not visited[idx]:
                                idx_list += [elem.item() for elem in edge_index[1][edge_index[0]==idx]]
                                idx_list += [elem.item() for elem in edge_index[0][edge_index[1]==idx]]
                                visited[idx] = True
                                new_assignement[idx] = new_comp_num
                            if torch.all(visited):
                                break
                    if torch.all(visited):
                        break
                
                super_assignement = new_assignement
                d1.super_index = super_assignement
                assignement = data_list[0].super_index
                for i in range(1,level):
                    assignement = data_list[i].super_index[assignement]
                super_assignement = super_assignement[assignement].reshape(w,h).long()

                vertices,edge_index,size,edge_attr = assign2graph(super_assignement,low_edges)
                edge_index = edge_index.long()
                size = size.long()
                edge_attr = edge_attr.float()

                x = cluster_color
                pos = cluster_pos

                # check for too small super-pixels
                too_small = torch.where(size<cut)[0]
                if too_small.numel()>0:
                    neighbors = [torch.cat((edge_index[0,edge_index[1]==idx],edge_index[1,edge_index[0]==idx])) for idx in too_small]
                    for idx,neighbor in zip(too_small,neighbors):
                        new_idx = (((x[idx]-x[neighbor])**2).sum(1)**(1/2) + (self.compactness/step) * ((pos[idx]-pos[neighbor])**2).sum(1)**(1/2)).argmin()
                        new_idx = neighbor[new_idx]
                        super_assignement[super_assignement == idx] = new_idx
                        super_assignement[super_assignement>idx] -= 1
                    vertices,edge_index,size,edge_attr = assign2graph(super_assignement,low_edges)
                    edge_index = edge_index.long()
                    size = size.long()
                    edge_attr = edge_attr.float()
                    x = scatter_mean(d1.x,super_assignement.flatten().long(),0)
                    pos = scatter_mean(d1.pos,super_assignement.flatten().long(),0)
                
                edge_attr = edge_attr*d1.x.std() #edge attribute equivalent to CutPursuit

                idx = torch.argsort(super_assignement.flatten())
                value = torch.arange(h*w)[idx] 
                pointer = torch.cat([torch.LongTensor([0]), size.cumsum(dim=0)])

                node_size_new = size
  
            ### out if ###   
            # creat new data

            d2 = Data(
                pos=pos, x=x, edge_index=edge_index, edge_attr=edge_attr,
                sub=Cluster(pointer, value), node_size=node_size_new)

            # Trim the graph
            d2 = d2.to_trimmed()

            # Aggregate some pixel attributes into the clusters. This
            # is not performed dynamically since not all attributes can
            # be aggregated (eg 'edge_index', 'edge_attr'...)
            if 'y' in d1.keys:
                assert d1.y.dim() == 2, \
                    "Expected Data.y to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.y = scatter_sum(
                    d1.y.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            if 'pred' in d1.keys:
                assert d1.pred.dim() == 2, \
                    "Expected Data.pred to hold `(num_nodes, num_classes)` " \
                    "histograms, not single labels"
                d2.pred = scatter_sum(
                    d1.pred.cuda(), d1.super_index.cuda(), dim=0).cpu()
                torch.cuda.empty_cache()

            # Add the l+1-level Data object to data_list and update the
            # l-level after super_index has been changed
            data_list[level] = d1
            data_list.append(d2)

            if self.verbose:
                print('\n' + '-' * 64 + '\n')

        # Create the NAG object
        nag = NAG(data_list)
        return nag
