import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))


# ------------------------------------------------------------------------------------ #
# `pyrootutils.setup_root(...)` is an optional line at the top of each entry file
# that helps to make the environment more robust and convenient
#
# the main advantages are:
# - allows you to keep all entry files in "src/" without installing project as a package
# - makes paths and scripts always work no matter where is your current work dir
# - automatically loads environment variables from ".env" file if exists
#
# how it works:
# - the line above recursively searches for either ".git" or "pyproject.toml" in present
#   and parent dirs, to determine the project root dir
# - adds root dir to the PYTHONPATH (if `pythonpath=True`), so this file can be run from
#   any place without installing project as a package
# - sets PROJECT_ROOT environment variable which is used in "configs/paths/default.yaml"
#   to make all paths always relative to the project root
# - loads environment variables from ".env" file in root dir (if `dotenv=True`)
#
# you can remove `pyrootutils.setup_root(...)` if you:
# 1. either install project as a package or move each entry file to the project root dir
# 2. simply remove PROJECT_ROOT variable from paths in "configs/paths/default.yaml"
# 3. always run entry files from the project root dir
#
# https://github.com/ashleve/pyrootutils
# ------------------------------------------------------------------------------------ #

# Hack importing pandas here to bypass some conflicts with hydra
import pandas as pd

from typing import List, Optional, Tuple

import hydra
import torch
torch.multiprocessing.set_sharing_strategy('file_system') # command to solve pin memory problème on my machine
import torch_geometric
import pytorch_lightning as pl
import time
import csv
import os.path as osp
import os
from omegaconf import OmegaConf, DictConfig, open_dict
from pytorch_lightning import Callback, LightningDataModule, LightningModule, Trainer
from pytorch_lightning.loggers import Logger
from src.data import NAG
from src import utils
import matplotlib.pyplot as plt
import numpy as np

# Registering the "eval" resolver allows for advanced config
# interpolation with arithmetic operations:
# https://omegaconf.readthedocs.io/en/2.3_branch/how_to_guides.html
OmegaConf.register_new_resolver("eval", eval)

log = utils.get_pylogger(__name__)


@utils.task_wrapper
def train(cfg: DictConfig) -> Tuple[dict, dict]:
    """Trains the model. Can additionally evaluate on a testset, using best weights obtained during
    training.

    This method is wrapped in optional @task_wrapper decorator which applies extra utilities
    before and after the call.

    Args:
        cfg (DictConfig): Configuration composed by Hydra.

    Returns:
        Tuple[dict, dict]: Dict with metrics and dict with all instantiated objects.
    """
    # record starting time to plot profiling correctly
    if cfg.get("profile_time"):
        start = time.time()

    # enforce cuda device id to the chosen one
    if hasattr(cfg,"device_id") and cfg.trainer.get("accelerator") == "gpu":
        assert cfg.device_id < torch.cuda.device_count()
        torch.cuda.set_device("cuda:"+str(cfg.device_id))
        os.sched_setaffinity(0,list(range(int(torch.multiprocessing.cpu_count()/torch.cuda.device_count())*cfg.device_id,int(torch.multiprocessing.cpu_count()/torch.cuda.device_count())*(cfg.device_id+1))))
    else:
        device = "cpu"

    # set seed for random number generators in pytorch, numpy and python.random
    if cfg.get("seed"):
        pl.seed_everything(cfg.seed, workers=True)
    
    if cfg.get("measure_time"): ###
        mes_instantiate = time.time()

    log.info(f"Instantiating datamodule <{cfg.datamodule._target_}>")
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)

    log.info(f"Instantiating model <{cfg.model._target_}>")
    model: LightningModule = hydra.utils.instantiate(cfg.model)

    log.info("Instantiating callbacks...")
    callbacks: List[Callback] = utils.instantiate_callbacks(cfg.get("callbacks"))

    log.info("Instantiating loggers...")
    logger: List[Logger] = utils.instantiate_loggers(cfg.get("logger"))

    log.info(f"Instantiating trainer <{cfg.trainer._target_}>")
    trainer: Trainer = hydra.utils.instantiate(cfg.trainer, callbacks=callbacks, logger=logger)
    if float('.'.join(torch.__version__.split('.')[:2])) >= 2.0:
        torch.set_float32_matmul_precision(cfg.float32_matmul_precision)
    
    if cfg.get("measure_time"): ###
        mes_instantiate = time.time()-mes_instantiate

    object_dict = {
        "cfg": cfg,
        "datamodule": datamodule,
        "model": model,
        "callbacks": callbacks,
        "logger": logger,
        "trainer": trainer,
    }

    if cfg.get("measure_time"): ###
        mes_train = time.time()

    if logger:
        log.info("Logging hyperparameters!")
        utils.log_hyperparameters(object_dict)

    if cfg.get("compile"):
        log.info("Compiling model!")
        model = torch_geometric.compile(model)
    
    if cfg.get("train"):
        log.info("Starting training!")
        trainer.fit(model=model, datamodule=datamodule, ckpt_path=cfg.get("ckpt_path"))

    # Print datamodule and model intou output file
    f = open(osp.join(cfg.paths.output_dir,"datamodule.txt"),"w")
    print(datamodule,file=f)
    print(datamodule.train_dataset,file=f)
    f.close()

    f = open(osp.join(cfg.paths.output_dir,"model.txt"),"w")
    print(model,file=f)
    compt = 0
    for p in model.parameters():
        compt += p.numel()
    print("\n\nNb Parameters : ",compt,file=f)
    f.close()
    
    if cfg.get("measure_time"): ###
        mes_train = time.time()-mes_train

    train_metrics = trainer.callback_metrics

    if cfg.get("measure_time"): ###
        mes_test = time.time()

    if cfg.get("test"): # not disponible for PascalVOC data
        log.info("Starting testing!")
        ckpt_path = trainer.checkpoint_callback.best_model_path
        if ckpt_path == "":
            log.warning("Best ckpt not found! Using current weights for testing...")
            ckpt_path = None
        trainer.test(model=model, datamodule=datamodule, ckpt_path=ckpt_path)
        log.info(f"Best ckpt path: {ckpt_path}")

    if cfg.get("measure_time"): # record all time measure in time.csv
        mes_test = time.time()-mes_test
        with open(osp.join(cfg.paths.output_dir,"time.csv"),'w',newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["Global Training Time"])
            writer.writerow(["name","time"])
            writer.writerow(["Instantiating",mes_instantiate])
            writer.writerow(["training",mes_train])
            writer.writerow(["testing",mes_test])
            writer.writerow([""])
            
            
            writer.writerow(["Data Processing Time"])
            writer.writerow(["Download time"])
            writer.writerow([datamodule.train_dataset.download_time.sum.item()])
            writer.writerow([""])
            
            writer.writerow(["Pre Processing time"])
            writer.writerow(["total_time","nb processed image","average time","standard deviation"])
            writer.writerow([datamodule.pre_process_time.sum.item(), \
                datamodule.pre_process_time.count.item(), \
                datamodule.pre_process_time.avg.item(), \
                datamodule.pre_process_time.std.item()])
            
            if datamodule.pre_transform is not None:
                writer.writerow(["Detail operations"]) 
                writer.writerow(["name","nb usage","average time","standard deviation","total time"])
                for t in datamodule.pre_transform.transforms:
                    if hasattr(t,"time"):
                        writer.writerow([t.__class__.__name__,t.time.count.item(),t.time.avg.item(),t.time.std.item(),t.time.sum.item()])
            writer.writerow([""])
           
            writer.writerow(["Processing time"])
            def print_processing_time(dataset):
                if dataset.in_memory:
                    writer.writerow(["in memory case :"])
                    writer.writerow(["total load time","total process time"])
                    writer.writerow([dataset.load_time_sum, dataset.process_time_sum])
                else:
                    writer.writerow(["name","nb usage","average time","standard deviation","total time"])
                    writer.writerow(["load",dataset.load_time_count.value, \
                        dataset.load_time_avg, \
                        dataset.load_time_std, \
                        dataset.load_time_sum])
                    writer.writerow(["process",dataset.process_time_count.value, \
                        dataset.process_time_avg, \
                        dataset.process_time_std, \
                        dataset.process_time_sum])
                    writer.writerow(["imageset",dataset.imageset_time_count.value, \
                        dataset.imageset_time_avg, \
                        dataset.imageset_time_std, \
                        dataset.imageset_time_sum])
                writer.writerow([""])

            def print_transform_time(transform):
                if transform is not None:
                    writer.writerow(["transform"])
                    writer.writerow(["Detail operations"])
                    writer.writerow(["name","nb usage","average time","standard deviation","total time"])
                    for t in transform.transforms:
                        if hasattr(t,"time"):
                            writer.writerow([t.__class__.__name__,t.time.count.item(),t.time.avg.item(),t.time.std.item(),t.time.sum.item()])
                writer.writerow([""])

            writer.writerow(["training set"])
            print_processing_time(datamodule.train_dataset)
            print_transform_time(datamodule.train_transform)
            
            writer.writerow(["validation set"])
            print_processing_time(datamodule.val_dataset)
            print_transform_time(datamodule.val_transform)

            writer.writerow(["test set"])
            print_processing_time(datamodule.test_dataset)
            print_transform_time(datamodule.test_transform)
            
            writer.writerow([""])
            writer.writerow(["On device Processing time"])
            writer.writerow(["total_time","nb processed image","average time","standard deviation"])
            writer.writerow([datamodule.on_device_process_time.sum.item(), \
                datamodule.on_device_process_time.count.item(), \
                datamodule.on_device_process_time.avg.item(), \
                datamodule.on_device_process_time.std.item()])
            
            writer.writerow(["training set"])
            print_transform_time(datamodule.on_device_train_transform)
            
            writer.writerow(["validation set"])
            print_transform_time(datamodule.on_device_val_transform)
                
            writer.writerow(["test set"])
            print_transform_time(datamodule.on_device_test_transform)

            writer.writerow([""])
            writer.writerow(["Model computing Time"])
            writer.writerow(["name","nb usage","average time","standard deviation","total time"])
            writer.writerow(["forward",model.time["forward"].count.item(), \
                model.time["forward"].avg.item(), \
                model.time["forward"].std.item(), \
                model.time["forward"].sum.item()])
            writer.writerow(["loss",model.time["loss"].count.item(), \
                model.time["loss"].avg.item(), \
                model.time["loss"].std.item(), \
                model.time["loss"].sum.item()])
            writer.writerow(["Network Computing time"])
            writer.writerow(["name","nb usage","average time","standard deviation","total time"])
            writer.writerow(["mlp_time",model.net.mlp_time.count.item(), \
                model.net.mlp_time.avg.item(), \
                model.net.mlp_time.std.item(), \
                model.net.mlp_time.sum.item()])
            writer.writerow(["first_stage_time",model.net.first_stage_time.count.item(), \
                model.net.first_stage_time.avg.item(), \
                model.net.first_stage_time.std.item(), \
                model.net.first_stage_time.sum.item()])
            writer.writerow(["stage_time",model.net.stage_time.count.item(), \
                model.net.stage_time.avg.item(), \
                model.net.stage_time.std.item(), \
                model.net.stage_time.sum.item()])
            writer.writerow(["shape encoding time",model.net.shape_encoding_time.count.item(), \
                model.net.shape_encoding_time.avg.item(), \
                model.net.shape_encoding_time.std.item(), \
                model.net.shape_encoding_time.sum.item()])
            writer.writerow([""])

    test_metrics = trainer.callback_metrics
    
    # merge train and test metrics
    metric_dict = {**train_metrics, **test_metrics}
    return metric_dict, object_dict


@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def main(cfg: DictConfig) -> Optional[float]:

    # train the model
    metric_dict, _ = train(cfg)

    # safely retrieve metric value for hydra-based hyperparameter optimization
    metric_value = utils.get_metric_value(
        metric_dict=metric_dict, metric_name=cfg.get("optimized_metric")
    )

    # return optimized metric
    return metric_value


if __name__ == "__main__":
    main()
