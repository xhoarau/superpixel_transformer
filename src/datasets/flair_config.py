import numpy as np
import os.path as osp
import os


########################################################################
#                         Download information                         #
########################################################################

FORM_URL = "https://storage.gra.cloud.ovh.net/v1/AUTH_366279ce616242ebb14161b7991a8461/defi-ia/flair_data_1/"
TRAIN_ZIP_NAME = "flair_aerial_train.zip"
TRAIN_LABEL_ZIP_NAME = "flair_labels_train.zip"
TEST_ZIP_NAME = "flair_1_aerial_test.zip"
TEST_LABEL_ZIP_NAME = "flair_1_labels_test.zip"
META_DATA_ZIP_NAME = "flair-1_metadata_aerial.zip"
#UNZIP_NAME = "VOCdevkit"

########################################################################
#                                Labels                                #
########################################################################

# Credit: https://github.com/torch-points3d/torch-points3d

FLAIR_NUM_CLASSES = 13

INV_OBJECT_LABEL = {
    0: "building",
    1: "previous surface",
    2: "imprevious surface",
    3: "bare soil",
    4: "water",
    5: "coniferous",
    6: "deciduous",
    7: "brushwood",
    8: "vineyard",
    9: "herbaceous vegetation",
    10: "agricultural land",
    11: "plowed land",
    12: "other"}

CLASS_NAMES = [INV_OBJECT_LABEL[i] for i in range(FLAIR_NUM_CLASSES)] + ['ignored']

CLASS_COLORS = np.asarray([
    [219,  14, 154], #building	            -> purple
    [147, 142, 123], #previous surface		-> gray
    [248,  12,   0], #imprevious surface	-> red
    [169, 113,   1], #bare soil 			-> brown
    [21,   83, 174], #water 			    -> darkblue
    [25,   74,  38], #coniferous 		    -> darkgreen
    [70,  228, 131], #deciduous		    	-> turquoise
    [243, 166,  13], #brushwood			    -> orange
    [102,   0, 130], #vineyard			    -> darkpurple
    [85,  255,   0], #herbaceous vegetation	-> green
    [255, 243,  13], #agricultural land		-> yellow
    [192, 128,   0], #plowed land			-> orange
    [  0,   0,   0]]) #other	            -> black

OBJECT_LABEL = {name: i for i, name in INV_OBJECT_LABEL.items()}

def object_name_to_label(object_class):
    """Convert from object name to int label. By default, if an unknown
    object name
    """
    object_label = OBJECT_LABEL.get(object_class, OBJECT_LABEL["other"])
    return object_label


########################################################################
#                                Domains                               #
########################################################################

TRAIN_DOMAINS = ['D055_2018', 'D080_2021', 'D086_2020', 'D029_2021', 'D035_2020', 'D067_2021', 'D038_2021', 'D072_2019', 'D070_2020', 'D033_2021', 'D007_2020', 'D034_2021', 'D008_2019', 'D046_2019', 'D017_2018', 'D023_2020', 'D021_2020', 'D009_2019', 'D051_2019', 'D032_2019', 'D044_2020', 'D063_2019', 'D030_2021', 'D066_2021', 'D081_2020', 'D016_2020', 'D078_2021', 'D031_2019', 'D052_2019', 'D074_2020']
VAL_DOMAINS = ['D041_2021', 'D013_2020', 'D004_2021', 'D060_2021', 'D006_2020', 'D091_2021', 'D058_2020', 'D014_2020', 'D077_2021', 'D049_2020']
TEST_DOMAINS = ['D071_2020', 'D083_2020', 'D064_2021', 'D076_2019', 'D026_2020', 'D068_2021', 'D022_2021', 'D075_2021', 'D085_2019', 'D012_2019']


