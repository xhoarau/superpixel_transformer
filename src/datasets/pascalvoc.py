import os
import sys
import glob
import torch
import shutil
import logging
#import pandas as pd
import requests
#import tarfile
from src.datasets import BaseDataset
from src.data import Data, Batch
from src.datasets.pascalvoc_config import *
from torch_geometric.data import extract_tar
#from src.utils import available_cpu_count, starmap_with_kwargs, to_float_rgb
from tqdm import tqdm

from PIL import Image,ImageOps
from torchvision import transforms
import numpy as np
import scipy.io as io

from progressbar import ProgressBar

DIR = osp.dirname(osp.realpath(__file__))
log = logging.getLogger(__name__)


__all__ = ['PascalVOC','MiniPascalVOC']

########################################################################
#                                 Utils                                #
########################################################################

def path_label_org(image_name,raw_dir):
    """Given an image name of the original PascalVOC, return the path of the corresponing raw label
        WARNING if the name is from the extanded PascalVOC the path will not exist"""
    return osp.join(raw_dir,"VOC2012","SegmentationClass",image_name+".png")

def path_label_sup(image_name,raw_dir):
    """Given an image name of the extanded PascalVOC, return the path of the corresponing label
        WARNING if the name is from the original PascalVOC the path will not exist"""        
    return osp.join(raw_dir,"VOC2012","dataset","cls",image_name+".mat")

def image_to_label_path(raw_image_path): 
    """Return the Normalised label path corresponding to the input image raw path."""
    # Extract image name <path>
    image_name = osp.splitext(osp.basename(raw_image_path))[0]
    
    # construct the normalised label path 
    normalised_label_path = osp.join(self.raw_dir,"VOC2012","Normalised_label",image_name+".png")
    return normalised_label_path


def setup_annotations(raw_dir):
    """ Normalise label format """
    target_path = osp.join(raw_dir,"VOC2012", "Normalised_label")
    if not os.path.exists(target_path):
        os.makedirs(target_path)
    sets_pascalvoc = glob.glob(osp.join(raw_dir,"VOC2012","ImageSets","Segmentation")+"/*.txt")
    sets_sup_pascalvoc = glob.glob(osp.join(raw_dir,"VOC2012","dataset")+"/*.txt")
    
    for file_path in sets_pascalvoc:
        with open(file_path) as f:
            labels = list(map(lambda s:s[:-1],f.readlines()))

            for label_name in labels:
                if not osp.exists(osp.join(target_path,label_name+".png")):
                    label_path = path_label_org(label_name,raw_dir)
                    label = np.array(Image.open(label_path)).astype(np.int32)
                    label[label==255] = 0
                    Image.fromarray(label).save(osp.join(target_path,label_name+".png"))
    
    for file_path in sets_sup_pascalvoc:
        with open(file_path) as f:
            labels = list(map(lambda s:s[:-1],f.readlines()))

            for label_name in labels:
                if not osp.exists(osp.join(target_path,label_name+".png")):
                    label_path = path_label_sup(label_name,raw_dir)
                    data = io.loadmat(label_path)
                    label = data["GTcls"][0]["Segmentation"][0].astype(np.int32)
                    Image.fromarray(label).save(osp.join(target_path,label_name+".png"))

def read_pascalvoc_image(imagepath, classes, labelpath = None, xy = True, semantic=True, intensity=False, background=-1):
    """ create a data from an image

    :param imagepath: path to find raw image
    :param classes: classes to keep in groundtruth
    :param labelpath: path to find raw label
    :param xy: save position of pixel in data
    :param semantic: save the labels in data
    :param intensity: create and save grayscale version of the image
    :param background: int
        index of background
        if -1 set non used classes to ignored index
        else set non used classes to background
    """
    
    num_classes = len(classes)

    # create a tensor that contain the new index for all classes (len(classes) for all non used classes)
    if background == -1:
        trad = torch.full((PascalVOC_NUM_CLASSES,1),len(classes)).squeeze()
    else:
        trad = torch.full((PascalVOC_NUM_CLASSES,1),background).squeeze()
    trad[classes]=torch.arange(len(classes))
    
    if labelpath is None:
        labelpath = image_to_label_path(imagepath)
    
    data = Data()
    image_pil = Image.open(imagepath)
    image = np.array(image_pil)
    tf = transforms.Compose(
        [
            transforms.ToTensor(),
        ]
    )
    image = tf(image)
    w,h = image.shape[-2:]
    if image.shape[0] == 1:
        image = image.repeat(3,1,1)
    data.image = image # saved for visualisation or to ease manipulation
    data.rgb_dim = torch.tensor([0,1,2])
    if xy:
        data.pos = torch.from_numpy(np.array(np.meshgrid(np.arange(h),np.arange(w))).reshape([2,w*h]).transpose()).float()
        data.pos = data.pos/torch.max(data.pos)
    
    if intensity:
        gray_image = np.array(ImageOps.grayscale(image_pil))
        tf = transforms.Compose(
            [
                transforms.ToTensor(),
            ]
        )
        gray_image = tf(gray_image)
        data.gray_image = gray_image
        data.intensity = gray_image.flatten().squeeze()

    if semantic:
        label = Image.open(labelpath)
        label = torch.from_numpy(np.array(label).astype(np.int64))
        label = trad[label]
        data.label = label
        data.y = torch.zeros([label.numel(),num_classes+1]).long()
        data.y[torch.arange(label.numel()),label.flatten().squeeze()] = 1

    return data


########################################################################
#                               PascalVOC                              #
########################################################################

class PascalVOC(BaseDataset):
    """Pascal dataset, for Images prediction.

    Dataset website: http://host.robots.ox.ac.uk/pascal/VOC/voc2012/
    supplement data: http://www.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/

    Parameters (see base.py for complete description)
    ----------
    root : `str`
        Root directory where the dataset should be saved.
    stage : {'train', 'val', 'trainval', 'train_extand', 'val_extand'}, optional
    transform : `callable`, optional
        transform function operating on data.
    pre_transform : `callable`, optional
        pre_transform function operating on data.
    pre_filter : `callable`, optional
        pre_filter function operating on data.
    on_device_transform: `callable`, optional
        on_device_transform function operating on data, in the
        'on_after_batch_transfer' hook. This is where GPU-based
        augmentations should be, as well as any Transform you do not
        want to run in CPU-based DataLoaders
    """
   
    # download informations #
    _form_url = FORM_URL
    _sup_from_url = SUP_FROM_URL
    _zip_name = ZIP_NAME
    _unzip_name = UNZIP_NAME
    _sup_zip_name = SUP_ZIP_NAME
    _sup_unzip_name = SUP_UNZIP_NAME
    
    @property
    def class_names(self):
        """List of string names for dataset classes. This list may be
        one-item larger than `self.num_classes` if the last label
        corresponds to 'unlabelled' or 'ignored' indices, indicated as
        `-1` in the dataset labels.
        """
        return CLASS_NAMES
    
    @property
    def class_colors(self):
        """array of size [nclass+1,3], with a color for each label and for unlabeled
        """
        return CLASS_COLORS

    @property
    def num_classes(self):
        """Number of classes in the dataset. May be one-item smaller
        than `self.class_names`, to account for the last class name
        being optionally used for 'unlabelled' or 'ignored' classes,
        indicated as `-1` in the dataset labels.
        """
        return PascalVOC_NUM_CLASSES

    @property
    def all_base_image_name(self):
        """Dictionary holding lists of image name, for each
        stage.

        The following structure is expected:
            `{'train': [...], 'val': [...], 'test': [...]}`
        """
        path_image_set = osp.join(self.raw_dir,self._unzip_name,"VOC2012","ImageSets","Segmentation")
        path_image_set_sup = osp.join(self.raw_dir,self._unzip_name,"VOC2012","dataset")

        with open(osp.join(path_image_set,"train.txt")) as f:
            train = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,"train.txt")) as f:
            train = train.union(set(map(lambda s:s[:-1],f.readlines())))
        with open(osp.join(path_image_set,"val.txt")) as f:
            val = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,"val.txt")) as f:
            val = val.union(set(map(lambda s:s[:-1],f.readlines())))
        val = val-train
        assert len(val) > 0, "the val stage should have data"
        return {
            'train': list(train),
            'val': list(val),
            'test': []}

    def download_dataset(self): # Done #
        """Download the PascalVOC dataset.
        """
        if not osp.exists(self.root):
            os.makedirs(self.root)
        if not osp.exists(osp.join(self.root,self._zip_name)):
            PascalVOC = requests.get(self._form_url, allow_redirects=True)
            open(osp.join(self.root,self._zip_name),'wb').write(PascalVOC.content)
            PascalVOC.close()
        
        if not osp.exists(osp.join(self.root,self._sup_zip_name)):
            Sup_PascalVOC = requests.get(self._sup_from_url, allow_redirects=True)
            open(osp.join(self.root,self._sup_zip_name),'wb').write(Sup_PascalVOC.content)
            Sup_PascalVOC.close()
        
        # Unzip the file and rename it into the `root/raw/` directory. This
        # directory contains the raw Area folders from the zip
        extract_tar(osp.join(self.root, self._zip_name), self.root,'r')
        shutil.rmtree(self.raw_dir)
        shutil.move(osp.join(self.root, self._unzip_name), osp.join(self.raw_dir,self._unzip_name))
        extract_tar(osp.join(self.root, self._sup_zip_name), self.root,'r')
        shutil.move(osp.join(self.root, self._sup_unzip_name,"dataset"),osp.join(self.raw_dir,self._unzip_name,"VOC2012"))
        shutil.rmtree(osp.join(self.root, self._sup_unzip_name))

        setup_annotations(osp.join(self.raw_dir,self._unzip_name))

    def read_single_raw_image(self, raw_image_path, raw_label_path=None): 
        """Read a single raw image and return a Data object, ready to
        be passed to `self.pre_transform`.
        """
        if raw_label_path is None:
            raw_label_path = self.path_label(osp.splitext(osp.basename(raw_image_path))[0]) # find the image name in the image path
        if not hasattr(self,"classes"):
            classes = [i for i in range(PascalVOC_NUM_CLASSES)]
        else:
            classes = self.classes
        if hasattr(self,"background"):
            background = self.background
        else:
            background = -1
        return read_pascalvoc_image(raw_image_path, classes, raw_label_path, semantic=True, background=background)

    @property
    def raw_file_structure(self): # Done #
        return f"""
    {self.root}/
        └── {self._zip_name}
        └── raw/
            └── VOCdevkit/
                └── VOC2012/
                    └── JPEGImages/
                        └── 2007_000027.jpg
                        └── ...
                    └── ImageSets/
                        └── Segmentation/
                            └── train.txt
                            └── trainval.txt
                            └── val.txt
                    └── dataset/
                        └── cls/
                            └── 2008_000002.mat # num class encoding
                            └── ...
                        └── train.txt
                        └── val.txt
                    └── SegmentationClass/
                        └── 2007_000032.png # color encoding
                        └── ...
                    └── Normalised_label/
                        └── 2007_000032.png # num class encoding
                        └── ...
                        └── 2008_000002.png # num class encoding
                        └── ...
            """
    
    def path_image(self,image_name):
        """Given an image name, return the path of the corresponing raw image"""
        return osp.join(self.raw_dir,self._unzip_name,"VOC2012","JPEGImages",image_name+".jpg")

    def path_label(self,image_name):
        """Given an image name, return the path of the corresponing normalised label"""
        return osp.join(self.raw_dir,self._unzip_name,"VOC2012","Normalised_label",image_name+".png")

    @property
    def raw_file_names(self):
        """The file paths to find in order to skip the download."""
        sets_pascalvoc = glob.glob(osp.join(self.raw_dir,self._unzip_name,"VOC2012","ImageSets","Segmentation")+"/*.txt")
        sets_sup_pascalvoc = glob.glob(osp.join(self.raw_dir,self._unzip_name,"VOC2012","dataset")+"/*.txt")
        images = []
        labels = []
        
        for file_path in sets_pascalvoc + sets_sup_pascalvoc:
            with open(file_path) as f:
                images = images + list(map(self.path_image,map(lambda s:s[:-1],f.readlines())))
                labels = labels + list(map(self.path_label,map(lambda s:s[:-1],f.readlines())))

        return sets_pascalvoc + sets_sup_pascalvoc + images + labels

    def processed_to_raw_path(self, processed_path):
        """Return the raw cloud path corresponding to the input processed path."""
        # Extract image name <path>
        stage, hash_dir, image_name = osp.splitext(processed_path)[0].split('/')[-3:]

        # Read the raw cloud data
        raw_path = self.path_image(image_name)
        return raw_path


########################################################################
#                           MiniPascalVOC                              #
########################################################################

class MiniPascalVOC(PascalVOC):
    """A mini version of PascalVOC with a fraction of the images and the classes for experimentation.
    """

    def __init__(self, *args, classes=None, frc_img=0.25, background=-1, **kwargs):
        if classes is None:
            self.classes = list(range(PascalVOC_NUM_CLASSES))
        else:
            assert all(c>=0 for c in classes) and all(c<PascalVOC_NUM_CLASSES for c in classes), f"classes index must be between 0 and {PascalVOC_NUM_CLASSES}"
            self.classes = list(set(classes))
        self.frc_img = frc_img
        assert self.frc_img < 1 and self.frc_img > 0
        self.background = background
        super().__init__(*args,**kwargs)

    def create_mini_set(self, path, source, dest, ignored_index=[0]):
        """ create a sub set from the file source and write the image names in dest file
        it also assure that all selected images get at least one classes of the list self.classes

        :param path: path where to find source and create dest
        :param source: name of the file from witch the subset is created
            size of sub set = len(source file)*self.frc_img
        :param dest: name of the file in witch write the image names of the sub set
        :param ignored_index: list of classes that doesn't count in the "at least one classes" (exemple background)
        """
        needed_classes = torch.tensor([c for c in self.classes if c not in ignored_index])

        with open(osp.join(path,source)) as f:
            set_ = set(map(lambda s:s[:-1],f.readlines()))
        objectif = len(set_) * self.frc_img
        compt = 0

        f = open(osp.join(path,dest),"w")
        bar = ProgressBar(objectif).start()
        for image_name in tqdm(set_, ascii=True):
            labelpath = self.path_label(image_name)
            label = Image.open(labelpath)
            label = torch.from_numpy(np.array(label).astype(np.int64))
            if torch.any(torch.isin(label,needed_classes)):
                f.write(image_name+"\n")
                compt += 1
                bar.update(compt)
            if compt >= objectif:
                break
        bar.finish()
        assert compt>=objectif, "missing images to create correct dataset"
        f.close()

    @property
    def all_base_image_name(self):
        path_image_set = osp.join(self.raw_dir,self._unzip_name,"VOC2012","ImageSets","Segmentation")
        path_image_set_sup = osp.join(self.raw_dir,self._unzip_name,"VOC2012","dataset")

        train_file_name = "train_mini_"+str(self.classes)+"_"+str(self.frc_img)+".txt"
        val_file_name = "val_mini_"+str(self.classes)+"_"+str(self.frc_img)+".txt"
        if not osp.isfile(os.path.join(path_image_set,train_file_name)):
            log.info(f"create train sub set in {path_image_set}")
            self.create_mini_set(path_image_set,"train.txt",train_file_name)
            log.info(f"train sub set created")
        if not osp.isfile(os.path.join(path_image_set_sup,train_file_name)):
            log.info(f"create train sub set in {path_image_set_sup}")
            self.create_mini_set(path_image_set_sup,"train.txt",train_file_name)
            log.info(f"train sub set created")
        if not osp.isfile(os.path.join(path_image_set,val_file_name)):
            log.info(f"create val sub set in {path_image_set}")
            self.create_mini_set(path_image_set,"val.txt",val_file_name)
            log.info(f"val sub set created")
        if not osp.isfile(os.path.join(path_image_set_sup,val_file_name)):
            log.info(f"create valsup sub set in {path_image_set_sup}")
            self.create_mini_set(path_image_set_sup,"val.txt",val_file_name)
            log.info(f"val sub set created")
        
        with open(osp.join(path_image_set,train_file_name)) as f:
            train = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,train_file_name)) as f:
            train = train.union(set(map(lambda s:s[:-1],f.readlines())))
        with open(osp.join(path_image_set,val_file_name)) as f:
            val = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,val_file_name)) as f:
            val = val.union(set(map(lambda s:s[:-1],f.readlines())))
        
        val = val-train
        assert len(val) > 0, "the val stage should have data"
        return {
            'train': list(train),
            'val': list(val),
            'test': []}


    @property
    def data_subdir_name(self):
        return self.__class__.__bases__[0].__name__.lower()

    # We have to include this method, otherwise the parent class skips
    # processing
    def process(self):
        super().process()

    # We have to include this method, otherwise the parent class skips
    # processing
    def download(self):
        super().download()

    @property
    def class_names(self):
        """List of string names for dataset classes. This list may be
        one-item larger than `self.num_classes` if the last label
        corresponds to 'unlabelled' or 'ignored' indices, indicated as
        `-1` in the dataset labels.
        """
        return [CLASS_NAMES[c] for c in self.classes+[-1]]
    
    @property
    def class_colors(self):
        """array of size [nclass+1,3], with a color for each label and for unlabeled
        """
        return CLASS_COLORS[self.classes+[-1]]

    @property
    def num_classes(self):
        """Number of classes in the dataset. May be one-item smaller
        than `self.class_names`, to account for the last class name
        being optionally used for 'unlabelled' or 'ignored' classes,
        indicated as `-1` in the dataset labels.
        """
        return len(self.classes)

