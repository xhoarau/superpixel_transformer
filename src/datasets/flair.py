import os
import sys
import glob
import torch
import shutil
import logging
#import pandas as pd
import requests
#import tarfile
from src.datasets import BaseDataset
from src.data import Data, Batch
from src.datasets.flair_config import *
import zipfile
#from src.utils import available_cpu_count, starmap_with_kwargs, to_float_rgb
from tqdm import tqdm

from PIL import Image,ImageOps
from torchvision import transforms
import numpy as np
import scipy.io as io

from progressbar import ProgressBar

import json
from libtiff import TIFF,libtiff_ctypes

DIR = osp.dirname(osp.realpath(__file__))
log = logging.getLogger(__name__)

__all__ = ['FLAIR','MiniFLAIR']

### Warning not Functional ###

########################################################################
#                                 Utils                                #
########################################################################

def load_tiff(paths):
    return TIFF.open(paths[0]).read_image(),TIFF.open(paths[1]).read_image()

def replace_image_empty(val):
    if not isinstance(val,np.ndarray):
        return np.zeros([512,512,5],dtype=np.uint8)
    return val
    
def replace_label_empty(val):
    if not isinstance(val,np.ndarray):
        return np.full([512,512],12,dtype=np.uint8)
    return val

def fuse(round_coords,list_paths):
    """ fuse the images of the ame zone

    :param round_coords: index coordinates of the images (see setup_images)
    :param list_paths: all images and label paths (see setup_images)
    """
    out_image = [[0 for _ in range(round_coords.max(0)[1]+1)] for _ in range(round_coords.max(0)[0]+1)]
    out_label = [[0 for _ in range(round_coords.max(0)[1]+1)] for _ in range(round_coords.max(0)[0]+1)]
    nb_lines = np.max(round_coords[:,1])
    libtiff_ctypes.suppress_warnings()
    list_images = list(map(load_tiff,list_paths))
    for i in range(len(list_paths)):
        out_image[round_coords[i,0]][nb_lines-round_coords[i,1]] = list_images[i][0]
        out_label[round_coords[i,0]][nb_lines-round_coords[i,1]] = list_images[i][1]
    out_image = np.concatenate([np.concatenate(list(map(replace_image_empty,out_image[i]))) for i in range(len(out_image))],axis=1)
    out_label = np.concatenate([np.concatenate(list(map(replace_label_empty,out_label[i]))) for i in range(len(out_label))],axis=1)-1 #set classes to start from 0
    out_label[out_label>12] = 12
    return out_image,out_label

def setup_images(raw_path,name,train_domains,val_domains,test_domains):
    """ read the json meta_data file reorganise the images to the proper format
    
    one folder for each stage (train,val,test)
    image fusion per zone (only fusion method implemented)

    also create txt files for future check for missing files and easier load

    :param raw_path: path where to find raw folders and json file
    :param name: name of fusion method (only one implemented yet)
    :param ***_domains: lists of domains name for each stage
    """
    train_file = open(osp.join(raw_path,"train.txt"),'w')
    val_file = open(osp.join(raw_path,"val.txt"),'w')
    test_file = open(osp.join(raw_path,"test.txt"),'w')
    compt_zones = 0
    with open(osp.join(raw_path,"flair-1_metadata_aerial.json"),'r') as f:
        data = json.load(f)
        dict_coords={}
        for id_ in data:
            if data[id_]["domain"] not in test_domains:
                image_path = "/app/data/dataset/flair/raw/"+"flair_aerial_train/"+data[id_]["domain"]+"/"+data[id_]["zone"]+"/img/"+id_+".tif"
                label_path = "/app/data/dataset/flair/raw/"+"flair_labels_train/"+data[id_]["domain"]+"/"+data[id_]["zone"]+"/msk/"+"MSK"+id_[3:]+".tif"
            else:
                image_path = "/app/data/dataset/flair/raw/"+"flair_1_aerial_test/"+data[id_]["domain"]+"/"+data[id_]["zone"]+"/img/"+id_+".tif"
                label_path = "/app/data/dataset/flair/raw/"+"flair_1_labels_test/"+data[id_]["domain"]+"/"+data[id_]["zone"]+"/msk/"+"MSK"+id_[3:]+".tif"
            if data[id_]["domain"] in dict_coords.keys():
                if data[id_]["zone"] in dict_coords[data[id_]["domain"]]["zones"]:
                    dict_coords[data[id_]["domain"]][data[id_]["zone"]+"_coords"].append([data[id_]["patch_centroid_x"],data[id_]["patch_centroid_y"]])
                    dict_coords[data[id_]["domain"]][data[id_]["zone"]+"_paths"].append([image_path,label_path])
                else:
                    compt_zones += 1
                    dict_coords[data[id_]["domain"]]["zones"].append(data[id_]["zone"])
                    dict_coords[data[id_]["domain"]][data[id_]["zone"]+"_coords"] = [[data[id_]["patch_centroid_x"],data[id_]["patch_centroid_y"]]]
                    dict_coords[data[id_]["domain"]][data[id_]["zone"]+"_paths"] = [[image_path,label_path]]
            else:
                compt_zones += 1
                dict_coords[data[id_]["domain"]] = {"zones": [data[id_]["zone"]], \
                data[id_]["zone"]+"_coords":[[data[id_]["patch_centroid_x"],data[id_]["patch_centroid_y"]]], \
                data[id_]["zone"]+"_paths":[[image_path,label_path]]}

    list_domains = list(dict_coords.keys())
    bar = ProgressBar(compt_zones).start()
    compt_fuse = 0
    if not osp.isdir(osp.join(raw_path,name,"test","msk")):
        os.mkdir(osp.join(raw_path,name))
        os.mkdir(osp.join(raw_path,name,"train"))
        os.mkdir(osp.join(raw_path,name,"train","img"))
        os.mkdir(osp.join(raw_path,name,"train","msk"))
        os.mkdir(osp.join(raw_path,name,"val"))
        os.mkdir(osp.join(raw_path,name,"val","img"))
        os.mkdir(osp.join(raw_path,name,"val","msk"))
        os.mkdir(osp.join(raw_path,name,"test"))
        os.mkdir(osp.join(raw_path,name,"test","img"))
        os.mkdir(osp.join(raw_path,name,"test","msk"))
    for domain in list_domains:
        for zone in dict_coords[domain]["zones"]:
            coords = np.array(dict_coords[domain][zone+"_coords"])
            round_coords = np.round(((coords - coords.min(axis=0))/102.4)).astype(np.int32) # images are 102.4 meters long
            out_image,out_label = fuse(round_coords,dict_coords[domain][zone+"_paths"]) # fuse all images of the zone in one
            if domain in train_domains:
                save_image_path = osp.join(raw_path,name,"train","img",domain+"_"+zone+".pt")
                save_label_path = osp.join(raw_path,name,"train","msk",domain+"_"+zone+".pt")
                train_file.write(domain+"_"+zone+"\n")
            elif domain in val_domains:
                save_image_path = osp.join(raw_path,name,"val","img",domain+"_"+zone+".pt")
                save_label_path = osp.join(raw_path,name,"val","msk",domain+"_"+zone+".pt")
                val_file.write(domain+"_"+zone+"\n")
            else:
                save_image_path = osp.join(raw_path,name,"test","img",domain+"_"+zone+".pt")
                save_label_path = osp.join(raw_path,name,"test","msk",domain+"_"+zone+".pt")
                test_file.write(domain+"_"+zone+"\n")
            torch.save(torch.from_numpy(out_image).permute(2,0,1),save_image_path)
            torch.save(torch.from_numpy(out_label),save_label_path)
            compt_fuse += 1
            bar.update(compt_fuse)

    train_file.close()
    val_file.close()
    test_file.close()


def read_flair_image(imagepath, classes, labelpath, xy = True, infrared=True, elevation=True, semantic=True, background=13):
    """ create a data from an image

    :param imagepath: path to find raw image
    :param classes: classes to keep in groundtruth
    :param labelpath: path to find raw label
    :param xy: save position of pixel in data
    :param semantic: save the labels in data
    :param intensity: create and save grayscale version of the image
    :param background: int
        index of background
        if -1 set non used classes to ignored index
        else set non used classes to background
    """
    
    num_classes = len(classes)

    # create a tensor that contain the new index for all classes (len(classes) for all non used classes)
    if background == -1:
        trad = torch.full((FLAIR_NUM_CLASSES,1),12).squeeze()
    else:
        trad = torch.full((FLAIR_NUM_CLASSES,1),background).squeeze()
    trad[classes]=torch.arange(len(classes)).squeeze()
    
    data = Data()
    image = torch.load(imagepath)
    w,h = image.shape[-2:]
    if not infrared:
        image = image[[0,1,2,4]]
    else:
        data.nir_dim = torch.tensor([3])
    if not elevation:
        image = image[:-1]
    else:
        data.elevation_dim = torch.tensor([4 - (not infrared)])
    data.image = image # saved for visualisation or to ease manipulation
    data.rgb_dim = torch.tensor([0,1,2])
    
    if xy:
        data.pos = torch.from_numpy(np.array(np.meshgrid(np.arange(h),np.arange(w))).reshape([2,w*h]).transpose()).float()
        data.pos = data.pos/torch.max(data.pos)
    if semantic:
        label = torch.load(labelpath).long()
        label = trad[label]
        data.label = label
        data.y = torch.zeros([label.numel(),num_classes+1]).long()
        data.y[torch.arange(label.numel()),label.flatten().squeeze()] = 1
    return data


########################################################################
#                                 FLAIR                                #
########################################################################

class FLAIR(BaseDataset):
    """FLAIR dataset, for Images prediction.

    Dataset website: https://ignf.github.io/FLAIR/

    Parameters (see base.py for complete description)
    ----------
    root : `str`
        Root directory where the dataset should be saved.
    stage : {'train', 'val', 'trainval'}, optional
    transform : `callable`, optional
        transform function operating on data.
    pre_transform : `callable`, optional
        pre_transform function operating on data.
    pre_filter : `callable`, optional
        pre_filter function operating on data.
    on_device_transform: `callable`, optional
        on_device_transform function operating on data, in the
        'on_after_batch_transfer' hook. This is where GPU-based
        augmentations should be, as well as any Transform you do not
        want to run in CPU-based DataLoaders
    """
   
    # download informations #
    _from_url = FORM_URL
    _train_zip_name = TRAIN_ZIP_NAME
    _train_label_zip_name = TRAIN_LABEL_ZIP_NAME
    _test_zip_name = TEST_ZIP_NAME
    _test_label_zip_name = TEST_LABEL_ZIP_NAME
    _meta_data_zip_name = META_DATA_ZIP_NAME

    _train_domains = TRAIN_DOMAINS
    _val_domains = VAL_DOMAINS
    _test_domains = TEST_DOMAINS
    
    def __init__(self,*args,fuse_name,infrared=True,elevation=True,**kwargs):
        self.fuse_name = fuse_name
        self.infrared = infrared # carry the information of the feature selection
        self.elevation = elevation
        super().__init__(*args,**kwargs)
        print("root : ",self.root)
        print("raw : ",self.raw_dir)
        STOP

        
        self.check_setup_files()
        self.check_setup_images()

    def check_setup_files(self,):
        """ check for all fused image """
        if not (osp.isfile(osp.join(self.raw_dir,"train.txt")) and \
                osp.isfile(osp.join(self.raw_dir,"val.txt")) and \
                osp.isfile(osp.join(self.raw_dir,"test.txt"))):
            setup_images(self.raw_dir,self.fuse_name,self._train_domains,self._val_domains,self._test_domains)
    
    def check_setup_images(self,):
        image_names = self.all_base_image_name
        all_train = all(list(map(osp.isfile,map(self.path_image,image_names["train"]))))
        all_val = all(list(map(osp.isfile,map(self.path_image,image_names["val"]))))
        all_test = all(list(map(osp.isfile,map(self.path_image,image_names["test"]))))
        if not (all_train and all_val and all_test):
            setup_images(self.raw_dir,self.fuse_name,self._train_domains,self._val_domains,self._test_domains)

    @property
    def class_names(self):
        """List of string names for dataset classes. This list may be
        one-item larger than `self.num_classes` if the last label
        corresponds to 'unlabelled' or 'ignored' indices, indicated as
        `-1` in the dataset labels.
        """
        return CLASS_NAMES
    
    @property
    def class_colors(self):
        """array of size [nclass+1,3], with a color for each label and for unlabeled
        """
        return CLASS_COLORS

    @property
    def num_classes(self):
        """Number of classes in the dataset. May be one-item smaller
        than `self.class_names`, to account for the last class name
        being optionally used for 'unlabelled' or 'ignored' classes,
        indicated as `-1` in the dataset labels.
        """
        return FLAIR_NUM_CLASSES

    @property
    def all_base_image_name(self):
        """Dictionary holding lists of image name, for each
        stage.

        The following structure is expected:
            `{'train': [...], 'val': [...], 'test': [...]}`
        """
        self.check_setup_files()
        with open(osp.join(self.raw_dir,"train.txt")) as f:
            train = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(self.raw_dir,"val.txt")) as f:
            val = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(self.raw_dir,"test.txt")) as f:
            test = set(map(lambda s:s[:-1],f.readlines()))
        val = val-train
        test = test - val
        test = test - train
        assert len(val) > 0, "the val stage should have data"
        assert len(test) > 0, "the test stage should have data"
        return {
            'train': list(train),
            'val': list(val),
            'test': list(test)}

    def download_dataset(self):
        """Download the FLAIR dataset.
        """
        if not osp.exists(self.root):
            os.makedirs(self.root)
        if not osp.exists(osp.join(self.root,self._train_zip_name)):
            Train_flair = requests.get(self._form_url+self._train_zip_name, allow_redirects=True)
            open(osp.join(self.root,self._train_zip_name),'wb').write(Train_flair.content)
            Train_flair.close()

        if not osp.exists(osp.join(self.root,self._train_label_zip_name)):
            Train_label_flair = requests.get(self._form_url+self._train_label_zip_name, allow_redirects=True)
            open(osp.join(self.root,self._train_label_zip_name),'wb').write(Train_label_flair.content)
            Train_label_flair.close()

        if not osp.exists(osp.join(self.root,self._test_zip_name)):
            Test_flair = requests.get(self._form_url+self._test_zip_name, allow_redirects=True)
            open(osp.join(self.root,self._test_zip_name),'wb').write(Test_flair.content)
            Test_flair.close()

        if not osp.exists(osp.join(self.root,self._test_label_zip_name)):
            Test_label_flair = requests.get(self._form_url+self._test_label_zip_name, allow_redirects=True)
            open(osp.join(self.root,self._test_label_zip_name),'wb').write(Test_label_flair.content)
            Test_label_flair.close()
        
        if not osp.exists(osp.join(self.root,self._meta_data_zip_name)):
            Meta_flair = requests.get(self._from_url+self._meta_data_zip_name, allow_redirects=True)
            open(osp.join(self.root,self._meta_data_zip_name),'wb').write(Meta_flair.content)
            Meta_flair.close()
        
        # Unzip the file and rename it into the `root/raw/` directory. This
        # directory contains the raw Area folders from the zip
        with zipfile.ZipFile(osp.join(self.root,self._train_zip_name),'r') as zip_ref:
            zip_ref.extractall(self.raw_dir)
        with zipfile.ZipFile(osp.join(self.root,self._train_label_zip_name),'r') as zip_ref:
            zip_ref.extractall(self.raw_dir)
        with zipfile.ZipFile(osp.join(self.root,self._test_zip_name),'r') as zip_ref:
            zip_ref.extractall(self.raw_dir)
        with zipfile.ZipFile(osp.join(self.root,self._test_label_zip_name),'r') as zip_ref:
            zip_ref.extractall(self.raw_dir)
        with zipfile.ZipFile(osp.join(self.root,self._meta_data_zip_name),'r') as zip_ref:
            zip_ref.extractall(self.raw_dir)
        
        setup_images(self.raw_dir,self.fuse_name,self._train_domains,self._val_domains,self._test_domains)

    def read_single_raw_image(self, raw_image_path, raw_label_path=None): 
        """Read a single raw image and return a Data object, ready to
        be passed to `self.pre_transform`.
        """
        if raw_label_path is None:
            raw_label_path = self.path_label(osp.splitext(osp.basename(raw_image_path))[0]) # find the image name in the image path
        if not hasattr(self,"classes"):
            classes = [i for i in range(FLAIR_NUM_CLASSES)]
        else:
            classes = self.classes
        if hasattr(self,"background"):
            background = self.background
        else:
            background = 13
        return read_flair_image(raw_image_path, classes, raw_label_path, 
            infrared=self.infrared, elevation=self.elevation, semantic=True, 
            background=background)

    @property
    def raw_file_structure(self):
        return f"""
        {self.root}/
        └── {self._zip_name}
        └── raw/
            └── flair-1_metadata_aerial.json
                flair_1_aerial_test
                └── D0**_YYYY
                    └── Z**_**
                        └── img
                            └── IMG_******.tif
                flair_1_labels_test
                    └── D0**_YYYY
                        └── Z**_**
                            └── msk
                                └── MSK_******.tif
                flair_aerial_train
                    └── ...
                flair_labels_train
                    └── ...
            """
    
    def path_image(self,image_name):
        """Given an image name, return the path of the corresponing raw image"""
        split = image_name.split("_")
        domain = split[0]+"_"+split[1]
        if domain in self._train_domains:
            domain_stage = "train"
        elif domain in self._val_domains:
            domain_stage = "val"
        elif domain in self._test_domains:
            domain_stage = "test"
        else:
            raise(f"{domain} domain doesn't exist in stages")

        return osp.join(self.raw_dir,self.fuse_name,domain_stage,"img",image_name+".pt")

    def path_label(self,image_name):
        """Given an image name, return the path of the corresponing normalised label"""
        split = image_name.split("_")
        domain = split[0]+"_"+split[1]
        if domain in self._train_domains:
            domain_stage = "train"
        elif domain in self._val_domains:
            domain_stage = "val"
        elif domain in self._test_domains:
            domain_stage = "test"
        else:
            raise(f"{domain} domain doesn't exist in stages")

        return osp.join(self.raw_dir,self.fuse_name,domain_stage,"msk",image_name+".pt")

    @property
    def raw_file_names(self):
        """The file paths to find in order to skip the download.
        
        only check for zip files if missing images unzip manualy or remove one zip file to restart
        all the download
        """
        return [self._train_zip_name,self._train_label_zip_name,
            self._test_zip_name,self._test_label_zip_name,
            self._meta_data_zip_name]

    def processed_to_raw_path(self, processed_path):
        """Return the raw image path corresponding to the input processed path."""
        # Extract image name <path>
        stage, hash_dir, image_name = osp.splitext(processed_path)[0].split('/')[-3:]

        # Read the raw cloud data
        raw_path = self.path_image(image_name)
        return raw_path


########################################################################
#                           MiniPascalVOC                              #
########################################################################

class MiniFLAIR(FLAIR): # TODO all
    """A mini version of PascalVOC with a fraction of the images and the classes for experimentation.
    """

    def __init__(self, *args, classes=None, frc_img=0.25, background=-1, **kwargs):
        if classes is None:
            self.classes = list(range(FLAIR_NUM_CLASSES))
        else:
            self.classes = list(set(classes))
            assert all(c>=0 for c in classes) and all(c<FLAIR_NUM_CLASSES for c in classes), f"classes index must be between 0 and {FLAIR_NUM_CLASSES}"
        self.frc_img = frc_img
        assert self.frc_img < 1 and self.frc_img > 0, "frc_img must be a fraction"
        self.background = background
        super().__init__(*args,**kwargs)

    def create_mini_set(self, path, source, dest, ignored_index=[0]):
        """ create a sub set from the file source and write the image names in dest file
        it also assure that all selected images get at least one classes of the list self.classes

        :param path: path where to find source and create dest
        :param source: name of the file from witch the subset is created
            size of sub set = len(source file)*self.frc_img
        :param dest: name of the file in witch write the image names of the sub set
        :param ignored_index: list of classes that doesn't count in the "at least one classes" (exemple background)
        """
        needed_classes = torch.tensor([c for c in self.classes if c not in ignored_index])

        with open(osp.join(path,source)) as f:
            set_ = set(map(lambda s:s[:-1],f.readlines()))
        objectif = len(set_) * self.frc_img
        compt = 0

        f = open(osp.join(path,dest),"w")
        bar = ProgressBar(objectif).start()
        for image_name in tqdm(set_, ascii=True):
            labelpath = self.path_label(image_name)
            label = Image.open(labelpath)
            label = torch.from_numpy(np.array(label).astype(np.int64))
            if torch.any(torch.isin(label,needed_classes)):
                f.write(image_name+"\n")
                compt += 1
                bar.update(compt)
            if compt >= objectif:
                break
        bar.finish()
        assert compt>=objectif, "missing images to create correct dataset"
        f.close()

    @property
    def all_base_image_name(self):
        path_image_set = osp.join(self.raw_dir,self._unzip_name,"VOC2012","ImageSets","Segmentation")
        path_image_set_sup = osp.join(self.raw_dir,self._unzip_name,"VOC2012","dataset")

        train_file_name = "train_mini_"+str(self.classes)+"_"+str(self.frc_img)+".txt"
        val_file_name = "val_mini_"+str(self.classes)+"_"+str(self.frc_img)+".txt"
        if not osp.isfile(os.path.join(path_image_set,train_file_name)):
            log.info(f"create train sub set in {path_image_set}")
            self.create_mini_set(path_image_set,"train.txt",train_file_name)
            log.info(f"train sub set created")
        if not osp.isfile(os.path.join(path_image_set_sup,train_file_name)):
            log.info(f"create train sub set in {path_image_set_sup}")
            self.create_mini_set(path_image_set_sup,"train.txt",train_file_name)
            log.info(f"train sub set created")
        if not osp.isfile(os.path.join(path_image_set,val_file_name)):
            log.info(f"create val sub set in {path_image_set}")
            self.create_mini_set(path_image_set,"val.txt",val_file_name)
            log.info(f"val sub set created")
        if not osp.isfile(os.path.join(path_image_set_sup,val_file_name)):
            log.info(f"create valsup sub set in {path_image_set_sup}")
            self.create_mini_set(path_image_set_sup,"val.txt",val_file_name)
            log.info(f"val sub set created")
        
        with open(osp.join(path_image_set,train_file_name)) as f:
            train = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,train_file_name)) as f:
            train = train.union(set(map(lambda s:s[:-1],f.readlines())))
        with open(osp.join(path_image_set,val_file_name)) as f:
            val = set(map(lambda s:s[:-1],f.readlines()))
        with open(osp.join(path_image_set_sup,val_file_name)) as f:
            val = val.union(set(map(lambda s:s[:-1],f.readlines())))
        
        val = val-train
        assert len(val) > 0, "the val stage should have data"
        return {
            'train': list(train),
            'val': list(val),
            'test': []}


    @property
    def data_subdir_name(self):
        return self.__class__.__bases__[0].__name__.lower()

    # We have to include this method, otherwise the parent class skips
    # processing
    def process(self):
        super().process()

    # We have to include this method, otherwise the parent class skips
    # processing
    def download(self):
        super().download()

    @property
    def class_names(self):
        """List of string names for dataset classes. This list may be
        one-item larger than `self.num_classes` if the last label
        corresponds to 'unlabelled' or 'ignored' indices, indicated as
        `-1` in the dataset labels.
        """
        return [CLASS_NAMES[c] for c in self.classes+[-1]]
    
    @property
    def class_colors(self):
        """array of size [nclass+1,3], with a color for each label and for unlabeled
        """
        return CLASS_COLORS[self.classes+[-1]]

    @property
    def num_classes(self):
        """Number of classes in the dataset. May be one-item smaller
        than `self.class_names`, to account for the last class name
        being optionally used for 'unlabelled' or 'ignored' classes,
        indicated as `-1` in the dataset labels.
        """
        return len(self.classes)

