import numpy as np
import os.path as osp
import os

########################################################################
#                         Download information                         #
########################################################################

# the data muwt be created or download before use
DIR_NAME = ["voronoi","flowers_and_fried_eggs","funny_shapes","funky_shapes","deformed_shapes","simple_letters",
            "complex_letters","big_complex_letters","one_letter","mix_letters","context_letters"]
ZIP_NAME = ["color_voronoi.zip","flowers_and_fried_eggs.zip","funny_shapes.zip","funky_shapes_grayscales.zip","deformed_shapes_grayscale.zip","simple_letters.zip",
            "complex_letters.zip","big_complex_letters.zip","one_letter.zip","mix_letters.zip","context_letters.zip"]
UNZIP_NAME = ["color_voronoi","flowers_and_fried_eggs","funny_shapes","funky_shapes_grayscales","deformed_shapes_grayscale","simple_letters",
            "complex_letters","big_complex_letters","one_letter","mix_letters","context_letters"]

########################################################################
#                                Labels                                #
########################################################################

Synthetic_NUM_CLASSES = [5,5,6,4,5,7,7,7,7,7,7]

INV_OBJECT_LABEL = [{
    0: "black",
    1: "red",
    2: "green",
    3: "blue",
    4: "yellow"},{
    0: "black",
    1: "egg_yellow",
    2: "flower_yellow",
    3: "white",
    4: "red"},{
    0: "black",
    1: "red",
    2: "green",
    3: "blue",
    4: "yellow_triangle",
    5: "yellow_octogone"},{
    0: "dark_gray",
    1: "light_gray",
    2: "triangle",
    3: "octogone"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "triangle",
    4: "octogone"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_a",
    4: "letter_v",
    5: "letter_O",
    6: "letter_I"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_b",
    4: "letter_d",
    5: "letter_I",
    6: "letter_J"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_b",
    4: "letter_d",
    5: "letter_I",
    6: "letter_J"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_a",
    4: "letter_v",
    5: "letter_O",
    6: "letter_I"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_b",
    4: "letter_d",
    5: "letter_a",
    6: "letter_v"},{
    0: "background_low",
    1: "background_medium",
    2: "background_high",
    3: "letter_b",
    4: "letter_d",
    5: "letter_a",
    6: "letter_v"}]

CLASS_NAMES = [[INV_OBJECT_LABEL[j][i] for i in range(Synthetic_NUM_CLASSES[j])] + ['ignored'] for j in range(len(INV_OBJECT_LABEL))]

CLASS_COLORS = [np.asarray([
    [  0,   0,   0], #black
    [255,   0,   0], #red
    [  0, 255,   0], #green
    [  0,   0, 255], #blue
    [255, 255,   0], #yellow
    [255, 255, 255]]), #unlabeled	-> white
    np.array([
    [  0,   0,   0], #black
    [255, 255,   0], #egg_yellow    -> yellow
    [255, 128,   0], #flower_yellow -> orange
    [255, 255, 255], #white
    [255,   0,   0], #red
    [  0,   0, 255]]), #unlabeled	-> blue (white already used)
    np.array([
    [  0,   0,   0], #black
    [255,   0,   0], #red
    [  0, 255,   0], #green
    [  0,   0, 255], #blue
    [255, 255,   0], #yellow_triangle   -> yellow
    [255, 128,   0], #yellow_octogone   -> orange
    [255, 255, 255]]), #unlabeled       -> white
    np.array([
    [  0,   0,   0], #dark_gray
    [128, 128, 128], #light_gray
    [255, 255,   0], #triangle      -> yellow
    [255, 128,   0], #octogone      -> orange
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255, 255,   0], #triangle      -> yellow
    [255, 128,   0], #octogone      -> orange
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_a      -> red
    [  0, 255,   0], #letter_i      -> green
    [  0,   0, 255], #letter_O      -> blue
    [255, 255,   0], #letter_V      -> yellow
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_b      -> red
    [  0, 255,   0], #letter_d      -> green
    [  0,   0, 255], #letter_I      -> blue
    [255, 255,   0], #letter_J      -> yellow
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_b      -> red
    [  0, 255,   0], #letter_d      -> green
    [  0,   0, 255], #letter_I      -> blue
    [255, 255,   0], #letter_J      -> yellow
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_a      -> red
    [  0, 255,   0], #letter_i      -> green
    [  0,   0, 255], #letter_O      -> blue
    [255, 255,   0], #letter_V      -> yellow
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_b      -> red
    [  0, 255,   0], #letter_d      -> green
    [  0,   0, 255], #letter_a      -> blue
    [255, 255,   0], #letter_v      -> yellow
    [255, 255, 255]]), #unlabeled   -> white
    np.array([
    [  0,   0,   0], #background_low
    [100, 100, 100], #background_medium
    [200, 200, 200], #background_high
    [255,   0,   0], #letter_b      -> red
    [  0, 255,   0], #letter_d      -> green
    [  0,   0, 255], #letter_a      -> blue
    [255, 255,   0], #letter_v      -> yellow
    [255, 255, 255]])] #unlabeled   -> white

OBJECT_LABEL = [{name: i for i, name in INV_OBJECT_LABEL[j].items()} for j in range(len(INV_OBJECT_LABEL))]

def object_name_to_label(object_class):
    """Convert from object name to int label. By default, if an unknown
    object nale
    """
    object_label = OBJECT_LABEL.get(object_class, OBJECT_LABEL["background"])
    return object_label
