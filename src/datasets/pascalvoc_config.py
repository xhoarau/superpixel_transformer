import numpy as np
import os.path as osp
import os


########################################################################
#                         Download information                         #
########################################################################

FORM_URL = "http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar"
SUP_FROM_URL = "https://www2.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/semantic_contours/benchmark.tgz"
ZIP_NAME = "VOCtrainval_11-May-2012.tar"
UNZIP_NAME = "VOCdevkit"
SUP_ZIP_NAME = "benchmark.tgz"
SUP_UNZIP_NAME = "benchmark_RELEASE"

########################################################################
#                                Labels                                #
########################################################################

# Credit: https://github.com/torch-points3d/torch-points3d

PascalVOC_NUM_CLASSES = 21

INV_OBJECT_LABEL = {
    0: "background",
    1: "aeroplane",
    2: "bicycle",
    3: "bird",
    4: "boat",
    5: "bottle",
    6: "bus",
    7: "car",
    8: "cat",
    9: "chair",
    10: "cow",
    11: "diningtable",
    12: "dog",
    13: "horse",
    14: "motorbike",
    15: "person",
    16: "pottedplant",
    17: "sheep",
    18: "sofa",
    19: "train",
    20: "tvmonitor"}

CLASS_NAMES = [INV_OBJECT_LABEL[i] for i in range(PascalVOC_NUM_CLASSES)] + ['ignored']

CLASS_COLORS = np.asarray([
    [  0,   0,   0], #background	-> black
    [128,   0,   0], #aeroplane		-> darkred
    [  0, 128,   0], #bicycle		-> green
    [128, 128,   0], #bird 			-> yellow
    [  0,   0, 128], #boat 			-> darkblue
    [128,   0, 128], #bottle 		-> purple
    [  0, 128, 128], #bus			-> cyan
    [128, 128, 128], #car			-> gray
    [ 64,   0,   0], #cat			-> brown
    [192,   0,   0], #chair			-> red
    [ 64, 128,   0], #cow			-> lawngreen
    [192, 128,   0], #diningtable	-> orange
    [ 64,   0, 128], #dog			-> indigo
    [192,   0, 128], #horse			-> magenta
    [ 64, 128, 128], #motorbike		-> turquoise
    [192, 128, 128], #person		-> pink
    [  0,  64,   0], #pottedplant	-> darkgreen
    [128,  64,   0], #sheep			-> saddlebrown
    [  0, 192,   0], #sofa			-> lime
    [128, 192,   0], #train			-> greenyellow
    [  0,  64, 128], #tvmonitor		-> blue
    [224, 224, 192]]) #unlabeled	-> white

OBJECT_LABEL = {name: i for i, name in INV_OBJECT_LABEL.items()}

def object_name_to_label(object_class):
    """Convert from object name to int label. By default, if an unknown
    object nale
    """
    object_label = OBJECT_LABEL.get(object_class, OBJECT_LABEL["background"])
    return object_label
