import os
import sys
import os.path as osp
import torch
import logging
import hashlib
import warnings
import time
import math
from datetime import datetime
#from itertools import product
from tqdm.auto import tqdm as tq
from torch_geometric.data import InMemoryDataset
from torch_geometric.data.dataset import files_exist
from torch_geometric.data.makedirs import makedirs
from torch_geometric.data.dataset import _repr
from src.data import NAG
from src.transforms import NAGSelectByKey, NAGRemoveKeys
from src.metrics import averageMeter
from src.utils.utils import save_norm
import multiprocessing as mp
from multiprocessing import RLock, Value, Array
import ctypes
#import numpy as np

DIR = os.path.dirname(os.path.realpath(__file__))
log = logging.getLogger(__name__)

__all__ = ['BaseDataset']


########################################################################
#                             BaseDataset                              #
########################################################################

class BaseDataset(InMemoryDataset):
    """Base class for datasets.

    Child classes must overwrite the following:

    ```
    MyDataset(BaseDataset):

        def class_names(self):
            pass

        def num_classes(self):
            pass

        def all_base_image_name(self): 
            pass

        def download_dataset(self):
            pass

        def read_single_raw_image(self):
            pass

        def processed_to_raw_path(self):
            zass

        def raw_file_structure(self) (optional):
            # Optional: only if your raw or processed file structure
            # differs from the default
            pass
        
        # New #
        def path_image(self,image_name):
            # Optional: only if your raw or processed file structure differs from the default
            pass

        def path_label(self,image_name):
            # Optional: only if your raw or processed file structure differs from the default
            pass
 
        def processed_to_raw_path(self):
            # Optional: only if your raw or processed file structure differs from the default
            pass
    ```


    Parameters
    ----------
    :param root: `str`
        Root directory where the dataset should be saved.
    :param stage: {'train', 'val', 'test', 'trainval'}, optional
    :param transform: `callable`, optional
        transform function operating on data.
    :param pre_transform: `callable`, optional
        pre_transform function operating on data.
    :param pre_filter: `callable`, optional
        pre_filter function operating on data.
    :param on_device_transform: `callable`, optional
        on_device_transform function operating on data,in the 'on_after_batch_transfer' hook. 
        This is where GPU-based augmentations should be, as well as any Transform you do not want to run in CPU-based DataLoaders
    :param save_[...]: select detail in saving method
    :param custom_hash: indicate a preprocessing to avoid duplicate (in case little variation in config files)
    :param in_memory: boolean
        load all data in RAM during setup
    :param pixel_[...]: detail of io for level 0
    :param segment_[...]: detail of io for level 1+
    :param test_subset: int or None
        number of image for test processing (see tests/test_preprocessing.py)
    :param measure_time: boolean
    :param setup: skip some step when only preprocessing (see src/datamodule/base.py)
    :param download_time: metric object for time measure (see datamodule)
    :param pre_process_time: metric object for time measure (see datamodule)
    :param queue: multiprocessing queue
        used to transmit profiling data from worker
    """

    def __init__(
            self,
            root,
            stage='train',
            transform=None,
            pre_transform=None,
            pre_filter=None,
            on_device_transform=None,
            save_y_to_csr=True,
            save_pos_dtype=torch.float,
            save_fp_dtype=torch.half,
            compress_files=False,
            custom_hash=None,
            in_memory=False,
            pixel_save_keys=None,
            pixel_no_save_keys=None,
            pixel_load_keys=None,
            segment_save_keys=None,
            segment_no_save_keys=None,
            segment_load_keys=None,
            load_low=0,
            load_high=-1,
            nano=False,
            test_subset=None,
            measure_time=False,
            setup=False,
            download_time=None,
            pre_process_time=None,
            queue=None,
            **kwargs):

        assert stage in ['train', 'val', 'trainval', 'test']

        # Set these attributes before calling parent `__init__` 
        # because some attributes will be needed in parent `download` and `process` methods
        self._stage = stage
        self.save_y_to_csr = save_y_to_csr
        self.save_pos_dtype = save_pos_dtype
        self.save_fp_dtype = save_fp_dtype
        self.compress_files = compress_files
        self.on_device_transform = on_device_transform
        self.custom_hash = custom_hash
        self.in_memory = in_memory
        self.pixel_save_keys = pixel_save_keys
        self.pixel_no_save_keys = pixel_no_save_keys
        self.pixel_load_keys = pixel_load_keys
        self.segment_save_keys = segment_save_keys
        self.segment_no_save_keys = segment_no_save_keys
        self.segment_load_keys = segment_load_keys
        self.load_low = load_low
        self.load_high = load_high
        if test_subset == None:
            self.test_subset = float('inf')
        else:
            self.test_subset = test_subset
        self.setup = setup 
        self.measure_time = measure_time
        if measure_time:
            self.lock = RLock()
            self.download_time = download_time
            self.pre_process_time = pre_process_time
            self.process_time_arr = Array(ctypes.c_float,4)
            self.process_time_count = Value('i',0)
            self.load_time_arr = Array(ctypes.c_float,4)
            self.load_time_count = Value('i',0)
            self.imageset_time_arr = Array(ctypes.c_float,4)
            self.imageset_time_count = Value('i',0)
            self.time_reste()

        self.q = queue

        self.class_weight = None # memorise class_weight to avoid repeat computation

        # Initialization with downloading and all preprocessing
        root = osp.join(root, self.data_subdir_name)
        super().__init__(root, transform, pre_transform, pre_filter)

         # Sanity check on the images name. Ensures image names are unique across all stages
        self.check_images_names()

        # Display the dataset pre_transform_hash and full path
        path = osp.join(self.processed_dir, f"{self.stage}", self.pre_transform_hash)
        log.info(f'Dataset hash: "{self.pre_transform_hash}"')
        log.info(f'Preprocessed data can be found at: "{path}"')
        
        # Load the processed data, if the dataset must be in memory
        if self.in_memory and self.setup:
            
            if self.measure_time:
                mes = time.time()

            log.info(f'Loading data in memory')

            if self.test_subset == float('inf'):
                nb_image = len(self)
            else:
                nb_image = min(len(self),self.test_subset)

            in_memory_data = [
                NAG.load(
                    self.processed_paths[i],
                    keys_low=self.pixel_load_keys,
                    keys=self.segment_load_keys,
                    compressed=self.compress_files)
                for i in tq(range(nb_image), ascii=True)]

            if self.measure_time:
                self.time_update(self.load_time_count, self.load_time_arr, time.time()-mes)
                mes = time.time()

            log.info(f'Process in memory data')

            if self.transform is not None:
                in_memory_data = [self.transform(x) for x in in_memory_data]

            if self.measure_time:
                self.time_update(self.process_time_count, self.process_time_arr, time.time()-mes)

            self._in_memory_data = in_memory_data
        else:
            self._in_memory_data = None

    ### set of method to measure time in workers ###

    def time_reste(self):
        self.lock.acquire()
        for i in range(4):
            self.process_time_arr[i] = 0
            self.load_time_arr[i] = 0
        self.lock.release()

    def download_time_update(self, val):
        self.lock.acquire()
        self.download_time.update(val)
        self.lock.release()

    def time_update(self, count, arr, val): # same as in metrics/averageMeter
        self.lock.acquire()
        if not torch.is_tensor(val):
            val = torch.tensor(val)
        val = val.to(torch.float)
        arr[1] = arr[1] + val
        if count.value !=0: #compute variance only if 2 values are stored
            arr[2] = (((count.value-1)*arr[2])/(count.value))+((count.value*(torch.mean(val)-arr[0])**2)/((count.value+1)*(count.value)))
        arr[3] = math.sqrt(arr[2]) # compute std
        count.value += 1 # keep total stored values
        arr[0] = arr[1] / count.value # compute average
        self.lock.release()
    
    @property
    def process_time_avg(self):
        return self.process_time_arr[0]

    @property
    def process_time_sum(self):
        return self.process_time_arr[1]

    @property
    def process_time_var(self):
        return self.process_time_arr[2]

    @property
    def process_time_std(self):
        return self.process_time_arr[3]

    @property
    def load_time_avg(self):
        return self.load_time_arr[0]

    @property
    def load_time_sum(self):
        return self.load_time_arr[1]

    @property
    def load_time_var(self):
        return self.load_time_arr[2]

    @property
    def load_time_std(self):
        return self.load_time_arr[3]

    @property
    def imageset_time_avg(self):
        return self.imageset_time_arr[0]

    @property
    def imageset_time_sum(self):
        return self.imageset_time_arr[1]

    @property
    def imageset_time_var(self):
        return self.imageset_time_arr[2]

    @property
    def imageset_time_std(self):
        return self.imageset_time_arr[3]

    ###   ###

    @property
    def class_names(self):
        """List of string names for dataset classes. 
        This list may be one-item larger than `self.num_classes` if the last label corresponds to 'unlabelled' or 'ignored' indices, 
        indicated as `-1` in the dataset labels.
        """
        raise NotImplementedError
    
    @property
    def class_colors(self):
        """array of size [nclass+1,3], with a color for each label and unlabeld
        """
        raise NotImplementedError

    @property
    def num_classes(self):
        """Number of classes in the dataset. 
        May be one-item smaller than `self.class_names`, 
        to account for the last class name being optionally used for 'unlabelled' or 'ignored' classes, 
        indicated as `-1` in the dataset labels.
        """
        raise NotImplementedError

    @property
    def data_subdir_name(self):
        return self.__class__.__name__.lower()

    @property
    def stage(self):
        """Dataset stage. Expected to be 'train', 'val', 'trainval', or 'test'
        """
        return self._stage

    @property
    def all_base_image_name(self):
        """Dictionary holding lists images names, for each stage.
        The following structure is expected: `{'train': [...], 'val': [...], 'test': [...]}`
        """
        raise NotImplementedError

    @property
    def image_names(self):
        """names of the dataset images, based on its `stage`.
        """
        if self.measure_time:
            mes = time.time()
        if self.stage == 'trainval':
            name = self.all_base_image_name['train'] + self.all_base_image_name['val']
        else:
            name = self.all_base_image_name[self.stage]
        out = sorted(list(set(name)))
        if self.measure_time:
            self.time_update(self.imageset_time_count, self.imageset_time_arr, time.time()-mes)
        return out

    def check_images_names(self):
        """Make sure the `all_base_image_name` are valid. More specifically, the image names must be unique across all stages """
        train = set(self.all_base_image_name['train'])
        val = set(self.all_base_image_name['val'])
        test = set(self.all_base_image_name['test'])

        assert len(train.intersection(val)) == 0 ,\
            "Image names must be unique across all the 'train' and 'val' stages"
        assert len(train.intersection(test)) == 0 ,\
            "Image names must be unique across all the 'train' and 'test' stages"
        assert len(val.intersection(test)) == 0 ,\
            "Image names must be unique across all the 'val' and 'test' stages"

    @property
    def raw_file_structure(self):
        """String to describe to the user the file structure of your dataset, at download time.
        """
        return None

    @property
    def raw_file_names(self):
        """The file paths to find in order to skip the download."""   
        return [self.name_to_relative_raw_path(x) for x in self.image_names]

    def name_to_relative_raw_path(self, image_name):
        """Given a image name as stored in `self.image_names`,
        return the path (relative to `self.raw_dir`) of the corresponding raw image.
        """
        return osp.join(image_name + '.png')

    @property
    def pre_transform_hash(self):
        """Produce a unique but stable hash based on the dataset's `pre_transform` attributes (as exposed by `_repr`).
        """
        if self.custom_hash is not None:
            return self.custom_hash
        if self.pre_transform is None:
            return 'no_pre_transform'
        return hashlib.md5(_repr(self.pre_transform).encode()).hexdigest()

    @property
    def processed_file_names(self):
        """The name of the files to find in the `self.processed_dir` folder in order to skip the processing
        """
        # For 'trainval', we use files from 'train' and 'val' to save
        # memory
        if not hasattr(self,"_processed_file_names"):
            if self.stage == 'trainval':
                path_set = [
                    osp.join(s, self.pre_transform_hash, f'{w}.h5')
                    for s in ('train', 'val')
                    for w in self.all_base_image_name[s]]
            else:
                path_set = [
                    osp.join(self.stage, self.pre_transform_hash, f'{w}.h5')
                    for w in self.image_names]
            self._processed_file_names = tuple(path_set) # save value to avoid repeating computation
        return self._processed_file_names

    def processed_to_raw_path(self, processed_path):
        """Given a processed image path from `self.processed_paths`, return the absolute path to the corresponding raw image.
        Overwrite this method if your raw data does not follow the default structure.
        """
        # Extract useful information from <path>
        stage, hash_dir, image_name = \
            osp.splitext(processed_path)[0].split('/')[-3:]

        # Read the raw image data
        raw_ext = osp.splitext(self.raw_file_names[0])[1]
        raw_path = osp.join(self.raw_dir, image_name + raw_ext)

        return raw_path

    @property
    def in_memory_data(self):
        """If the `self.in_memory`, this will return all processed data, loaded in memory. Returns None otherwise.
        """
        return self._in_memory_data

    @property
    def submission_dir(self):
        """Submissions are saved in the `submissions` folder, in the same hierarchy as `raw` and `processed` directories. 
        Each submission has a subdirectory of its own, named based on the date and time of creation.
        """
        submissions_dir = osp.join(self.root, "submissions")
        date = '-'.join([
            f'{getattr(datetime.now(), x)}'
            for x in ['year', 'month', 'day']])
        time = '-'.join([
            f'{getattr(datetime.now(), x)}'
            for x in ['hour', 'minute', 'second']])
        submission_name = f'{date}_{time}'
        path = osp.join(submissions_dir, submission_name)
        return path

    def download(self):

        if self.measure_time:
            mes = time.time()

        self.download_warning()
        self.download_dataset()

        if self.measure_time:
            self.download_time.update(time.time()-mes)

    def download_dataset(self):
        """Download the dataset data. Modify this method to implement your own `BaseDataset` child class.
        """
        raise NotImplementedError

    def download_warning(self, interactive=False):
        # Warning message for the user about to download
        log.info(
            f"WARNING: You are about to download {self.__class__.__name__} "
            f"data.")
        if self.raw_file_structure is not None:
            log.info("Files will be organized in the following structure:")
            log.info(self.raw_file_structure)
        log.info("")
        if interactive:
            log.info("Press any key to continue, or CTRL-C to exit.")
            input("")
            log.info("")

    def download_message(self, msg):
        log.info(f'Downloading "{msg}" to {self.raw_dir}...')

    def _process(self):
        """Overwrites torch-geometric's Dataset._process. 
        This simply removes the 'pre_transform.pt' file used for checking whetherthe pre-transforms have changed. 
        This is possible thanks to our `pre_transform_hash` mechanism.
        """

        # create and transmit to transforms the name of the file with global information on dataset (normalisation values ...)
        global_file_name = osp.join(self.processed_dir,self.stage,self.pre_transform_hash + ".h5")
        for t in self.on_device_transform.transforms:
            if hasattr(t,'global_file_name'):
                t.global_file_name=global_file_name

        f = osp.join(self.processed_dir, 'pre_filter.pt')
        if osp.exists(f) and torch.load(f) != _repr(self.pre_filter):
            warnings.warn(
                "The `pre_filter` argument differs from the one used in "
                "the pre-processed version of this dataset. If you want to "
                "make use of another pre-fitering technique, make sure to "
                "delete '{self.processed_dir}' first")

        if self.test_subset == float('inf'):
            nb_image = len(self)
        else:
            nb_image = min(len(self),self.test_subset)
        
        if files_exist(self.processed_paths[:nb_image]) and (self.test_subset==float('inf') or self.setup or self.custom_hash is not None):  # pragma: no cover
            if len(self.processed_paths)!=0:
                name,ext = osp.splitext(global_file_name)
                self.class_weight = torch.load(name+"_"+'cuda'+ext)[-1]
            return
        
        if self.log and 'pytest' not in sys.modules:
            print('Processing...', file=sys.stderr)

        makedirs(self.processed_dir)
        self.process()

        path = osp.join(self.processed_dir, 'pre_filter.pt')
        torch.save(_repr(self.pre_filter), path)

        if self.log and 'pytest' not in sys.modules:
            print('Done!', file=sys.stderr)
        
        normalize = None
        for t in self.pre_transform.transforms:
            if hasattr(t,'normalize'):
                normalize = t.normalize.copy()
                t.normalize = [] # need to be reset to keep a constant hash
                break 
       
        name,ext = os.path.splitext(global_file_name)
        if len(self.processed_paths)!=0:
            if len(os.listdir(name)) <= self.test_subset:
                if normalize is not None:
                    save_norm(global_file_name,normalize)
                    self.class_weight = normalize[-1]
                else:
                    torch.save([],name+"_"+'cpu'+ext)
                    torch.save([],name+"_"+'cuda'+ext)
        else:
            torch.save([],name+"_"+'cpu'+ext)
            torch.save([],name+"_"+'cuda'+ext)
        
    def process(self):
        hash_dir = self.pre_transform_hash
        train_dir = osp.join(self.processed_dir, 'train', hash_dir)
        val_dir = osp.join(self.processed_dir, 'val', hash_dir)
        test_dir = osp.join(self.processed_dir, 'test', hash_dir)
        if not osp.exists(train_dir):
            os.makedirs(train_dir, exist_ok=True)
        if not osp.exists(val_dir):
            os.makedirs(val_dir, exist_ok=True)
        if not osp.exists(test_dir):
            os.makedirs(test_dir, exist_ok=True)

        # Process images one by one
        if self.test_subset == float('inf'):
            for p in tq(self.processed_paths, ascii=True):
                self._process_single_image(p)
        else:
            test_images = self.processed_paths[:self.test_subset]
            for p in tq(test_images, ascii=True):
                self._process_single_image(p)

    def _process_single_image(self, image_path):
        """Internal method called by `self.process` to preprocess a
        single image.
        """
        if self.measure_time:
            mes = time.time()
        
        # If required files exist, skip processing
        if (self.test_subset == float('inf') or self.setup or self.custom_hash is not None) and osp.exists(image_path):
            return

        # Create necessary parent folders if need be
        os.makedirs(osp.dirname(image_path), exist_ok=True)

        # Read the raw image corresponding to the final processed 'image_path` and convert it to a Data object
        raw_path = self.processed_to_raw_path(image_path)
        data = self.read_single_raw_image(raw_path)

        if getattr(data, 'y', None) is not None:
            data.y[data.y == -1] = self.num_classes
        
        # Apply pre_transform
        if self.pre_transform is not None:
            nag = self.pre_transform(data)
        else:
            nag = NAG([data])

        # To save some disk space, we discard some level-0 attributes
        if self.pixel_save_keys is not None:
            keys = set(nag[0].keys) - set(self.pixel_save_keys)
            nag = NAGRemoveKeys(level=0, keys=keys)(nag)
        elif self.pixel_no_save_keys is not None:
            nag = NAGRemoveKeys(level=0, keys=self.pixel_no_save_keys)(nag)
        if self.segment_save_keys is not None:
            keys = set(nag[1].keys) - set(self.segment_save_keys)
            nag = NAGRemoveKeys(level='1+', keys=keys)(nag)
        elif self.segment_no_save_keys is not None:
            nag = NAGRemoveKeys(level='1+', keys=self.segment_no_save_keys)(nag)
        # Save pre_transformed data to the processed dir/<path>
        nag.save(
            image_path,
            y_to_csr=self.save_y_to_csr,
            pos_dtype=self.save_pos_dtype,
            fp_dtype=self.save_fp_dtype,
            compress=self.compress_files)
        
        if self.measure_time:
            self.pre_process_time.update(time.time()-mes)

        del nag

    def recompute_norm(self):
        print("recompute_norm")
        path = osp.join(self.processed_dir,self.stage,self.pre_transform_hash + ".h5")
        name,ext = osp.splitext(path)
        list_nags = os.listdir(name)
        normalize_list = []
        mean_keys = None
        std_keys = None
        for nag_name in tq(list_nags, ascii=True):
            nag = NAG.load(osp.join(name,nag_name),compresse=self.compress_files)
            while len(normalize_list)<nag.num_levels:
                normalize_list.append({})
            for i_level in range(1,nag.num_levels):
                normalize = normalize_list[i_level]
                data = nag[i_level]
                keys = data.keys
                sub_size = nag.get_sub_size(i_level,low=0)
                if 'global_mean_size' in keys:
                    if 'global_mean_size' not in normalize:
                        normalize['global_mean_size'] = [averageMeter()]
                    normalize['global_mean_size'][0].update(sub_size.float().mean())
 
                if 'graph_size' in keys:
                    if 'graph_size' not in normalize:
                        normalize['graph_size'] = [averageMeter()]
                    normalize['graph_size'][0].update(sub_size.shape[0])
         
                if 'node_size' in keys:
                    if 'node_size' not in normalize:
                        normalize['node_size'] = [averageMeter()]
                    normalize['node_size'][0].update(data.node_size)
            
                if 'log_size' in keys:
                    if 'log_size' not in normalize:
                        normalize['log_size'] = [averageMeter()]
                    normalize['log_size'][0].update(data.log_size)

                if 'perimeter' in keys:
                    if 'perimeter' not in normalize:
                        normalize['perimeter'] = [averageMeter()]
                normalize['perimeter'][0].update(data.perimeter)

                if 'log_perimeter' in keys:
                    if 'log_perimeter' not in normalize:
                        normalize['log_perimeter'] = [averageMeter()]
                    normalize['log_perimeter'][0].update(data.log_perimeter)

                if 'pca_val' in keys:
                    if 'pca_val' not in normalize:
                        normalize['pca_val'] = [averageMeter() for _ in range(data[f'pca_val'].shape[1])]
                    for i in range(data['pca_val'].shape[1]):
                        normalize['pca_val'][i].update(data['pca_val'][:,i])

                if 'pca_ratio' in keys:
                    if 'pca_ratio' not in normalize:
                        normalize['pca_ratio'] = [averageMeter()]
                    normalize['pca_ratio'][0].update(data.pca_ratio)

                if mean_keys is None:
                    mean_keys = [x for x in data.keys if x.startswith("mean")]

                if std_keys is None:
                    std_keys = [x for x in data.keys if x.startswith("std")]

                for key in mean_keys:
                    if key not in normalize:
                        normalize[key] = [averageMeter() for _ in range(data[key].shape[1])]
                    for i in range(data[key].shape[1]):
                        normalize[key][i].update(data[key][:,i])
                
                for key in std_keys:
                    if key not in normalize:
                        normalize[key] = [averageMeter() for _ in range(data[key].shape[1])]
                    for i in range(data[key].shape[1]):
                        normalize[key][i].update(data[key][:,i])
        
                if 'interface_length' in keys:
                    if 'interface_length' not in normalize:
                        normalize['interface_length'] = [averageMeter()]
                    normalize['interface_length'][0].update(data["interface_length"])

                if 'centroid_dist' in keys:
                    if 'centroid_dist' not in normalize:
                        normalize['centroid_dist'] = [averageMeter()]
                    normalize['centroid_dist'][0].update(data["centroid_dist"])
                
            if len(normalize_list)==nag.num_levels:
                normalize_list.append(torch.zeros(self.num_classes,device=nag.device))
            normalize_list[nag.num_levels] += nag[nag.num_levels-1].y.sum(dim=0)[:self.num_classes]

        save_norm(path,normalize_list)

    def read_single_raw_image(self, raw_image_path):
        """Read a single raw image and return a Data object, ready to be passed to `self.pre_transform`.
        """
        raise NotImplementedError

    def get_class_weight(self, smooth='sqrt'):
        """Compute class weights based on the labels distribution in thedataset. 
        Optionally a 'smooth' function may be passed to smoothen the weights' statistics.
        """
        assert smooth in [None, 'sqrt', 'log']
        
        # Compute the class weights. Optionally, a 'smooth' function may be applied to smoothen the weights statistics
        if smooth == 'sqrt':
            counts = self.class_weight.sqrt()
        elif smooth == 'log':
            counts = self.class_weight.log()
        else:
            counts = self.class_weight.clone()
        
        weights = 1 / (counts + 1)
        weights /= weights.sum()

        return weights

    def __len__(self):
        """Number of image in the dataset."""
        return len(self.image_names)

    def __getitem__(self, idx):
        """Load a preprocessed NAG from disk and apply `self.transform` if any. 
        Optionally, one may pass a tuple (idx, bool) where the boolean indicates whether the data should be loaded from disk, 
        if `self.in_memory=True`.
        """
        if self.q is not None:
            start = time.time()
        # Prepare from_hdd
        from_hdd = False
        if isinstance(idx, tuple):
            assert len(idx) == 2 and isinstance(idx[1], bool), "Only supports indexing with `str` or `(str, bool)` where the" \
                " boolean indicates whether the data should be loaded from disk, when `self.in_memory=True`."
            idx, from_hdd = idx

        # Get the processed NAG directly from RAM
        if self.in_memory and self.setup and not from_hdd:
            return self.in_memory_data[idx]
        
        if self.measure_time:
            mes = time.time()
        
        # Read the NAG from HDD
        low = getattr(self,"load_low",0)
        nag = NAG.load(
            self.processed_paths[idx],
            low = low,
            high = getattr(self,"load_high",-1),
            keys_low = self.pixel_load_keys if low == 0 else self.segment_load_keys,
            keys = self.segment_load_keys,
            compressed = self.compress_files)
        nag.debug(True,True)
        if self.q is not None:
            self.q.put((os.getpid(),'r',start,time.time()))
        
        if self.q is not None:
            start = time.time() 
        if self.measure_time:
            self.time_update(self.load_time_count, self.load_time_arr, time.time()-mes)
            mes = time.time()

        # Apply transforms
        nag = nag if self.transform is None else self.transform(nag)
        
        if self.measure_time:
            self.time_update(self.process_time_count, self.process_time_arr, time.time()-mes)
        if self.q is not None:
            self.q.put((os.getpid(),'g',start,time.time()))
        return nag

    def make_submission(self, name, pred, pos, submission_dir=None):
        """Implement this if your dataset needs to produce data in a given format for submission. 
        This is typically needed for datasets with held-out test sets.
        """
        raise NotImplementedError

    def finalize_submission(self, submission_dir):
        """Implement this if your dataset needs to produce data in a given format for submission. 
        This is typically needed for datasets with held-out test sets.
        """
        raise NotImplementedError
   
    def __repr__(self):
        return f'{self.__class__.__name__}, directory : {self.root}, used hash : {self.pre_transform_hash}'
