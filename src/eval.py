import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

# ------------------------------------------------------------------------------------ #
# `pyrootutils.setup_root(...)` is an optional line at the top of each entry file
# that helps to make the environment more robust and convenient
#
# the main advantages are:
# - allows you to keep all entry files in "src/" without installing project as a package
# - makes paths and scripts always work no matter where is your current work dir
# - automatically loads environment variables from ".env" file if exists
#
# how it works:
# - the line above recursively searches for either ".git" or "pyproject.toml" in present
#   and parent dirs, to determine the project root dir
# - adds root dir to the PYTHONPATH (if `pythonpath=True`), so this file can be run from
#   any place without installing project as a package
# - sets PROJECT_ROOT environment variable which is used in "configs/paths/default.yaml"
#   to make all paths always relative to the project root
# - loads environment variables from ".env" file in root dir (if `dotenv=True`)
#
# you can remove `pyrootutils.setup_root(...)` if you:
# 1. either install project as a package or move each entry file to the project root dir
# 2. simply remove PROJECT_ROOT variable from paths in "configs/paths/default.yaml"
# 3. always run entry files from the project root dir
#
# https://github.com/ashleve/pyrootutils
# ------------------------------------------------------------------------------------ #

# Hack importing pandas here to bypass some conflicts with hydra
import pandas as pd

from typing import List, Tuple

import hydra
import torch
torch.multiprocessing.set_sharing_strategy('file_system') # command to solve pin memory problème on my machine
import torch_geometric
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import LightningDataModule, LightningModule, Trainer
from pytorch_lightning.loggers import Logger
import os
from tqdm import tqdm

from src.data import NAG
import os.path as osp
from src.visualization.visualization import visualize
from src.metrics import *

from src import utils
from time import time
import csv

from matplotlib import pyplot as plt

# Registering the "eval" resolver allows for advanced config
# interpolation with arithmetic operations:
# https://omegaconf.readthedocs.io/en/2.3_branch/how_to_guides.html
OmegaConf.register_new_resolver("eval", eval)

log = utils.get_pylogger(__name__)


@utils.task_wrapper
def evaluate(cfg: DictConfig) -> Tuple[dict, dict]:
    """Evaluates given checkpoint on a datamodule testset.

    This method is wrapped in optional @task_wrapper decorator which applies extra utilities
    before and after the call.

    Args:
        cfg (DictConfig): Configuration composed by Hydra.

    Returns:
        Tuple[dict, dict]: Dict with metrics and dict with all instantiated objects.
    """
    # enforce cuda device id to the chosen one
    if hasattr(cfg,"device_id") and cfg.trainer.get("accelerator") == "gpu":
        assert cfg.device_id < torch.cuda.device_count()
        torch.cuda.set_device("cuda:"+str(cfg.device_id))
        os.sched_setaffinity(0,list(range(int(torch.multiprocessing.cpu_count()/torch.cuda.device_count())*cfg.device_id,int(torch.multiprocessing.cpu_count()/torch.cuda.device_count())*(cfg.device_id+1))))
        device = "cuda:" + str(cfg.device_id)
    else:
        device = "cpu"

    assert cfg.ckpt_path

    log.info(f"Instantiating datamodule <{cfg.datamodule._target_}>")
    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule)

    log.info(f"Instantiating model <{cfg.model._target_}>")
    model: LightningModule = hydra.utils.instantiate(cfg.model)

    #checkpoint = torch.load(cfg.ckpt_path)
    #model.load_state_dict(checkpoint['state_dict'])

    log.info("Instantiating loggers...")
    logger: List[Logger] = utils.instantiate_loggers(cfg.get("logger"))

    log.info(f"Instantiating trainer <{cfg.trainer._target_}>")
    trainer: Trainer = hydra.utils.instantiate(cfg.trainer, logger=logger)
    if float('.'.join(torch.__version__.split('.')[:2])) >= 2.0:
        torch.set_float32_matmul_precision(cfg.float32_matmul_precision)

    object_dict = {
        "cfg": cfg,
        "datamodule": datamodule,
        "model": model,
        "logger": logger,
        "trainer": trainer,
    }

    if logger:
        log.info("Logging hyperparameters!")
        utils.log_hyperparameters(object_dict)
    
    if cfg.get("compile"):
        log.info("Compiling model!")
        model = torch_geometric.compile(model)
    
    log.info("Starting testing!")
    if cfg.get("validate"):
        val = trainer.validate(model=model, datamodule=datamodule, ckpt_path=cfg.ckpt_path)
    
    if cfg.get("print_in_file"):
        out_file = open(osp.join(cfg.paths.output_dir,"evaluation.txt"),"w")
    else:
        out_file = None

    datamodule.setup()
    datamodule.val_dataset.pixel_load_keys += ["label"]

    checkpoint = torch.load(cfg.ckpt_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.to(device)
    model = model.eval()

    if cfg.get("validate_train_set"):
        log.info("validate_train_set")
        cm = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm_node = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm.to(device)
        cm_node.to(device)
        mean_loss = averageMeter()
        mean_loss.to(device)
        for batch in tqdm(datamodule.train_dataloader(), ascii=True):
            ### check tensor reconstruction ###
            if isinstance(batch,(tuple,list)):
                batch = NAG.from_tensor(batch[0],batch[1])
            if batch.device != device:
                batch.to(device)
            loss, preds, targets = model.model_step(datamodule.on_device_val_transform(batch))
            mean_loss.update(loss)
            cm.update(preds,targets)
            targets_node = targets.argmax(1) 
            cm_node.update(preds,targets_node)
        print("mean loss : ",mean_loss.avg.item(),file=out_file)
        print("std loss : ",mean_loss.std.item(),file=out_file)
        cm.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        print(" ",file=out_file)
        cm_node.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        del mean_loss
        del cm
        del cm_node
    
    if cfg.get("validate_val_set"):
        log.info("validate_val_set")
        cm = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm_node = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm.to(device)
        cm_node.to(device)
        mean_loss = averageMeter()
        mean_loss.to(device)
        for batch in tqdm(datamodule.val_dataloader(), ascii=True):
            ### check tensor reconstruction ###
            if isinstance(batch,(tuple,list)):
                batch = NAG.from_tensor(batch[0],batch[1])
            if batch.device != device:
                batch.to(device)
            loss, preds, targets = model.model_step(datamodule.on_device_val_transform(batch))
            mean_loss.update(loss)
            cm.update(preds,targets)
            targets_node = targets.argmax(1)
            cm_node.update(preds,targets_node)
        print("mean loss : ",mean_loss.avg.item(),file=out_file)
        print("std loss : ",mean_loss.std.item(),file=out_file)
        cm.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        print(" ",file=out_file)
        cm_node.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        del cm
        del cm_node
        del mean_loss

    if cfg.get("test"):
        log.info("testing")
        cm = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm_node = ConfusionMatrix(cfg.datamodule.num_classes, ignore_index=cfg.datamodule.num_classes)
        cm.to(device)
        cm_node.to(device)
        mean_loss = averageMeter()
        mean_loss.to(device)
        for batch in tqdm(datamodule.test_dataloader(), ascii=True):
            ### check tensor reconstruction ###
            if isinstance(batch,(tuple,list)):
                batch = NAG.from_tensor(batch[0],batch[1])
            if batch.device != device:
                batch.to(device)
            loss, preds, targets = model.model_step(datamodule.on_device_test_transform(batch))
            mean_loss.update(loss)
            cm.update(preds,targets)
            targets_node = targets.argmax(1)
            cm_node.update(preds,targets_node)
        print("mean loss : ",mean_loss.avg.item(),file=out_file)
        print("std loss : ",mean_loss.std.item(),file=out_file)
        cm.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        print(" ",file=out_file)
        cm_node.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        del cm
        del cm_node
        del mean_loss

    if cfg.get("visualisation"):
        log.info("visualization predictions")
        compressed = datamodule.train_dataset.compress_files

        for i in range(cfg.nb_visu):
            if cfg.get("validate_train_set"):
                log.info(f"visualization train set {i}")
                nag = datamodule.train_dataset.__getitem__(i).to(device)
                loss, preds, targets = model.model_step(datamodule.on_device_val_transform(nag))
                nag = NAG.load(datamodule.train_dataset.processed_paths[i], compressed = compressed)
                data = nag[0]
                data.pred = preds[data.super_index]
                visualize(nag,osp.join(cfg.paths.output_dir,f"visu_{i}_train"),keys=["image","label","Pred"],
                    straight_line_tol=cfg.datamodule.straight_line_tol if cfg.datamodule.straight_line_tol is not None else 1.,
                    class_color=datamodule.val_dataset.class_colors)
            if cfg.get("validate_val_set"):
                log.info(f"visualization val set {i}")
                nag = datamodule.val_dataset.__getitem__(i).to(device)
                loss, preds, targets = model.model_step(datamodule.on_device_val_transform(nag))
                nag = NAG.load(datamodule.val_dataset.processed_paths[i], compressed=compressed)
                data = nag[0]
                data.pred = preds[data.super_index]
                visualize(nag,osp.join(cfg.paths.output_dir,f"visu_{i}_val"),keys=["image","label","Pred"],
                    straight_line_tol=cfg.datamodule.straight_line_tol if cfg.datamodule.straight_line_tol is not None else 1.,
                    class_color=datamodule.val_dataset.class_colors)
            if cfg.get("test"):
                log.info(f"visualization test set {i}")
                nag = datamodule.test_dataset.__getitem__(i).to(device)
                loss, preds, targets = model.model_step(datamodule.on_device_test_transform(nag))
                nag = NAG.load(datamodule.test_dataset.processed_paths[i], compressed=compressed)
                data = nag[0]
                data.pred = preds[data.super_index]
                visualize(nag,osp.join(cfg.paths.output_dir,f"visu_{i}_test"),keys=["image","label","Pred"],
                    straight_line_tol=cfg.datamodule.straight_line_tol if cfg.datamodule.straight_line_tol is not None else 1.,
                    class_color=datamodule.test_dataset.class_colors)
                
    if cfg.get("check_preprocessing"):
        log.info("checking preprocessing")
        nlevel = len(cfg.datamodule.pcp_regularization)
        
        # keep track of size and quality of over segmentation
        meters = []
        for l in range(nlevel):
            meters.append(averageMeter())
        cm = ConfusionMatrix(cfg.datamodule.num_classes)
        i = 0
        for batch in tqdm(datamodule.train_dataloader(), ascii=True):
            for nag in batch:
                for l in range(nlevel):
                    sizes = nag.get_sub_size(l+1).numpy()
                    meters[l].update(sizes)
                cm.update(nag[1].y.argmax(dim=1)[nag.get_super_index(high=1,low=0)],nag[0].y)
                i = i + 1
                if i == cfg.nb_check:
                    break
            if i == cfg.nb_check:
                break
        
        for l in range(nlevel):
            print("avg sp size level ",l+1," : ",meters[l].avg,file=out_file)
            print("std sp size level ",l+1," : ",meters[l].std,file=out_file)
        cm.print_metrics(class_names = datamodule.train_dataset.class_names,f=out_file)
        del cm
        del meters

    metric_dict = trainer.callback_metrics

    del model
    del datamodule
    del checkpoint

    return metric_dict, object_dict


@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="eval.yaml")
def main(cfg: DictConfig) -> None:
    #print(cfg)
    with torch.no_grad():
        evaluate(cfg)

if __name__ == "__main__":
    main()
