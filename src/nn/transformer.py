import torch
from torch import nn
from src.nn import SelfAttentionBlock, FFN, DropPath, LayerNorm, \
    INDEX_BASED_NORMS, Topological_SelfAttentionBlock
from torch.profiler import record_function

__all__ = ['TransformerBlock']


class TransformerBlock(nn.Module):
    """Base block of the Transformer architecture:

        x ------------------------------------- + -->
            \                   \               ^
             - N --- SA -- [] -- + -- N -- FFN -|
                   \       ^
                    - TSA -|
                    (optional)
    Where:
        - N: Normalization
        - SA: Self-Attention
        - TSA: Topological Self-Attention (optional)
        - FFN: Feed-Forward Network

    Inspired by: https://github.com/microsoft/Swin-Transformer
    """

    def __init__(
            self,
            dim,
            num_heads=1,
            qkv_bias=True,
            k_in_dim=3, # only fot TSA
            qk_dim=8,
            qk_scale=None,
            in_rpe_dim=18,
            ffn_ratio=4,
            residual_drop=None,
            attn_drop=None,
            drop_path=None,
            activation=nn.LeakyReLU(),
            norm=LayerNorm,
            pre_norm=True,
            no_sa=False,
            no_ffn=False,
            k_rpe=False,
            q_rpe=False,
            v_rpe=False,
            k_delta_rpe=False,
            q_delta_rpe=False,
            qk_share_rpe=False,
            q_on_minus_rpe=False,
            rpe_fusion='add',
            mlp_rpe=0,
            heads_share_rpe=False,
            topological_attention=False):
        super().__init__()

        self.dim = dim
        self.pre_norm = pre_norm
        self.topological_attention = topological_attention
        
        # Self-Attention residual branch
        self.no_sa = no_sa
        if not no_sa:
            self.sa_norm = norm(dim)
            self.sa = SelfAttentionBlock(
                dim,
                num_heads=num_heads,
                in_dim=None,
                out_dim=dim//2+dim%2 if topological_attention else dim, # outdim is half round up of total dim for concatenation with sat
                qkv_bias=qkv_bias,
                qk_dim=qk_dim,
                qk_scale=qk_scale,
                in_rpe_dim=in_rpe_dim,
                attn_drop=attn_drop,
                drop=residual_drop,
                k_rpe=k_rpe,
                q_rpe=q_rpe,
                v_rpe=v_rpe,
                k_delta_rpe=k_delta_rpe,
                q_delta_rpe=q_delta_rpe,
                qk_share_rpe=qk_share_rpe,
                q_on_minus_rpe=q_on_minus_rpe,
                heads_share_rpe=heads_share_rpe,
                rpe_fusion=rpe_fusion,
                mlp_rpe=mlp_rpe,
                norm=norm)
            if topological_attention:
                self.sat = Topological_SelfAttentionBlock(
                    dim,
                    num_heads=num_heads,
                    in_dim=None,
                    out_dim=dim//2,# outdim is half round down of total dim for concatenation with sa
                    qkv_bias=qkv_bias,
                    k_in_dim=k_in_dim,
                    qk_dim=qk_dim,
                    qk_scale=qk_scale,
                    in_rpe_dim=in_rpe_dim,
                    attn_drop=attn_drop,
                    drop=residual_drop,
                    v_rpe=v_rpe,
                    heads_share_rpe=heads_share_rpe,
                    rpe_fusion=rpe_fusion,
                    mlp_rpe=mlp_rpe,
                    norm=norm)

        # Feed-Forward Network residual branch
        self.no_ffn = no_ffn
        if not no_ffn:
            self.ffn_norm = norm(dim)
            self.ffn_ratio = ffn_ratio
            self.ffn = FFN(
                dim,
                hidden_dim=int(dim * ffn_ratio),
                activation=activation,
                drop=residual_drop)

        # Optional DropPath module for stochastic depth
        self.drop_path = DropPath(drop_path) \
            if drop_path is not None and drop_path > 0 else nn.Identity()

    def forward(self, x, norm_index, edge_index=None, edge_attr=None, topology=None):
        """
        :param x: FloatTensor or shape (N, C)
            Node features
        :param norm_index: LongTensor or shape (N)
            Node indices for the LayerNorm
        :param edge_index: LongTensor of shape (2, E)
            Edges in torch_geometric [[sources], [targets]] format for
            the self-attention module
        :param edge_attr: FloatTensor of shape (E, F)
            Edge attributes in torch_geometric format for relative pose
            encoding in the self-attention module
        :param topology: FloatTensor of shape (E, 3)
            Node direction and distances for Topological_Attention
        :return:
        """
        assert x.dim() == 2, 'x should be a 2D Tensor'
        assert x.is_floating_point(), 'x should be a 2D FloatTensor'
        assert norm_index.dim() == 1 and norm_index.shape[0] == x.shape[0], \
            'norm_index should be a 1D LongTensor'
        assert edge_index is None or \
               (edge_index.dim() == 2 and not edge_index.is_floating_point()), \
            'edge_index should be a 2D LongTensor'
        assert edge_attr is None or \
               (edge_attr.dim() == 2 and edge_attr.shape[0] == edge_index.shape[1]),\
            'edge_attr be a 2D Tensor of the same number of edges as in edge_index'

        # Keep track of x for the residual connection
        shortcut = x

        # Self-Attention residual branch. Skip the SA block if no edges
        # are provided
        with record_function("self attention"):
            if self.no_sa or edge_index is None or edge_index.shape[1] == 0:
                pass
            elif self.pre_norm:
                x = self._forward_norm(self.sa_norm, x, norm_index)
                if self.topological_attention:
                    x= torch.cat((self.sa(x, edge_index, edge_attr=edge_attr, norm_index=norm_index),
                        self.sat(x, edge_index, edge_attr=edge_attr, topology=topology, norm_index=norm_index)), dim=1)
                else:
                    x = self.sa(x, edge_index, edge_attr=edge_attr, norm_index=norm_index)
                x = shortcut + self.drop_path(x)
            else:
                if self.topological_attention:
                    x= torch.cat((self.sa(x, edge_index, edge_attr=edge_attr, norm_index=norm_index),
                        self.sat(x, edge_index, edge_attr=edge_attr, topology=topology, norm_index=norm_index)), dim=1)
                else:
                    x = self.sa(x, edge_index, edge_attr=edge_attr, norm_index=norm_index)
                x = self.drop_path(x)
                x = self._forward_norm(self.sa_norm, shortcut + x, norm_index)

        # Feed-Forward Network residual branch
        with record_function("FFN"):
            if not self.no_ffn and self.pre_norm:
                x = self._forward_norm(self.ffn_norm, x, norm_index)
                x = self.ffn(x)
                x = shortcut + self.drop_path(x)
            if not self.no_ffn and not self.pre_norm:
                x = self.ffn(x)
                x = self.drop_path(x)
                x = self._forward_norm(self.ffn_norm, shortcut + x, norm_index)

        return x, norm_index, edge_index

    @staticmethod
    def _forward_norm(norm, x, norm_index):
        """Simple helper for the forward pass on norm modules. Some
        modules require an index, while others don't.
        """
        if isinstance(norm, INDEX_BASED_NORMS):
            return norm(x, batch=norm_index)
        return norm(x)

    @property
    def out_dim(self):
        return self.dim
