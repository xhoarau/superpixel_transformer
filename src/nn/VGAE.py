import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import global_add_pool,global_mean_pool,global_max_pool
from torch_geometric.nn.models import VGAE,InnerProductDecoder
from src.nn import MLP,GraphNorm
from src.nn import ConvBlock

import os
import numpy as np

EPS = 1e-15

"""
alternative to the encodeur for shape and interfaces with an variational autoencoder
recreate graph and features

In developement, need batched negative edge sampling and feat loss
"""

__all__ = [] # "VGAE_polygones"] TODO finish compatibility

class Decoder(InnerProductDecoder):
    """ Evolution of InnerProductDecoder with MLP projection

    :param embedding_dims: list of ints 
        dimentions of the embedding_projection_MLP layers
    :param node_dims: list of ints (optional)
        dimentions of the node_projection_MLP layers
        None to deactivate projection
    :param norm: norm module to use in all MLPs
    """
    def __init__(self, feat_dims, node_dims=None, act_fn=nn.LeakyReLU(), norm=GraphNorm):
        super().__init__()
        self.node_proj = MLP(node_dims, activation=act_fn, last_activation=False, norm=norm) if node_dims is not None else None
        if len(feat_dims) > 2:
            self.feat_proj = nn.Sequential(MLP(feat_dims[:-1], activation=act_fn, last_activation=True, norm=norm), nn.Linear(feat_dims[-2], feat_dims[-1])) # no norm at last layer for reconstruction
        else:
            self.feat_proj = nn.Linear(feat_dims[0], feat_dims[1])

    def forward(self,z, edge_index, sigmoid=True): # add the node projection
        z_proj = self.node_proj(z) if self.node_proj is not None else z
        return super().forward(z_proj, edge_index, sigmoid)
    
    def forward_all(self, z, sigmoid=True, embedding=None): # add node projection and feature reconstruction
        """ only for one graph at a time """
        z_proj = self.node_proj(z) if self.node_proj is not None else z
        A_pred = super().forward_all(z_proj, sigmoid)
        if embedding is not None:
            X_pred = self.feat_proj(torch.cat((z_proj, embedding.repeat(z_proj.shape[0],1)), dim=1))
        else:
            X_pred = None
        return A_pred,X_pred

    def forward_embbeding(self,z,embedding): # compute feature reconstruction
        z_proj = self.node_proj(z) if self.node_proj is not None else z
        X_pred = self.feat_proj(torch.cat((z_proj, embedding.repeat(z_proj.shape[0],1)), dim=1))
        return X_pred
         
class Encoder(nn.Module):
    def __init__(self, conv_layer_type='gcnconv', base_dims=[8,8], mean_dims=[8,8], act_fn=nn.LeakyReLU(), norm=GraphNorm, agg='add', k=20):
        """
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 
            'edgeconv', 'dynamicedgeconv', 'ginconv', 'gcnconv', 'gatconv', 'sageconv', 'genconv', 'transformerconv', 'gatv2conv'
        :param base_dims: list of ints
            dimention of the base_gcn
            see src.nn.graph_conv for details
        :param mean_dims: list of ints
            dimention of the gcn_mean and gcn_logstd
            see src.nn.graph_conv for details
        :param agg: aggregation in EdgeConv : 'add', 'mean', 'max'
        :param act_fn: activation function: torch.nn.Module
        :param k: number of neigbors for DynamicEdgeConv
        """
        super().__init__()
        self.base_gcn = ConvBlock(conv_layer_type, base_dims, act_fn=act_fn, norm=norm, agg=agg, k=k)
        self.gcn_mean = ConvBlock(conv_layer_type, mean_dims, act_fn=act_fn, last_activation=False, norm=norm, agg=agg, k=k)
        self.gcn_logstd = ConvBlock(conv_layer_type, mean_dims, act_fn=act_fn, last_activation=False, norm=norm, agg=agg, k=k)

    def forward(self, X, edge_index, edge_weight=None):
        x = self.base_gcn(X, edge_index, edge_weight)
        mean = self.gcn_mean(x, edge_index)
        logstd = self.gcn_logstd(x, edge_index)
        return mean, logstd

class Pooler(nn.Module):
    """ global pooling module

    :param embedding_dims: list of ints 
        dimentions of the embedding_projection_MLP layers
    :param node_dims: list of ints (optional)
        dimentions of the node_projection_MLP layers
        None to deactivate projection
    :param norm: norm module to use in all MLPs (optional)
    :param pool: str (optional)
        pooling method in "add", "mean", "max"
    """

    def __init__(self, embedding_dims, node_dims=None, pool="add", act_fn=nn.LeakyReLU(), norm=GraphNorm):
        super(Pooler,self).__init__()
        self.node_proj = MLP(node_dims, activation=act_fn, norm=norm) if node_dims is not None else None
        if pool == "add":
            self.pool = global_add_pool
        elif pool == "mean":
            self.pool = global_mean_pool
        elif pool == "max":
            self.pool = global_max_pool
        else:
            raise ValueError(f"Unknown pooling method : {pool}")
        self.embedding_proj = MLP(embedding_dims, activation=act_fn, last_activation=False, norm=norm)

    def forward(self, z, batch=None):
        z_proj = self.node_proj(z) if self.node_proj is not None else z
        e = self.pool(z_proj,batch).squeeze()
        e_proj = self.embedding_proj(e)
        return e_proj

class VGAE_polygones(VGAE):
    """ VGAE adapt to creat graphs embeddings
    
    :param encoder_base_dims: list of ints
        dimentions of the Encoder base_gcn
    :param encoder_mean_dims: list of ints
        dimentions of the Encoder mean_gcn and logstd_gcn
        the out_dim of Encoder base_gcn is automatically added in front
    
    :param pool: type of pooling in : 'add', 'mean', 'max'
    :param pool_node_dims: int or list of ints (optional)
        dimentions of the Pooler node_projection_MLP layers 
        the out_dim of encoder is automatically added in front
        None to deactivate projection
    :param pool_embedding_dims: int or list of ints
        dimentions of the Pooler embedding_projection_MLP layers 
        the out_dim of node_projection_MLP (replace by encoder out_dim if no node_projection_MLP) is automatically added in front
        last int is the embedding_size
    
    :param decoder_node_dims: int or list of ints (optional)
        dimentions of both Decoder node_projection_MLPs layers 
        the out_dim of Encoder is automatically added in front and the embedding_size is automatically added at the end
        None to deactivate projection; 0 to use one layer
    :param decoder_feat_dims: int or list of ints
        dimentions of the Decoder feat_projection_MLP layers 
        the 2*embedding_size is automatically added in front and the in_dim of Encoder is automatically added at the end
        0 to use one layer
    
    :param act_fn: activation function: torch.nn.Module
    :param norm: norm module to use in all MLPs
    :param X_loss: loss module to compute feature error
    :param agg: aggregation in EdgeConv : 'add', 'mean', 'max'
    :param k: number of neigbors for DynamicEdgeConv
    """
    def __init__(self, conv_layer_type='gcnconv', encoder_base_dims=[8,8], encoder_mean_dims=8, pool="add", pool_node_dims=8, pool_embedding_dims=8, decoder_node_dims=8, decoder_feat_dims=0, 
        act_fn=nn.LeakyReLU(), norm=GraphNorm, X_loss=nn.MSELoss(reduction="mean"),agg='add',k=20):
        # Encoder checks
        if not isinstance(encoder_mean_dims,list):
            encoder_mean_dims = [encoder_mean_dims]
        encoder_mean_dims = [encoder_base_dims[-1]] + encoder_mean_dims
        
        # Pooler checks
        assert pool_embedding_dims is not None, "at least one int is needed for embedding_size"
        if not isinstance(pool_embedding_dims,list):
            pool_embedding_dims = [pool_embedding_dims]
        if pool_node_dims is not None:
            if not isinstance(pool_node_dims,list):
                pool_node_dims = [pool_node_dims]
            pool_node_dims = [encoder_mean_dims[-1]]+pool_node_dims
            pool_embedding_dims = [pool_node_dims[-1]] + pool_embedding_dims
        else:
            pool_embedding_dims = [encoder_mean_dims[-1]] + pool_embedding_dims
        
        # Decoder checks
        self.embedding_size = pool_embedding_dims[-1]
        if decoder_node_dims is not None:
            if decoder_node_dims == 0:
                decoder_node_dims = [encoder_mean_dims[-1],self.embedding_size]
            else:
                if not isinstance(decoder_node_dims,list):
                    decoder_node_dims = [decoder_node_dims]
                decoder_node_dims = [encoder_mean_dims[-1]] + decoder_node_dims + [self.embedding_size]
        if decoder_feat_dims == 0:
            decoder_feat_dims = [self.embedding_size*2,encoder_base_dims[0]]
        else:
            if not isinstance(decoder_feat_dims,list):
                decoder_feat_dims = [decoder_feat_dims]
            decoder_feat_dims = [self.embedding_size*2] + decoder_feat_dims + [encoder_base_dims[0]]

        # model creation
        super().__init__(Encoder(conv_layer_type, encoder_base_dims,encoder_mean_dims, act_fn=act_fn, norm=norm, agg=agg, k=k),
            Decoder(feat_dims=decoder_feat_dims, node_dims=decoder_node_dims, act_fn=act_fn, norm=norm))
        self.pool = Pooler(embedding_dims=pool_embedding_dims, node_dims=pool_node_dims, pool=pool, act_fn=act_fn, norm=norm)
        self.X_loss = X_loss

    def forward(self, X, edge_index, batch=None, edge_weight=None):
        """ save encoded values and return embedding """
        self.batch = batch
        self.Z,self.embedding = self.encode(X, edge_index, edge_weight, batch)
        return self.embedding

    def encode(self, X, edge_index, edge_weight=None, batch=None):
        z = super().encode(X, edge_index, edge_weight)
        embedding = self.pool(z, batch)
        return z,embedding

    def recon_loss(self, pos_edge_index, z=None, neg_edge_index=None):
        """ compute loss from given z or saved Z """
        z = z if z is not None else self.Z
        if self.neg_edge_index is None:
            assert self.batch is None or torch.all(self.batch == self.batch[0]), "neg_edge_index must be given if multiple are given as input" 
        return super().recon_loss(z, pos_edge_index, neg_edge_index)

    def feat_loss(self, X, z=None, embedding=None):
        """ Compute feature reconstruction loss """
        z = z if z is not None else self.Z
        embedding = embedding if embedding is not None else self.embedding
        X_pred = self.decoder.forward_embbeding(z, embedding)
        return self.X_loss(X_pred, X)

