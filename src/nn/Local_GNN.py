"""
from git : https://github.com/lukasknobel/ShapeGNN
"""
import torch
from torch import nn as nn

from src.nn.fusion import CatFusion 
from src.nn.graph_conv import *

from src.data import NAG,Data

from torch_scatter import scatter_mul
 

__all__ = ['Shape_Interface_Encoders','Simple_Encoder']

""" module encoding polychain """

class Shape_Interface_Encoders(nn.Module):
    """
    create all the encoders needed for SPT
    """
    def __init__(self, List_Param_Shape_Encoders, List_Param_Interface_Encoders, share_level, share_type, hyper_share_level, hyper_share_type, nb_levels, nano, use_shape, use_interface, use_local_image_feature):
        """
        # hyperparameters Lists
        List_Param_Shape_Encoders : list of dictionary
            list of parameters of Shape Encoders
        List_Param_Interface_Encoders : list of dictionary
            list of parameters of Interface Encoders
            Should be empty if sharing parameters or hyperparameters between types

        # parameters sharing
		share_level: bool
			whether to share weights between levels
		share_type: bool
			whether to share weights between shape and interface encoders
		# hyperparameters sharing
		hyper_share_level: bool
			whether to use the same hyperparameters between levels
                relevant only if share_level = False
		hyper_share_type: bool
			whether to use the same hyperparameters between shape and interface encoders
                relevant only if share_level = False

            Sharing type need both types (see use_* below)
        
        # module hyperparameters
		nb_levels: int
			number of down stage
        nano: bool
            is SPT in nano mode (if True can't use local features)
        use_shape: bool
            whether to use shape encoding
        use_interface: bool
            whether to use interface encoding
            if the two last are False disable all in the class, and forward calls are forbiden
        """
        super().__init__()
        self.use_shape = use_shape
        self.use_interface = use_interface

        if use_shape or use_interface:
            self.share_level = share_level
            self.share_type = share_type
            self.hyper_share_level = hyper_share_level
            self.hyper_share_type = hyper_share_type
            self.nb_levels = nb_levels
            self.nano = nano
            self.use_local_image_feature = use_local_image_feature
            assert not (nano and use_local_image_feature), "no local image feature in nano mode"
            assert not ((not use_shape or not use_interface) and (share_type or hyper_share_type)), "can't share if not all encoders types are used"
            assert not ((share_type or hyper_share_type) and not len(List_Param_Interface_Encoders) == 0), "no Param Interface Encoders needed when sharing" # check for non contradictory hyperparameters
        
            self.feature_fusion = CatFusion()

            if use_shape:
                assert max((nb_levels * (not share_level) * (not hyper_share_level)),1) == len(List_Param_Shape_Encoders), \
                    "number of Graph Shape Encoder incorrect"

                List_Shape_Encoders = []
                for e in range(max((nb_levels * (not share_level)),1)):
                    List_Shape_Encoders.append(GraphEncoder(**List_Param_Shape_Encoders[e*(not hyper_share_level)]))
                self.List_Shape_Encoders = nn.ModuleList(List_Shape_Encoders)

            if use_interface:
                if share_type:
                    self.List_Interface_Encoders = self.List_Shape_Encoders
                else:
                    if hyper_share_type:
                        List_Param_Interface_Encoders = List_Param_Shape_Encoders
                    else:
                        assert max((nb_levels * (not share_level) * (not hyper_share_level)),1) == len(List_Param_Interface_Encoders), \
                            "number of Graph Interface Encoder incorrect"
                    
                    List_Interface_Encoders = []
                    for e in range(max((nb_levels * (not share_level)),1)):
                        List_Interface_Encoders.append(GraphEncoder(**List_Param_Interface_Encoders[e * (not hyper_share_level)]))
            
                    self.List_Interface_Encoders = nn.ModuleList(List_Interface_Encoders)
                
    def forward(self,nag,x=None):
        """
        :param nag: NAG object
            containing needed shape and interface polygonal chain at each level
        :param x: tensor
            convolution result (used with use_local_image_feature)
        """
        
        assert self.use_shape or self.use_interface, "the shape and interface encoding are disabled"
        assert isinstance(nag,NAG), "can only compute shape encoding for a NAG with computed partition"
        assert nag.num_levels-(not self.nano) == self.nb_levels, "incompatible number of levels"
        assert not self.use_shape or all([hasattr(nag[i+(not self.nano)],"shapes") for i in range(self.nb_levels)]), "missing shape information for shape encoding"
        assert not self.use_interface or all([hasattr(nag[i+(not self.nano)],"interfaces") for i in range(self.nb_levels)]), "missing shape information for interface encoding"
        
        if self.use_local_image_feature:
            img_size = getattr(nag[0],'img_size',None)
            assert img_size is not None
            if self.use_shape:
                nb_batch = torch.max(nag[1-self.nano].shapes.batch)+1
            else:
                nb_batch = torch.max(nag[1-self.nano].interfaces.batch)+1
            shift_batch = torch.cumsum(torch.cat((torch.tensor([0],device=x.device),scatter_mul(img_size,torch.repeat_interleave(torch.arange(nb_batch,device=x.device),2)))),0)
        
        for l in range(self.nb_levels):
            
            data = nag[l+(not self.nano)]

            if self.use_shape:
                if self.use_local_image_feature:
                    features = torch.cat(
                        (data.shapes.pos,x[data.shapes.round_pos[:,1]*img_size[data.shapes.batch*2+1]+data.shapes.round_pos[:,0]+shift_batch[data.shapes.batch]]),
                        dim=1)
                else:
                    features = data.shapes.pos
                
                data.x = self.feature_fusion(self.List_Shape_Encoders[l * (not self.share_level)](features,data.shapes.edge_index,data.shapes.batch_node),data.x)
                

            if self.use_interface:
                if self.use_local_image_feature:
                    features = x[data.interfaces.round_pos[:,1]*img_size[data.interfaces.batch*2+1]+data.interfaces.round_pos[:,0]+shift_batch[data.interfaces.batch]]
                    features[data.interfaces.null_interfaces] *= 0 # no image features for edges without interfaces
                    features = torch.cat((data.interfaces.pos,features),dim=1)
                else:
                    features = data.interfaces.pos
                
                # duplique the result for both direction and add null embeddings for self conections
                embeddings = self.List_Interface_Encoders[l*(not self.share_level)](features,data.interfaces.edge_index,data.interfaces.batch_node)
                data.edge_attr = self.feature_fusion(torch.cat((torch.cat((embeddings,embeddings),dim=0),torch.zeros([data.num_nodes,embeddings.shape[1]],device=embeddings.device)),dim=0),data.edge_attr)
            
### addon only for debuging ############################################################################################

class Simple_Encoder(nn.Module):
    """ 
        create one Graph_Encoder to test it
    """
    def __init__(self, **kwargs):
        """
        :param num_features: number of features for each node
        :param latent_dim: size of embeding space
        :param hidden_dim: size of hidden layers
            if 0 replace by max(latent_dim,pooling_dim)
        :param pooling_dim: size of pooling latent space (querry, key and value dim = pooling_dim)
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 'edgeconv','dynamicedgeconv','ginconv','gcnconv','gatconv','sageconv','genconv','transformerconv','gatv2conv'
        :param num_linear_layers_mult: number of linear layer in all mlp
        :param num_res_blocks: number of res_block (cf. BuildingBlocks)
        :param residual_type: type of residual connection : 'add', 'cat', 'no'
        :param pool: aggregation for pooling (graph to embeding): 'add', 'mean', 'max', 'att'
            'att' for attention mecanism cf. BuildingBlocks
        """
        
        super().__init__()
        self.Encoder = GraphEncoder(**kwargs)
    
    def forward(self,input):
        return self.Encoder(input[0],input[1],input[2])
