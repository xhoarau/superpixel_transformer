import torch
from torch import nn as nn
from torch_geometric import nn as geom_nn
from torch_sparse import SparseTensor

from src.nn import MLP
from src.nn.fusion import CatFusion 

__all__ = ['ConvBlock','ResBlock','BaseGNN','GraphEncoder','GATConv_custom','ResGatedGraphConv_custom']

""" Graph convolutions to use as sub_module for Stages or in shape encoders """

class ConvBlock(nn.Module):
    """
    Implement multiple graph convolution
    """
    def __init__(self, conv_layer_type='gcnconv', nodes=[8,8], act_fn=nn.LeakyReLU(), last_activation=True, norm=geom_nn.GraphNorm, agg='add', k=20):
        """
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 
            'edgeconv', 'dynamicedgeconv', 'ginconv', 'gcnconv', 'gatconv', 'sageconv', 'genconv', 'transformerconv', 'gatv2conv' 
        :param agg: aggregation in EdgeConv : 'add', 'mean', 'max'
        :param act_fn: activation function: torch.nn.Module
        :param nodes: input and output size of each mlp layer
        :param k: number of neigbors for DynamicEdgeConv 
        """
        super().__init__()
        self.conv_layer_type = conv_layer_type
        self.mlp_block = None
        self.act_fn = act_fn if last_activation else None
        self.k = k
        self.input_dim = nodes[0]
        self.output_dim = nodes[-1]
        if conv_layer_type == 'edgeconv':
            mod_nodes = list(nodes)
            mod_nodes[0] *= 2
            mlp = MLP(mod_nodes, activation=act_fn, last_activation=False, norm=norm)
            conv_layer = geom_nn.EdgeConv(mlp, aggr=agg)
        elif conv_layer_type == 'dynamicedgeconv': 
            mod_nodes = list(nodes)
            mod_nodes[0] *= 2
            mlp = MLP(mod_nodes, activation=act_fn, last_activation=False, norm=norm)
            conv_layer = geom_nn.DynamicEdgeConv(nn=mlp, k=self.k, aggr=agg)
        elif conv_layer_type == 'ginconv':
            mlp = MLP(nodes, activation=act_fn, last_activation=False, norm=norm)
            conv_layer = geom_nn.GINConv(mlp, aggr=agg)
        else: # projection of each node with mlp + graph convolution
            if len(nodes) > 2:
                self.mlp_block = MLP(nodes[:-1], activation=act_fn, last_activation=False, norm=norm)
            if conv_layer_type == 'gcnconv':
                conv_layer = geom_nn.GCNConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'gatconv':
                conv_layer = geom_nn.GATConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'sageconv':
                conv_layer = geom_nn.SAGEConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'genconv':
                conv_layer = geom_nn.GENConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'transformerconv':
                conv_layer = geom_nn.TransformerConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'gatv2conv':
                conv_layer = geom_nn.GATv2Conv(nodes[-2], nodes[-1])
            else:
                raise ValueError(f'Unknown conv_layer_type {conv_layer_type}')
        self.conv_layer = conv_layer

    def forward(self, x, adj_t: SparseTensor, edge_attr=None):
        output = x
        if self.mlp_block is not None: # projection depend on conv_type and number of layer
            output = self.mlp_block(output)
        if edge_attr is not None:
            output = self.conv_layer(output, adj_t, edge_attr)
        else:
            output = self.conv_layer(output, adj_t)
        if self.act_fn is not None:
            output = self.act_fn(output)
        return output


class ResBlock(nn.Module):
    """
    Based on ConvBlock adding a residual connection
    """
    def __init__(self, conv_block: ConvBlock, residual_type='add',verbose = False):
        """
        :param conv_block: A ConvBlock
        :param residual_type: type of connection: 'add', 'mean', 'no'
        """
        super().__init__()
        self.block = conv_block
        self.res_projection = None
        self.residual_type = residual_type
        if self.residual_type == 'add':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.output_dim
            if self.block.input_dim != self.block.output_dim:
                if verbose:
                    print(f'Adding linear projection to residual connection to map {self.block.input_dim} to {self.block.output_dim} dimensions')
                self.res_projection = nn.Linear(self.block.input_dim, self.block.output_dim)
        elif self.residual_type == 'cat':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.input_dim+self.block.output_dim
        elif self.residual_type == 'no':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.output_dim
        else:
            raise ValueError(f'Unknown residual_type: "{residual_type}"')

    def forward(self, x, adj_t: SparseTensor, edge_attr=None):
        output = self.block(x, adj_t, edge_attr)

        if self.residual_type == 'add':
            if self.res_projection is not None:
                output = output + self.res_projection(x)
            else:
                output = output + x
        elif self.residual_type == 'cat':
            output = torch.concat((x, output), dim=-1)
        elif self.residual_type == 'no':
            pass

        return output

### from GNNModels ##########################################################################################################################################

class BaseGNN(nn.Module):
    """
    Superclass of all other GNN classes
    """
    def __init__(self, linear_dropout=0.0, act_fn=nn.LeakyReLU(), norm=geom_nn.GraphNorm, agg='add', pool='add', pooling_dim=-1, residual_type='add', num_linear_layers=1, verbose = False, **kwargs):
        """
        :param linear_dropout: probability of zeroing in dropout layers
        :param act_fn: activation function: torch.nn.Module
        :param agg: aggregation EdgeConv: 'add', 'mean', 'max'
        :param pool: aggregation for pooling (graph to embeding): 'add', 'mean', 'max', 'att'
        :param residual_type: residual connection type : 'add', 'cat', 'no'
        :param pooling_dim: number of feature for each node at polling input
        :param num_linear_layers: number of linear layer in all mlp
        """
        super().__init__()
        if verbose:
            print(f'Ignoring the following keyword arguments when creating {self.__class__.__name__} model: {kwargs}')
        if linear_dropout > 0.0:
            self.linear_dropout = nn.Dropout(linear_dropout)
        else:
            self.linear_dropout = None
        self.act_fn = act_fn
        self.norm = norm
        self.agg = agg
        self.pooling_dim = pooling_dim
        self.residual_type = residual_type
        self.num_linear_layers = num_linear_layers
        self.verbose = verbose

        # identify pooling method
        pool = pool.lower()
        self.pool_name = pool
        if self.pool_name == 'add':
            self.pool = geom_nn.global_add_pool
        elif self.pool_name == 'max':
            self.pool = geom_nn.global_max_pool
        elif pool == 'mean':
            self.pool = geom_nn.global_mean_pool
        elif self._set_att_pooling():
            pass
        else:
            raise ValueError(f'Unknown pooling "{self.pool_name}"')

    def _set_att_pooling(self):
        """
        Create an attention pooling layer and save it in an attribute
        """
        if self.pool_name == 'att':
            if self.pooling_dim == -1:
                raise ValueError(f'pre_pooling_dim has to be specified to use "{self.pool_name}"-pooling')
            self.pool = geom_nn.GlobalAttention(
                gate_nn=nn.Sequential(MLP([self.pooling_dim for _ in range(self.num_linear_layers+1)],
                    activation=self.act_fn, last_activation=False, norm=self.norm, drop=self.linear_dropout),nn.Linear(self.pooling_dim,1)),
                nn=MLP([self.pooling_dim for _ in range(self.num_linear_layers+1)], 
                    activation=self.act_fn, last_activation=False, norm=self.norm, drop=self.linear_dropout)
            )
            return True
        return False

    def get_res_block(self, conv_layer_type, conv_lin_layers, residual_type=None, k=20):
        """
        Create a residual block
        """
        conv_block = self.get_conv_block(conv_layer_type, conv_lin_layers, k=k)
        if residual_type is None:
            residual_type = self.residual_type
        return ResBlock(conv_block, residual_type, self.verbose)

    def get_conv_block(self, conv_layer_type, conv_lin_layers, k=20):
        """
        Create a graph convolution block
        """
        return ConvBlock(conv_layer_type, conv_lin_layers, act_fn=self.act_fn, norm=self.norm, agg=self.agg, k=k)

### from GraphEncoder #######################################################################################################################################

class GraphEncoder(BaseGNN):
    """
    The local GNN used to encode superpixel shapes
    """
    def __init__(self, num_features, latent_dim,  hidden_dim=0, num_res_blocks=1, conv_layer_type='gcnconv',
        act_fn=nn.LeakyReLU(), norm=geom_nn.GraphNorm, pool='add', pooling_dim=4, residual_type='no', num_linear_layers=1, **kwargs):
        """
        :param num_features: number of features for each node
        :param latent_dim: size of embeding space
        :param hidden_dim: size of hidden layers
            if 0 replace by max(latent_dim,pooling_dim)
        :param pooling_dim: size of pooling latent space (querry, key and value dim = pooling_dim)
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 'edgeconv','dynamicedgeconv','ginconv','gcnconv','gatconv','sageconv','genconv','transformerconv','gatv2conv'
        :param num_linear_layers: number of linear layer in all mlp
        :param num_res_blocks: number of res_block (cf. BuildingBlocks)
        :param residual_type: type of residual connection : 'add', 'cat', 'no'
        :param pool: aggregation for pooling (graph to embeding): 'add', 'mean', 'max', 'att'
            'att' for attention mecanism cf. BuildingBlocks
        """
        self.latent_dim = latent_dim
        self.conv_layer_type = conv_layer_type
        if hidden_dim == 0:
            self.hidden_dim = max(self.latent_dim,pooling_dim)
        else:
            self.hidden_dim = hidden_dim

        super().__init__(
            act_fn=act_fn,
            norm=norm,
            pool=pool,
            pooling_dim=pooling_dim,
            residual_type=residual_type, 
            num_linear_layers=num_linear_layers, 
            **kwargs)

        res_block_list = []
        input_dim = num_features
        output_dim = hidden_dim
        for i in range(num_res_blocks):
            res_llayers = [input_dim] + [self.hidden_dim for _ in range(num_linear_layers-1)] + [output_dim]
            res_block = self.get_res_block(conv_layer_type, res_llayers)
            res_block_list.append(res_block)
            input_dim = self.hidden_dim
            if i == num_res_blocks-2: # only for last res block
                output_dim = pooling_dim
        self.res_blocks = nn.ModuleList(res_block_list)
        
        mlp_llayers = [pooling_dim] + [hidden_dim for _ in range(num_linear_layers-1)] + [self.latent_dim]
        self.mlp = MLP(mlp_llayers, activation=self.act_fn, last_activation=False, norm=norm)

    def forward(self, x, adj_t: SparseTensor, batch):
        for res_block in self.res_blocks:
            x = res_block(x=x, adj_t=adj_t)
        x = self.pool(x, batch)
        x = self.mlp(x)
        return x

########## little addon to classic Conv ##########

class GATConv_custom(geom_nn.conv.GATConv):
    @property
    def out_dim(self):
        if self.concat:
            return self.heads*self.out_channels
        else:
            return self.out_channels


class ResGatedGraphConv_custom(geom_nn.conv.ResGatedGraphConv):
    @property
    def out_dim(self):
        return self.out_channels
