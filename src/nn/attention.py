import torch
from torch import nn
from torch_scatter import scatter_sum
from torch_geometric.utils import softmax
from src.utils.nn import build_qk_scale_func
from src.nn import MLP, LayerNorm
from src.nn.fusion import CatFusion, fusion_factory
# from src.metrics.meters import *

__all__ = ['SelfAttentionBlock','Topological_SelfAttentionBlock']


class SelfAttentionBlock(nn.Module):
    """SelfAttentionBlock is intended to be used in a residual fashion
    (or not) in TransformerBlock.

    Inspired by: https://github.com/microsoft/Swin-Transformer

    :param dim:Int
        total dimention of nodes for attention mecanism
    :param num_heads: Int
        number of parallel attention mecanism (must divied dim)
    :param in_dim: Int
        dimention of nodes at input
    :param out_dim: Int
        dimention of nodes at output
    :param qkv_bias: Boolean
        use bias to generate q, k and v
    :param qk_dim: Int
        dimention of q and k (v dimentions = dim//num_heads)
    :param qk_scale: funcion,str or None 
        function to scale q,k product (classic: sqrt(qk_dim))
    :param attn_drop: None, float in [0,1]
        add dropout layer after attention mecanism
    :param drop: None, float in [0,1]
        add dropout layer ant the end
    :param in_rpe_dim:Int
        dimention of edge_attr
    :param k_rpe: nn module
        modèle to compute k modulation with edge_attr
    :param q_rpe: nn module
        modèle to compute q modulation with edge_attr
    :param v_rpe: nn module
        modèle to compute v modulation with edge_attr
    :param k_delta_rpe: nn module
        modèle to compute k modulation with x_source - x_target
    :param q_delta_rpe: nn module
        modèle to compute q modulation with x_source - x_target
    :param qk_share_rpe: Boolean
        share weight between q and k rpe
    :param q_on_minus_rpe: Boolean
        compute q modulation with -edge_attr
    :param heads_share_rpe: Boolean 
        all heads share qkv_rpe
    :param rpe_fusion: str (see fusion_factory)
        fusion method of node and edge features
    :param mlp_rpe: int
        number of layer for post rpe fusion mlps
        0 to deactivate mlps
    :param norm: Norm (nn.module)
        norm layer to use in mlps
    """

    def __init__(
            self,
            dim,
            num_heads=1,
            in_dim=None,
            out_dim=None,
            qkv_bias=True,
            qk_dim=8,
            qk_scale=None,
            attn_drop=None,
            drop=None,
            in_rpe_dim=18,
            k_rpe=False,
            q_rpe=False,
            v_rpe=False,
            k_delta_rpe=False,
            q_delta_rpe=False,
            qk_share_rpe=False,
            q_on_minus_rpe=False,
            heads_share_rpe=False,
            rpe_fusion='add',
            mlp_rpe=0,
            norm=LayerNorm):
        super().__init__()

        assert dim % num_heads == 0, f"dim must be a multiple of num_heads"

        self.dim = dim
        self.num_heads = num_heads
        self.qk_dim = qk_dim
        self.qk_scale = build_qk_scale_func(dim, num_heads, qk_scale)
        self.heads_share_rpe = heads_share_rpe
        self.mlp_rpe = mlp_rpe
        self.rpe_fusion = fusion_factory(rpe_fusion)
        qkv_size = qk_dim * 2 * num_heads + dim
        if isinstance(self.rpe_fusion,CatFusion):
            qkv_size = qkv_size//2+qkv_size%2
        self.qkv = nn.Linear(dim, qkv_size, bias=qkv_bias)

        # Build the RPE encoders, with the option of sharing weights
        # across all heads
        qk_rpe_dim = qk_dim if heads_share_rpe else qk_dim * num_heads 
        v_rpe_dim = dim // num_heads if heads_share_rpe else dim
        if isinstance(self.rpe_fusion,CatFusion):
            qk_rpe_dim = qk_rpe_dim//2
            v_rpe_dim = v_rpe_dim//2

        if not isinstance(k_rpe, bool):
            self.k_rpe = k_rpe
        else:
            self.k_rpe = nn.Linear(in_rpe_dim, qk_rpe_dim) if \
                k_rpe else None

        if self.k_rpe is not None and mlp_rpe>0:
            self.k_rpe_mlp = MLP([qk_dim]*mlp_rpe,last_activation=False,norm=norm,num_mlp=num_heads)

        if not isinstance(q_rpe, bool):
            self.q_rpe = q_rpe
        else:
            self.q_rpe = nn.Linear(in_rpe_dim, qk_rpe_dim) if \
                q_rpe and not (k_rpe and qk_share_rpe) else None
        
        if self.q_rpe is not None and mlp_rpe>0:
            self.q_rpe_mlp = MLP([qk_dim]*mlp_rpe,last_activation=False,norm=norm,num_mlp=num_heads)

        if not isinstance(k_delta_rpe, bool):
            self.k_delta_rpe = k_delta_rpe
        else:
            self.k_delta_rpe = nn.Linear(dim, qk_rpe_dim) if  \
                k_delta_rpe else None
        
        if self.k_delta_rpe is not None and mlp_rpe>0:
            self.k_delta_rpe_mlp = MLP([qk_dim]*mlp_rpe,last_activation=False,norm=norm,num_mlp=num_heads)

        if not isinstance(q_delta_rpe, bool):
            self.q_delta_rpe = q_delta_rpe
        else:
            self.q_delta_rpe = nn.Linear(dim, qk_rpe_dim) if \
                q_delta_rpe and not (k_delta_rpe and qk_share_rpe) \
                else None
        
        if self.q_delta_rpe is not None and mlp_rpe>0:
            self.q_delta_rpe_mlp = MLP([qk_dim]*mlp_rpe,last_activation=False,norm=norm,num_mlp=num_heads)

        self.qk_share_rpe = qk_share_rpe
        self.q_on_minus_rpe = q_on_minus_rpe

        if not isinstance(v_rpe, bool):
            self.v_rpe = v_rpe
        else:
            self.v_rpe = nn.Linear(in_rpe_dim, v_rpe_dim) if v_rpe else None

        if self.v_rpe is not None and mlp_rpe>0: 
            self.v_rpe_mlp = MLP([dim//num_heads]*mlp_rpe,last_activation=False,norm=norm,num_mlp=num_heads)

        self.in_proj = nn.Linear(in_dim, dim) if in_dim is not None else None
        self.out_proj = nn.Linear(dim, out_dim) if out_dim is not None else None

        self.attn_drop = nn.Dropout(attn_drop) \
            if attn_drop is not None and attn_drop > 0 else None
        self.out_drop = nn.Dropout(drop) \
            if drop is not None and drop > 0 else None
    
    def forward(self, x, edge_index, edge_attr=None, norm_index=None):
        """
        :param x: Tensor of shape (N, Cx)
            Node features
        :param edge_index: LongTensor of shape (2, E)
            Source and target indices for the edges of the attention
            graph. Source indicates the querying element, while Target
            indicates the key elements
        :param edge_attr: FloatTensor or shape (E, Ce)
            Edge attributes for relative pose encoding
        :return:
        """
        N = x.shape[0]
        E = edge_index.shape[1]
        H = self.num_heads
        D = self.qk_dim if not isinstance(self.rpe_fusion,CatFusion) else self.qk_dim//2+self.qk_dim%2
        DH = D * H

        # Optional linear projection of features
        if self.in_proj is not None:
            x = self.in_proj(x)

        # Compute queries, keys and values
        # qkv = self.qkv(x).view(N, 3, self.num_heads, self.dim // self.num_heads)
        qkv = self.qkv(x)

        # Separate queries, keys, values
        q = qkv[:, :DH].view(N, H, D)        # [N, H, D]
        k = qkv[:, DH:2 * DH].view(N, H, D)  # [N, H, D]
        v = qkv[:, 2 * DH:].view(N, H, -1)   # [N, H, C // H]

        # Expand queries, keys and values to edges
        s = edge_index[0]  # [E]
        t = edge_index[1]  # [E]
        q = q[s]  # [E, H, D]
        k = k[t]  # [E, H, D]
        v = v[t]  # [E, H, C // H]
        
        norm_index_q = norm_index[s] if norm_index is not None else None
        norm_index_kv = norm_index[t] if norm_index is not None else None

        # Apply scaling on the queries.
        q = q * self.qk_scale(s)

        if self.k_rpe is not None and edge_attr is not None:
            rpe = self.k_rpe(edge_attr)

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)
            
            k = self.rpe_fusion(k,rpe.view(E,H,-1),dim=2)
            
            if self.mlp_rpe>0:
                k = self.k_rpe_mlp(k,batch=norm_index_kv)

        if self.q_rpe is not None and edge_attr is not None:
            if self.q_on_minus_rpe:
                rpe = self.q_rpe(-edge_attr)
            else:
                rpe = self.q_rpe(edge_attr)

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            q = self.rpe_fusion(q,rpe.view(E,H,-1),dim=2)

            if self.mlp_rpe>0:
                q = self.q_rpe_mlp(q,batch=norm_index_q)
        
        elif self.k_rpe is not None and self.qk_share_rpe and edge_attr is not None:
            if self.q_on_minus_rpe:
                rpe = self.k_rpe(-edge_attr)
            else:
                rpe = self.k_rpe(edge_attr)

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            q = self.rpe_fusion(q,rpe.view(E,H,-1),dim=2)

            if self.mlp_rpe>0:
                q = self.k_rpe_mlp(q,batch=norm_index_q)

        if self.k_delta_rpe is not None:
            rpe = self.k_delta_rpe(x[edge_index[1]] - x[edge_index[0]])

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            k = self.rpe_fusion(k,rpe.view(E,H,-1),dim=2)
            
            if self.mlp_rpe>0:
                k = self.k_delta_rpe_mlp(k,batch=norm_index_kv)

        if self.q_delta_rpe is not None:
            if self.q_on_minus_rpe:
                rpe = self.q_delta_rpe(x[edge_index[0]] - x[edge_index[1]])
            else:
                rpe = self.q_delta_rpe(x[edge_index[1]] - x[edge_index[0]])

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            q = self.rpe_fusion(q,rpe.view(E,H,-1),dim=2)
            
            if self.mlp_rpe>0:
                q = self.q_delta_rpe_mlp(q,batch=norm_index_q)

        elif self.k_delta_rpe is not None and self.qk_share_rpe and edge_attr is not None:
            if self.q_on_minus_rpe:
                rpe = self.k_delta_rpe(x[edge_index[0]] - x[edge_index[1]])
            else:
                rpe = self.k_delta_rpe(x[edge_index[1]] - x[edge_index[0]])

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            q = self.rpe_fusion(q,rpe.view(E,H,-1),dim=2)

            if self.mlp_rpe>0:
                q = self.k_delta_rpe_mlp(q,batch=norm_index_q)

        if self.v_rpe is not None and edge_attr is not None:
            rpe = self.v_rpe(edge_attr)

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)

            v = self.rpe_fusion(v,rpe.view(E,H,-1),dim=2)

            if self.mlp_rpe>0:
                v = self.v_rpe_mlp(v,batch=norm_index_kv)

        # Compute compatibility scores from the query-key products
        compat = torch.einsum('ehd, ehd -> eh', q, k)  # [E, H]

        # Compute the attention scores with scaled softmax
        attn = softmax(compat, index=s, dim=0, num_nodes=N)  # [E, H]

        # Optional attention dropout
        if self.attn_drop is not None:
            attn = self.attn_drop(attn)

        # Apply the attention on the values
        x = (v * attn.unsqueeze(-1)).reshape(E, self.dim)  # [E, C]
        x = scatter_sum(x, s, dim=0, dim_size=N)  # [N, C]

        # Optional linear projection of features
        if self.out_proj is not None:
            x = self.out_proj(x)  # [N, out_dim]

        # Optional dropout on projection of features
        if self.out_drop is not None:
            x = self.out_drop(x)  # [N, C or out_dim]

        return x

    def extra_repr(self) -> str:
        return f'dim={self.dim}, num_heads={self.num_heads}'


class Topological_SelfAttentionBlock(nn.Module):
    """works same as selfAtentionBlock, but change q and k values to compute them only on the local topology of the graph
    
    SelfAttentionBlock is intended to be used in a residual fashion
    (or not) in TransformerBlock.

    Inspired by: https://github.com/microsoft/Swin-Transformer

    :param dim: Int
        total dimention of nodes for attention mecanism
    :param num_heads: Int
        number of parallel attention mecanism (must divied dim)
    :param in_dim: Int
        dimention of nodes at input
    :param out_dim: Int
        dimention of nodes at output
    :param qkv_bias: Boolean
        use bias to generate q, k and v
    :param qk_dim: Int
        dimention of q and k (v dimentions = dim//num_heads)
    :param qk_scale: funcion,str or None 
        function to scale q,k product (classic: sqrt(qk_dim))
    :param attn_drop: None, float in [0,1]
        add dropout layer after attention mecanism
    :param drop: None, float in [0,1]
        add dropout layer ant the end
    :param in_rpe_dim: Int
        dimention of edge_attr
    :param v_rpe: nn module
        modèle to compute v modulation with edge_attr
    :param heads_share_rpe: Boolean
        all heads share v_rpe
    """

    def __init__(
            self,
            dim,
            num_heads=1,
            in_dim=None,
            out_dim=None,
            qkv_bias=True,
            k_in_dim=3,
            qk_dim=8,
            qk_scale=None,
            attn_drop=None,
            drop=None,
            in_rpe_dim=18,
            v_rpe=False,
            heads_share_rpe=False,
            mlp_rpe=False,
            norm=LayerNorm):
        super().__init__()

        assert dim % num_heads == 0, f"dim must be a multiple of num_heads"

        self.dim = dim
        self.num_heads = num_heads
        self.qk_dim = qk_dim
        self.qk_scale = build_qk_scale_func(dim, num_heads, qk_scale)
        self.heads_share_rpe = heads_share_rpe
        self.mlp_rpe = mlp_rpe
        self.v = nn.Linear(dim, dim, bias = qkv_bias) # classic module (after removing q and k)
        self.q = nn.Parameter(torch.rand((num_heads,qk_dim))) # learn one q vector / head ## [H, D]
        self.k = nn.Linear(k_in_dim, qk_dim*num_heads, bias = qkv_bias) # compute k for each edges with distance and direction (in_dim in 2D space = 3) 

        # Build the RPE encoders, with the option of sharing weights
        # across all heads
        v_rpe_dim = dim // num_heads if heads_share_rpe else dim
        
        if not isinstance(v_rpe, bool):
            self.v_rpe = v_rpe
        else:
            self.v_rpe = nn.Linear(in_rpe_dim, v_rpe_dim) if v_rpe else None

        if self.v_rpe is not None and self.mlp_rpe:
            self.v_rpe_fuse_mlp = MLP([2*dim,dim,dim],last_activation=False,norm=norm)

        self.in_proj = nn.Linear(in_dim, dim) if in_dim is not None else None
        self.out_proj = nn.Linear(dim, out_dim) if out_dim is not None else None

        self.attn_drop = nn.Dropout(attn_drop) \
            if attn_drop is not None and attn_drop > 0 else None
        self.out_drop = nn.Dropout(drop) \
            if drop is not None and drop > 0 else None
    
    def forward(self, x, edge_index, edge_attr=None, topology=None, norm_index=None):
        """
        :param x: Tensor of shape (N, Cx)
            Node features
        :param edge_index: LongTensor of shape (2, E)
            Source and target indices for the edges of the attention
            graph. Source indicates the querying element, while Target
            indicates the key elements
        :param edge_attr: FloatTensor of shape (E, Ce)
            Edge attributes for relative pose encoding
        :param topology: FloatTensor of shape (E, 3)
            node directions (normalied vector) and distances
        :return:
        """
        N = x.shape[0]
        E = edge_index.shape[1]
        H = self.num_heads
        D = self.qk_dim
        DH = D * H

        # Optional linear projection of features
        if self.in_proj is not None:
            x = self.in_proj(x)

        # Compute keys and values
        v = self.v(x)           # [N, CH]
        k = self.k(topology)    # [E, DH]

        # reshape keys and values
        v = v.view(N, H, -1)    # [N, H, C // H]
        k = k.view(E, H, D)     # [E, H, D]
        
        # values to edges
        s = edge_index[0]  # [E]
        t = edge_index[1]  # [E]
        v = v[t]  # [E, H, C // H]

        norm_index_v = norm_index[t] if norm_index is not None else None

        # Apply scaling on the queries.
        k = k * self.qk_scale(s)
        
        if self.v_rpe is not None and edge_attr is not None:
            rpe = self.v_rpe(edge_attr)

            # Expand RPE to all heads if heads share the RPE encoder
            if self.heads_share_rpe:
                rpe = rpe.repeat(1, H)
            
            if self.mlp_rpe:
                v = self.v_rpe_fuse_mlp(torch.cat((v,rpe.view(E,H,-1)),dim=2),batch=norm_index_v)
            else:
                v = v + rpe.view(E, H, -1)

        # Compute compatibility scores from the query-key products
        compat = torch.einsum('ehd, ehd -> eh', self.q.repeat((E,1,1)), k)  # [E, H]

        # Compute the attention scores with scaled softmax
        attn = softmax(compat, index=s, dim=0, num_nodes=N)  # [1, H]

        # Optional attention dropout
        if self.attn_drop is not None:
            attn = self.attn_drop(attn)

        # Apply the attention on the values
        x = (v * attn.unsqueeze(-1)).view(E, self.dim)  # [E, C]
        x = scatter_sum(x, s, dim=0, dim_size=N)  # [N, C]

        # Optional linear projection of features
        if self.out_proj is not None:
            x = self.out_proj(x)  # [N, out_dim]

        # Optional dropout on projection of features
        if self.out_drop is not None:
            x = self.out_drop(x)  # [N, C or out_dim]

        return x

    def extra_repr(self) -> str:
        return f'dim={self.dim}, num_heads={self.num_heads}'
