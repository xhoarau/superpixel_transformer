# Adapted from score written by wkentaro
# https://github.com/wkentaro/pytorch-fcn/blob/master/torchfcn/utils.py

import torch
from multiprocessing import Value, RLock

__all__ = ["averageMeter","averageMeter_vect"]

class averageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = torch.tensor(0.)
        self.avg = torch.tensor(0.)
        self.sum = torch.tensor(0.)
        self.count = torch.tensor(0)
        self.var = torch.tensor(0.)
        self.std = torch.tensor(0.)
    
    def to(self,device):
        self.val = self.val.to(device)
        self.avg = self.avg.to(device)
        self.sum = self.sum.to(device)
        self.count = self.count.to(device)
        self.var = self.var.to(device)
        self.std = self.std.to(device)

    def update(self, val):
        if not torch.is_tensor(val):
            val = torch.tensor(val)
        if val.device != self.val.device: # meter should be used in only one device, then this should append 1 time
            self.to(val.device) 
        val = val.to(torch.float)
        n = val.numel()
        self.val = val
        self.sum += val.sum()
        try: # depends on torch version (1.* or 2.*)
            if self.count !=0:
                self.var = (((n-1)*torch.var(val,correction=0)+(self.count-1)*self.var)/(n+self.count-1))+((n*self.count*(torch.mean(val)-self.avg)**2)/((n+self.count)*(n+self.count-1)))
            else:
                self.var = torch.var(val,correction=0)
        except TypeError:
            if self.count !=0:
                self.var = (((n-1)*torch.var(val,unbiased=False)+(self.count-1)*self.var)/(n+self.count-1))+((n*self.count*(torch.mean(val)-self.avg)**2)/((n+self.count)*(n+self.count-1)))
            else:
                self.var = torch.var(val,unbiased=False)
        
        self.std = torch.sqrt(self.var)
        self.count += n
        self.avg = self.sum / self.count

class averageMeter_vect(object):
    """Computes and stores the average and current value"""

    def __init__(self,size):
        self.size = size
        self.reset()

    def reset(self):
        self.val = torch.zeros(self.size)
        self.avg = torch.zeros(self.size)
        self.sum = torch.zeros(self.size)
        self.count = torch.tensor(0)
        self.var = torch.zeros(self.size)
        self.std = torch.zeros(self.size)
    
    def to(self,device):
        self.val = self.val.to(device)
        self.avg = self.avg.to(device)
        self.sum = self.sum.to(device)
        self.count = self.count.to(device)
        self.var = self.var.to(device)
        self.std = self.std.to(device)

    def update(self, val, dim): # dimention to keep
        if not torch.is_tensor(val):
            val = torch.tensor(val)
        if val.device != self.val.device:
            self.to(val.device) 
        val = val.to(torch.float)
        n = int(val.numel()/val.shape[dim])
        val = val.transpose(0,dim).flatten(1)
        assert self.val.shape[0] == val.shape[0]
        self.val = val
        self.sum += val.sum(1)
        try: # depends on torch version (1.* or 2.*)
            if self.count !=0:
                self.var = (((n-1)*torch.var(val,dim=1,correction=0)+(self.count-1)*self.var)/(n+self.count-1))+((n*self.count*(torch.mean(val,dim=1)-self.avg)**2)/((n+self.count)*(n+self.count-1)))
            else:
                self.var = torch.var(val,dim=1,correction=0)
        except TypeError:
            if self.count !=0:
                self.var = (((n-1)*torch.var(val,dim=1,unbiased=False)+(self.count-1)*self.var)/(n+self.count-1))+((n*self.count*(torch.mean(val,dim=1)-self.avg)**2)/((n+self.count)*(n+self.count-1)))
            else:
                self.var = torch.var(val,dim=1,unbiased=False)
        
        self.std = torch.sqrt(self.var)
        self.count += n
        self.avg = self.sum / self.count

