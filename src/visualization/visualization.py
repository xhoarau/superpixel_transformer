import torch
import torch_geometric
import numpy as np
import os.path as osp
import plotly.graph_objects as go
from src.data import Data, NAG, Cluster
from src.utils import fast_randperm, to_trimmed, image_to_feature #, grid_to_graph
from torch_scatter import scatter_mean
from src.utils.color import *
import matplotlib.pyplot as plt
import networkx as nx
import itertools

import sys
import os
sys.path.append("src/dependencies/multilabel-potrace/python/bin")
sys.path.append("src/dependencies/grid_graph/python/bin")
from multilabel_potrace_svg import multilabel_potrace_svg
from grid_graph import grid_to_graph

from time import time,sleep
import svgutils

def figure_html(fig):
    # Save plotly figure to temp HTML
    fig.write_html(
        '/tmp/fig.html',
        config={'displayModeBar': False},
        include_plotlyjs='cdn',
        full_html=False)

    # Read the HTML
    with open("/tmp/fig.html", "r") as f:
        fig_html = f.read()

    # Center the figure div for cleaner display
    fig_html = fig_html.replace(
        'class="plotly-graph-div" style="',
        'class="plotly-graph-div" style="margin:0 auto;')

    return fig_html


def plt_figure(n_row, n_col, num = None, Final_version=False):
    """ parametrize the plt figure

    :param n_row: maximum number of subplot per column
    :param n_column: maximum number of subplot per row
    :param num: identifier of figure if reuse an existing figure 
        if None create a new figure, 
        if string also title of figure
    :Final_version: to set the precision on the figure
        to achieve lighter image uring experiment
    """
    # square of 5/5 inches for each subplot
    figsize = [5*n_col,5*n_row]

    if Final_version:
        dpi = 1000
    else:
        dpi = 200

    fig = plt.figure(num = num, figsize = figsize, dpi = dpi, layout = 'constrained')

    return fig

def new_subplot(coord, fig, title = None, axis= False):
    """ create a new subplot in fig at the given position

    :param coord: position of the subplot
    :param fig: figure in witch create the sub plot
    :param title: optional title of the subplot
    :param axis: keep or remove the axis visible
    """
    
    n_col,n_row = fig.get_size_inches()/5
    n_col = int(n_col)
    n_row = int(n_row)

    assert coord[0] <= n_row and coord[1] <= n_col, "coordinate outside of the figure"

    index = int((coord[0]-1)*n_col + coord[1])
    ax = fig.add_subplot(n_row, n_col, index, aspect='equal')
    if not title is None :
        ax.set(title=title)
    if not axis:
        ax.axis('off')
    
    return ax

def image_SuperPixel_visualize(img,super_index):
    """ create a big image to visualise super pixel
    compute the mean rgb for each super pixel and drow blacl lines to seperate them

    :param image: the origin image (rgb [3,w,h] or gray [1,w,h])
    :param super_index: index of super pixel for each pixel
    """
    img_size = torch.tensor(img.shape[1:],device=img.device)
    big_image_super_index = torch.nn.functional.interpolate(super_index.reshape(tuple(img_size)).unsqueeze(0).unsqueeze(0).float(),scale_factor=5).squeeze().long()
    big_super_index = big_image_super_index.flatten()

    edges = grid_to_graph(np.array(big_image_super_index.shape),connectivity=1, 
        compute_connectivities = False, graph_as_forward_star = False,row_major_index = True)
    edges = edges.astype(np.int64).T
    
    index_bord = edges[0,big_super_index[edges[0]]!=big_super_index[edges[1]]]
    
    big_image = scatter_mean(img.flatten(-2).permute(1,0).float(),super_index.unsqueeze(1),dim=0).permute(1,0)
    big_image = big_image[:,big_super_index]
    big_image[:,index_bord] = torch.tensor([0.,0.,0.],dtype=torch.float,device=img.device).unsqueeze(1)
    big_image = big_image.reshape([3,*big_image_super_index.shape])
    
    return big_image

def label_SuperPixel_visualize(img_size,y,pos,super_index):
    """ create a big image to visualise class of superpixel
    compute the majority label in each super pixel and draw black lines to seperate them

    :param y: the low level label [num_nodes,num_classes]
    :param super_index: index of super pixel for each pixel
    """
    img = torch.zeros(tuple(img_size),device=super_index.device)
    coord = (pos*(img_size.max()-1)).long()
    img[coord[:,1],coord[:,0]] = super_index.to(img.dtype)+1
    big_image_super_index = torch.nn.functional.interpolate(img.unsqueeze(0).unsqueeze(0),scale_factor=5).squeeze().long()
    big_super_index = big_image_super_index.flatten()
    
    edges = grid_to_graph(np.array(big_image_super_index.shape),connectivity = 1, 
        compute_connectivities = False, graph_as_forward_star = False,row_major_index = True)
    edges = edges.astype(np.int64).T
    
    index_bord = edges[0,big_super_index[edges[0]]!=big_super_index[edges[1]]]
    big_label = torch.cat((torch.tensor([y.shape[1]-1]),y.argmax(dim=1)))[big_super_index]
    big_label[index_bord] = y.shape[1]-1
    big_label = big_label.reshape(big_image_super_index.shape)

    return big_label

def next_subplot_feature(num_row,num_col):
    """ compute the next subplot coordinate """

    if num_col >= 3:
        return (num_row+1, 1)
    return (num_row, num_col+1)

def visu_img(input,level):
    
    # add the image to the current subplot
    image = input[0].image.float()
    img_size = torch.tensor(image.shape,device=image.device)
    if torch.max(image) > 1:
        image = image/255.
    if len(image.shape) == 2:
        image = image.unsqueeze(0)
    if level==0:
        img = np.array(image.permute(1,2,0)) # to normalise data
        plt.imshow(img.astype(np.float32()),cmap="gray") # cmap ignored for rgb data
        plt.gca().set_axis_off()
        plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
        plt.margins(0, 0)
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())
    else:
        super_index = input.get_super_index(level,0)

        # create a subplot
        plt.title("nb super pixels : "+str(torch.max(super_index).long().item()+1))
        
        # add the image to the current subplot
        img = np.array(image_SuperPixel_visualize(image,super_index).permute(1,2,0))
        plt.imshow(img.astype(np.float32()),cmap="gray")
        plt.gca().set_axis_off()
        plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
        plt.margins(0, 0)
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())

def visu_label(input,level,class_color):
    # add the label to the current subplot
    label = input[0].label
    img_size = torch.tensor(label.shape,device=label.device)

    label[label>=class_color.shape[0]] = class_color.shape[0]
    
    if level==0:
        img = np.array(class_color[label])/255.
        plt.imshow(img.astype(np.float32()))
        plt.gca().set_axis_off()
        plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
        plt.margins(0, 0)
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())
    else:
        super_index = input.get_super_index(level)
        y = input[level].y
        
        # create a subplot
        plt.title("nb super pixels : "+str(torch.max(super_index).long().item()+1))
        
        # add the label to the current subplot
        img = np.array(class_color[label_SuperPixel_visualize(img_size,y,input[0].pos,super_index)])/255.
        plt.imshow(img.astype(np.float32()))
        plt.gca().set_axis_off()
        plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
        plt.margins(0, 0)
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())

def visu_pred(input,class_color):
    assert hasattr(input[0],"pred"), "no pred in the lowest level"
    assert hasattr(input[0],"image"), "need image size to show prediction"
    
    pred = input[0].pred
    pred = pred.argmax(1).numpy() if pred.dim() == 2 else pred.numpy() # if pred is an histogram or raw prediction
    pred = pred.reshape(input[0].image.shape[1:])

    pred[pred>=class_color.shape[0]] = class_color.shape[0]
    
    img = np.array(class_color[pred])/255.
    plt.imshow(img.astype(np.float32()))
    plt.gca().set_axis_off()
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())

def fill_comp(super_index,x,start):
    if x == 0:
        return super_index
    assert isinstance(start,list) and len(start)==2
    list_pixel = [start]
    selected = torch.zeros_like(super_index)
    selected[start[0],start[1]]=1
    while len(list_pixel)>0:
        l = list_pixel.pop(0)
        if l[0]+1<super_index.shape[0] and super_index[l[0]+1,l[1]] == 0 and selected[l[0]+1,l[1]] == 0:
            list_pixel.append([l[0]+1,l[1]])
            selected[l[0]+1,l[1]] = 1

        if l[0]-1>=0 and super_index[l[0]-1,l[1]] == 0 and selected[l[0]-1,l[1]] == 0:
            list_pixel.append([l[0]-1,l[1]])
            selected[l[0]-1,l[1]] = 1

        if l[1]+1<super_index.shape[1] and super_index[l[0],l[1]+1] == 0 and selected[l[0],l[1]+1] == 0:
            list_pixel.append([l[0],l[1]+1])
            selected[l[0],l[1]+1] = 1

        if l[1]-1>=0 and super_index[l[0],l[1]-1] == 0 and selected[l[0],l[1]-1] == 0:
            list_pixel.append([l[0],l[1]-1])
            selected[l[0],l[1]-1] = 1
        super_index[l[0],l[1]]=x
    return super_index

def fill_super_index(super_index):
    """ search for all connexe composante with 0 value 
    and replace index with increasing values starting at max+1 

    :parma super_index: super_index in image format
        super_index.shape == [w,h]
    """
    for i in range(super_index.shape[0]):
        for j in range(super_index.shape[1]):
            if super_index[i,j] == 0:
                super_index = fill_comp(super_index,super_index.max()+1,[i,j])
    return super_index

def visualize(input, path, keys = None, straight_line_tol=1.,class_color = None, Final_version=False): 
    """ create the figure visualising a DATA or a NAG
    
    :param input: Data or NAG object
    :param path: dir path where to pu all generated images
    :param keys: data to show
        accept in : ["image","label","SegError","Pred"] or ["all"]
        "SegError" show the error made by the oracle (with the level 1 partition)
    :param straight_line_tol: precision of multilabel_potrace_svg
    :param feature_keys: handcrafted feature to show
        handfeatures are in Nag[0].$keys$ or Data.$keys$
    :param class_color: color of each class [nclass+1,3]
    :param Final_version:to set the precision on the figure (dot per inches, dpi)
        to achieve lighter image during experiment 
    """

    straight_line_tol = straight_line_tol if straight_line_tol is not None else 1.

    dpi=100 # set figure precision

    assert isinstance(input, (Data, NAG))
    
    if not osp.isdir(path):
        os.mkdir(path)
    
    # We work on copies of the input data, to allow modified in this
    # scope
    input = input.clone().detach().cpu()

    # If the input is a simple Data object, we convert it to a NAG
    nag = NAG([input]) if isinstance(input, Data) else input

    # If the last level of the NAG has super_index, we manually
    # construct an additional Data level and append it to the NAG
    if nag[nag.num_levels - 1].is_sub:
        data_last = nag[nag.num_levels - 1]
        sub = Cluster(
            data_last.super_index, torch.arange(data_last.num_nodes),
            dense=True)
        pos = scatter_mean(data_last.pos, data_last.super_index, dim=0)
        nag = NAG(nag.to_list() + [Data(pos=pos, sub=sub)])
    
    nlevels = nag.num_levels
    
    # visualise pricipal data (rgb, labels, prediction)
    if not keys is None:
        if isinstance(keys, str):
            keys = [keys]

        if keys == ["all"] or "image" in keys:
            assert hasattr(nag[0],"image"), "no image in the lowest level"
            if getattr(nag[0],'x',None) is not None and nag[0].x.shape[0] == 3:
                image = nag[0].x
                image = (image-image.min())/(image.max()-image.min())
            else:
                image = nag[0].image.float()
                rgb_dim = getattr(nag[0],"rgb_dim",None)
                if rgb_dim is not None:
                    image = image[rgb_dim]

                if torch.max(image) > 2: # biger than 1+noise
                    image = image/255.
            
            image = torch.clamp(image,0.,1.)
            if len(image.shape) == 2:
                image = image.unsqueeze(0)
            img = np.array(image.permute(1,2,0))
            plt.figure(figsize=(img.shape[1]/dpi,img.shape[0]/dpi),dpi=dpi)
            plt.imshow(img.astype(np.float32()),cmap="gray") # cmap ignored for rgb data
            plt.gca().set_axis_off()
            plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
            plt.margins(0, 0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(osp.join(path,"image.png"))
            plt.close()

            if getattr(nag[0],'rotation',0) != 0 and nag[0].rotation%2:
                shape = list(image.shape[-2:])
                shape.reverse()
            else:
                shape = image.shape[-2:]

            for l in range(1,nlevels):
                super_index = nag.get_super_index(l)

                if hasattr(nag[l],'mean_rgb'):
                    colors = nag[l].mean_rgb.permute(1,0)
                    if colors.max() > 2:# biger than 1+noise
                        colors = colors/255.
                else:
                    rgb = image_to_feature(image,feature_dim = getattr(nag[0],'rgb_dim',None),
                        image_index=getattr(nag[0],'image_index',None),rot=getattr(nag[0],'rotation',0),
                        flip=getattr(nag[0],'flip',None))
                    colors = scatter_mean(rgb.permute(1,0),super_index) 

                if getattr(nag[0],'image_index',None) is not None:
                    sp = torch.zeros(image.shape[-2:].numel(),dtype=super_index.dtype,device=super_index.device)
                    sp[nag[0].image_index] = super_index+1
                    nb_comp_start = super_index.max()
                    super_index = fill_super_index(sp.reshape(shape[-2:]))-1
                    nb_comp_end = super_index.max()
                    colors = torch.cat((colors,
                        torch.zeros((3,nb_comp_end-nb_comp_start),dtype=colors.dtype,device=colors.device)),dim=1)
                else:
                    super_index = super_index.reshape(shape)

                if getattr(nag[0],'flip',None) is not None:
                    for f in nag[0].flip:
                        super_index = super_index.flip(f)

                if getattr(nag[0],'rotation',0) != 0:
                    super_index = super_index.rot90(k=nag[0].rotation)

                super_index = super_index.cpu()
                colors = colors.cpu()

                multilabel_potrace_svg(np.asfortranarray(super_index.numpy().astype(np.uint32)), f"{l}.svg", 
                    comp_colors = (colors.permute(1,0).contiguous().numpy()*255).astype(np.uint8), write_raw = True, write_poly = True,
                    straight_line_tol = straight_line_tol, smoothing = 0.,line_width = 1)
            
                with open(f"{l}.svg") as fp:
                    svg_1 = svgutils.compose.SVG(f"{l}.svg")

                with open(f"{l}_raw.svg") as fp:
                    svg_2 = svgutils.compose.SVG(f"{l}_raw.svg")
                
                if l == 1:
                    width = max(svg_1.width,svg_2.width)
                    height = max(svg_1.height,svg_2.height)
                    F_raw = svgutils.transform.SVGFigure(width,height*(nlevels-1))
                    F = svgutils.transform.SVGFigure(width,height*(nlevels-1))

                svg_1.move(0,(nlevels-l-1)*height)
                svg_2.move(0,(nlevels-l-1)*height)
                                    
                F.append(svg_1)
                F_raw.append(svg_2)

                os.system(f"rm {l}.svg")
                os.system(f"rm {l}_raw.svg")
            F.save(osp.join(path,"sp_rgb.svg"))
            F_raw.save(osp.join(path,"sp_rgb_raw.svg"))


        if keys == ["all"] or "label" in keys:
            assert not class_color is None, "to visualise labels, class_color is needed"
            assert hasattr(nag[0],"label"), "no label in the lowest level"
            
            label = nag[0].label

            #label[label>class_color.shape[0]] = class_color.shape[0]
            
            img = np.array(class_color[label])/255.
            plt.figure(figsize=(img.shape[1]/dpi,img.shape[0]/dpi),dpi=dpi)
            plt.imshow(img.astype(np.float32())) # cmap ignored for rgb data
            plt.gca().set_axis_off()
            plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
            plt.margins(0, 0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(osp.join(path,"label.png"))
            plt.close()
            
            if getattr(nag[0],'rotation',0) != 0 and nag[0].rotation%2:
                shape = list(label.shape)
                shape.reverse()
            else:
                shape = label.shape

            for l in range(1,nlevels):
                super_index = nag.get_super_index(l)
                y = nag[l].y.argmax(1)
                
                if getattr(nag[0],'image_index',None) is not None:
                    sp = torch.zeros(label.numel(),dtype=super_index.dtype,device=super_index.device)
                    sp[nag[0].image_index] = super_index+1
                    nb_comp_start = super_index.max()
                    super_index = fill_super_index(sp.reshape(shape))-1
                    nb_comp_end = super_index.max()
                    y = torch.cat((y,torch.ones(nb_comp_end-nb_comp_start,dtype=y.dtype,device=y.device)*(class_color.shape[0]-1)))
                else:
                    super_index = super_index.reshape(shape)

                if getattr(nag[0],'flip',None) is not None:
                    for f in nag[0].flip:
                        super_index = super_index.flip(f)

                if getattr(nag[0],'rotation',0) != 0:
                    super_index = super_index.rot90(k=nag[0].rotation)

                #y[y>class_color.shape[0]] = class_color.shape[0]
                colors = np.array(class_color[y])
                super_index = super_index.cpu()

                multilabel_potrace_svg(np.asfortranarray(super_index.numpy().astype(np.uint32)), f"{l}.svg", 
                    comp_colors = (colors).astype(np.uint8), write_raw = True, write_poly = True,
                    straight_line_tol = straight_line_tol, smoothing = 0.,line_width = 1)
                
                with open(f"{l}.svg") as fp:
                    svg_1 = svgutils.compose.SVG(f"{l}.svg")

                with open(f"{l}_raw.svg") as fp:
                    svg_2 = svgutils.compose.SVG(f"{l}_raw.svg")
                
                if l == 1:
                    width = max(svg_1.width,svg_2.width)
                    height = max(svg_1.height,svg_2.height)
                    F_raw = svgutils.transform.SVGFigure(width,height*(nlevels-1))
                    F = svgutils.transform.SVGFigure(width,height*(nlevels-1))

                svg_1.move(0,(nlevels-l-1)*height)
                svg_2.move(0,(nlevels-l-1)*height)
                                    
                F.append(svg_1)
                F_raw.append(svg_2)

                os.system(f"rm {l}.svg")
                os.system(f"rm {l}_raw.svg")
            F.save(osp.join(path,"sp_label.svg"))
            F_raw.save(osp.join(path,"sp_label_raw.svg"))

        if keys == ["all"] or "SegError" in keys:
            assert not class_color is None, "to visualise labels, class_color is needed"
            assert hasattr(nag[0],"label"), "no label in the lowest level"
            
            label = nag[0].label
            super_index = nag.get_super_index(1)
            
            if getattr(nag[0],'rotation',0) != 0 and nag[0].rotation%2:
                shape = list(label.shape)
                shape.reverse()
            else:
                shape = label.shape
            if getattr(nag[0],'image_index',None) is not None:
                sp = torch.zeros(label.numel(),dtype=super_index.dtype,device=super_index.device)
                sp[nag[0].image_index] = super_index+1
                super_index = torch.maximum(sp-1,torch.tensor(0,dtype=super_index.dtype,device=super_index.device)).reshape(shape)
            else:
                sp = None
                super_index = super_index.reshape(label.shape)
            if getattr(nag[0],'flip',None) is not None:
                for f in nag[0].flip:
                    super_index = super_index.flip(f)
            
            if getattr(nag[0],'rotation',0) != 0:
                super_index = super_index.rot90(k=nag[0].rotation)
            
            seg_truth = (nag[1].y.argmax(1)[super_index] == label)
            if sp is not None:
                seg_truth[sp.reshape(label.shape)==0] = 2
            seg_truth = seg_truth.numpy().astype(np.int8)
            seg_truth[label==class_color.shape[0]-1] = 2

            error_color= np.array([[1.,0,0], # incorrect prediction
                                [0,1,0], # correct
                                [1,1,1]]) # unlabeled

            img = error_color[seg_truth]
            plt.figure(figsize=(img.shape[1]/dpi,img.shape[0]/dpi),dpi=dpi)
            plt.imshow(img.astype(np.float32())) # cmap ignored for rgb data
            plt.gca().set_axis_off()
            plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
            plt.margins(0, 0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(osp.join(path,"seg_error.png"))
            plt.close()

        if keys == ["all"] or "Pred" in keys:
            assert not class_color is None, "to visualise predictions, class_color is needed"
            assert hasattr(nag[0],"pred"), "no pred in the lowest level"
            assert hasattr(nag[0],"image"), "need image size to show prediction"
            
            pred = nag[0].pred
            if pred.dim() > 1 and pred.shape[1] == class_color.shape[0]-1:
                pred = pred.argmax(1)

            image = nag[0].image
            if getattr(nag[0],'rotation',0) != 0 and nag[0].rotation%2:
                shape = list(image.shape[-2:])
                shape.reverse()
            else:
                shape = image.shape[-2:]
            
            pred = pred.numpy().reshape(shape) if pred.dim() != 2 else pred.numpy() # if pred is an histogram or raw prediction
            
            pred[pred>class_color.shape[0]] = class_color.shape[0]
            
            img = np.array(class_color[pred])/255.
            plt.figure(figsize=(img.shape[1]/dpi,img.shape[0]/dpi),dpi=dpi)
            plt.imshow(img.astype(np.float32())) # cmap ignored for rgb data
            plt.gca().set_axis_off()
            plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
            plt.margins(0, 0)
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            plt.savefig(osp.join(path,"pred.png"))
            plt.close()
            
            if hasattr(nag[0],"label"): # pred error only if annotation to compare to
                
                error_color= np.array([[0.,1,0], # correct prediction
                                    [1,0,0], # error
                                    [1,1,1]]) # unlabeled
                
                label = nag[0].label.numpy()
                #label[label>=class_color.shape[0]-1] = class_color.shape[0]-1
                error = (label!=pred)*1
                error[label==class_color.shape[0]-1] = 2
                img = np.array(error_color[error])
                plt.figure(figsize=(img.shape[1]/dpi,img.shape[0]/dpi),dpi=dpi)
                plt.imshow(img.astype(np.float32())) # cmap ignored for rgb data
                plt.gca().set_axis_off()
                plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
                plt.margins(0, 0)
                plt.gca().xaxis.set_major_locator(plt.NullLocator())
                plt.gca().yaxis.set_major_locator(plt.NullLocator())
                plt.savefig(osp.join(path,"pred_error.png"))
                plt.close()

def show_unique(input, key, level, class_color, path=None, file_name=None):
    fig = plt.figure(figsize = [5,5], dpi = 200)
    plt.axis('off')
    if key == "image":
        visu_img(input,level)
    if key == "label":
        visu_label(input,level)
    if key == "pred":
        visu_pred(input,class_color)

    # Sanitize title and path
    if file_name is None:
        file_name = "SPT Image"
    if path is not None:
        if osp.isdir(path):
            path = osp.join(path, f"{file_name}.svg")
        else:
            path = osp.splitext(path)[0] + '.svg' # make sure the extention is correct
    
    plt.savefig(path)
    plt.close()

def show(input, path=None, **kwargs):
    """Interactive data visualization.

    :param input: Data or NAG object
    :param path: dir path to save the visualizations
    :param kwargs: parameter of the visualisation
    :return:
    """
        
    assert osp.splitext(path)[1] == '', "must be dir path"
    
    # Draw a figure for 3D data visualization
    visualize(input, **kwargs)

def graph_visualization(nag_path, path, class_color, level=None, vertical_color=True, Final_version=False, compressed=False):
    """ graph visualisation (only to put on presentations)

    :param nag_path: path to preprocessed nag
    :param path: path to save resulting image
    :param class_color: color of each class [nclass+1,3]
    :param level: int or None
        if None show all level else show only given level
    :param vertical_color: bool
        color of the node relate to parents, else random
    :param Final_version:to set the precision on the figure (dot per inches, dpi)
        to achieve lighter image during experiment
    :param compressed: bool
        if nag files are compressed on disk
    """

    nag = NAG.load(nag_path, compressed=compressed)
    
    if level is None:
        n_row = nag.num_levels
        n_col = 3

        fig = plt_figure(n_row, n_col, num="Graph Visualisation", Final_version = Final_version)

        visualize(nag,keys=["image","label"],title="graph_visualisation",class_color=class_color,fig=fig)

        num_col = 3
        num_row = 1
        ax = new_subplot([num_row,num_col],fig)
        color_base = torch.tensor(list(itertools.product(np.linspace(0,1,np.ceil(np.power(nag.num_pixels[n_row-1],1/3)).astype(np.int32)),repeat=3))[:nag.num_pixels[n_row-1]])
        img = np.array(color_base[nag.get_super_index(n_row-1).reshape(nag[0].image.shape[-2:])]).astype(np.float32)
        plt.imshow(img)
        num_row = 2
        for i in range(1,n_row):
            g = torch_geometric.utils.to_networkx(nag[i], to_undirected=False)
            pos = {a.item(): b.numpy() for a,b in zip(torch.arange(g.number_of_nodes()).unsqueeze(1),nag[i].pos*torch.tensor([1,-1]))}
            ax = new_subplot([num_row,num_col],fig)
            if vertical_color:
                if i == n_row-1:
                    colors = color_base
                else:
                    colors = color_base[nag.get_super_index(n_row-1,i)]
            else:
                colors = scatter_mean(nag[0].image.flatten(-2).squeeze(),nag.get_super_index(i)).permute(1,0).contiguous()
            nx.draw(g,node_size=5,pos=pos,node_color=colors,ax=ax,margins=0.,arrows=False)
            num_row = num_row + 1
    else:
        colors = scatter_mean(nag[0].image.flatten(-2).squeeze(),nag.get_super_index(level)).permute(1,0).contiguous()
        g = torch_geometric.utils.to_networkx(nag[level], to_undirected=False)
        pos = {a.item(): b.numpy() for a,b in zip(torch.arange(g.number_of_nodes()).unsqueeze(1),nag[level].pos*torch.tensor([1,-1]))}
        nx.draw(g,node_size=5,pos=pos,node_color=colors,margins=0.,arrows=False)
        plt.axis("off")

    plt.savefig(path)
    plt.close()

def polygone_visualization(nag_path,path,straight_line_tol=1.,level=None,side=False,vertical_color=True,Final_version=False,compressed=False):
    """ graph visualisation (only to put on presentations)

    :param nag_path: nag to show
    :param path: path to save resulting image
    :param straight_line_tol: float
        precision of smoothing border algorithm (higher meens less precise)
    :param level: int or None
        if None show all level else show only given level
    :param side: bool
        if level is integer and side is True show the smoothed level, else show raw level
    :param vertical_color: bool
        color of the node relate to parents, else random
    :param Final_version:to set the precision on the figure (dot per inches, dpi)
        to achieve lighter image during experiment
    :param compressed: bool
        if nag files are compressed on disk
    """

    nag = NAG.load(nag_path, compressed=compressed)
    
    nlevels = nag.num_levels
    w,h = nag[0].image.shape[-2:]

    if level is None:
        color_base = torch.tensor(list(itertools.product(np.linspace(0,1,np.ceil(np.power(nag.num_pixels[nlevels-1],1/3)).astype(np.int32)),repeat=3))[:nag.num_pixels[nlevels-1]])
        for i in range(1,nlevels):
            if vertical_color:
                if i == nlevels-1:
                    colors = color_base
                else:
                    colors = color_base[nag.get_super_index(nlevels-1,i)]
            else:
                colors = scatter_mean(nag[0].image.flatten(-2).squeeze(),nag.get_super_index(i)).permute(1,0).contiguous()

            multilabel_potrace_svg(nag.get_super_index(i).reshape((w,h)).numpy().astype(np.uint16), f"{i}.svg", 
                comp_colors = (colors.numpy()*255).astype(np.uint8),
                write_raw = True, write_poly = True, straight_line_tol = straight_line_tol, smoothing = 0.,line_width = 1)
            
        for i in range(1,nlevels):
            with open(f"{i}.svg") as fp:
                svg_1_content = fp.read()

            with open(f"{i}_raw.svg") as fp:
                svg_2_content = fp.read()

            svg_1_element = svg2rlg(io.StringIO(svg_1_content))
            svg_2_element = svg2rlg(io.StringIO(svg_2_content))

            if i == 1:
                width = svg_1_element.width+svg_2_element.width
                height = max(svg_1_element.height,svg_2_element.height)
                d = Drawing(width,height*(nlevels-1))

            svg_1_element.shift(0,(nlevels-i-1)*height)
            svg_2_element.shift(svg_1_element.width,(nlevels-i-1)*height)
            
            d.add(svg_1_element)
            d.add(svg_2_element)

            os.system(f"rm {i}.svg")
            os.system(f"rm {i}_raw.svg")
    else:
        colors = scatter_mean(nag[0].image.flatten(-2).squeeze(),nag.get_super_index(level)).permute(1,0).contiguous()
        multilabel_potrace_svg(nag.get_super_index(level).reshape((w,h)).numpy().astype(np.uint16), f"{level}.svg", 
                comp_colors = (colors.numpy()*255).astype(np.uint8),
                write_raw = True, write_poly = True, straight_line_tol = straight_line_tol, smoothing = 0.,line_width = 1)
        
        if side:
            with open(f"{level}.svg") as fp:
                svg_content = fp.read()
        else:
            with open(f"{level}_raw.svg") as fp:
                svg_content = fp.read()

        svg_element = svg2rlg(io.StringIO(svg_content))
        width = svg_element.width
        height = svg_element.height
        d = Drawing(width,height)
        
        d.add(svg_element)

        os.system(f"rm {level}.svg")
        os.system(f"rm {level}_raw.svg")

    renderPDF.drawToFile(d, f"{path}.pdf")

def visualise_polygones(dataloader,nb_classes,nb_show,path):
    """
    visualize polygone separate by classes and level
    """

    compt_plot_classes = None
    
    for batch in dataloader():
        for nag in batch:
            if compt_plot_classes is None:
                compt_plot_classes = torch.zeros(nb_classes,nag.num_levels-1).long()
            for l in range(nag.num_levels-1):
                fig = plt_figure(nb_classes, nb_show, num=l)
                data = nag[l+1]
                sizes = nag.get_sub_size(l+1)
                classif = torch.argmax(data.y,dim=1)
                classes = torch.unique(classif)
                classes = classes[classes<nb_classes]
                for c in classes:
                    if compt_plot_classes[c,l] < nb_show:
                        nb_select = min(5,nb_show-compt_plot_classes[c,l])
                        index = (classif==c).nonzero(as_tuple=True)[0]
                        index = index[torch.randperm(index.numel())[:nb_select]]
                        pos = data.shapes.pos[:,:2]
                        for i in index:
                            ax = new_subplot([c+1,compt_plot_classes[c,l]+1], fig, title=f"{sizes[i]} pixels")
                            compt_plot_classes[c,l] += 1
                            edges = data.shapes.edge_index[:,torch.logical_or(*torch.isin(data.shapes.edge_index,*(data.shapes.batch_node==i).nonzero(as_tuple=True)))]
                            for j in range(edges.shape[1]):
                                ax.plot([pos[edges[0,j],0],pos[edges[1,j],0]],[pos[edges[0,j],1],pos[edges[1,j],1]],'k')
            if torch.all(compt_plot_classes>=nb_show):
                break
        if torch.all(compt_plot_classes>=nb_show):
            break
    
    for l in range(nag.num_levels-1):
        fig = plt_figure(nb_classes, nb_show, num=l)
        plt.savefig(path+f"{l+1}"+".svg")

import progressbar

def visualise_interfaces(dataloader,nb_classes,nb_show,path):
    """
    visualize polygone separate by classes and level
    """

    nb_paires = int(np.math.factorial(nb_classes+1)/(2*np.math.factorial(nb_classes-1)))

    compt_plot_classes = None

    for batch in dataloader():
        for nag in batch:
            if compt_plot_classes is None:
                compt_plot_classes = torch.zeros(nb_paires,nag.num_levels-1).long()
                bar = progressbar.ProgressBar(maxval=nb_paires*(nag.num_levels-1)*nb_show, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
                bar.start()
            for l in range(nag.num_levels-1):
                fig = plt_figure(nb_paires, nb_show, num=l)
                data = nag[l+1]
                sizes = nag.get_sub_size(l+1)
                classif = torch.argmax(data.y,dim=1)
                classes = torch.unique(classif)
                classes = classes[classes<nb_classes]
                for c1 in classes:
                    for c2 in classes:
                        if c2>=c1:
                            c=c1*nb_classes+c2-(c1*(c1+1))//2
                            if compt_plot_classes[c,l] < nb_show:
                                nb_select = min(nb_show//2,nb_show-compt_plot_classes[c,l])
                                index_1 = (classif==c1).nonzero(as_tuple=True)[0]
                                index_2 = (classif==c2).nonzero(as_tuple=True)[0]
                                index = torch.logical_and(torch.isin(data.edge_index[0],index_1),torch.isin(data.edge_index[1],index_2)).nonzero(as_tuple=True)[0]
                                index = index[torch.randperm(index.numel())[:nb_select]]
                                pos = data.interfaces.pos[:,:2]
                                for i in index:
                                    if i > data.interfaces.batch_node.max():
                                        i = i-data.interfaces.batch_node.max()
                                    ax = new_subplot([c+1,compt_plot_classes[c,l]+1], fig, title=f"classes : {c1}\{c2}; length : {int(data.interface_length[i])}")
                                    compt_plot_classes[c,l] += 1
                                    edges = data.interfaces.edge_index[:,torch.logical_or(*torch.isin(data.interfaces.edge_index,*(data.interfaces.batch_node==i).nonzero(as_tuple=True)))]
                                    for j in range(edges.shape[1]):
                                        ax.plot([pos[edges[0,j],0],pos[edges[1,j],0]],[pos[edges[0,j],1],pos[edges[1,j],1]],'k')
                                    bar.update(torch.sum(compt_plot_classes))
            if torch.all(compt_plot_classes>=nb_show):
                break
        if torch.all(compt_plot_classes>=nb_show):
            break
    bar.finish()
    for l in range(nag.num_levels-1):
        fig = plt_figure(nb_paires, nb_show, num=l)
        plt.savefig(path+f"{l+1}"+".svg")
