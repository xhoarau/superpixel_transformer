# Datasets

All datasets inherit from the `torch_geometric` `Dataset` class, allowing for automated preprocessing and inference-time transforms. 
See the [official documentation](https://pytorch-geometric.readthedocs.io/en/latest/tutorial/create_dataset.html) for more details. 

## Supported datasets
<div align="center">

| Dataset                                               |                                                Download from ?                                       |                    Which files ?                       |       Where to ?      |
|:------------------------------------------------------|:-----------------------------------------------------------------------------------------------------|:-------------------------------------------------------|:----------------------|
| [PacalVOC](http://host.robots.ox.ac.uk/pascal/VOC/)   |[link](http://host.robots.ox.ac.uk/pascal/VOC/) and [link](http://host.robots.ox.ac.uk/pascal/VOC/)   |   `VOCtrainval_11-May-2012.tar` and `benchmark.tgz`    |   `data/pascalvoc/`   |

</div>


### Structure of the `data/` directory
<details>
<summary><b>S3DIS data directory structure.</b></summary>
<br><br>
```
└── data/
    └── VOCtrainval_11-May-2012.tar
    └── raw/
        └── VOC2012/
            └── JPEGImages/
                └── 2007_000027.jpg
                └── ...
            └── ImageSets/
                └── Segmentation/
                    └── train.txt
                    └── trainval.txt
                    └── val.txt
            └── dataset/
                └── cls/
                    └── 2008_000002.mat # num class encoding
                    └── ...
                └── train.txt
                └── val.txt
            └── SegmentationClass/
                └── 2007_000032.png # color encoding
                └── ...
            └── Normalised_label/
                └── 2007_000032.png # num class encoding
                └── ...
                └── 2008_000002.png # num class encoding
                └── ... 
    └── processed/
        └── train/
        └── val/
        └── test/
```
<br>
</details>

> **Note**: **Already have the dataset on your machine ?** Save memory 💾 by 
> simply symlinking or copying the files to `data/<dataset_name>/raw/`, following the 
> [above-described `data/` structure](#structure-of-the-data-directory).

### Automatic download and preprocessing
Following `torch_geometric`'s `Dataset` behaviour:
0. Dataset instantiation <br>
➡ Load preprocessed data in `data/<dataset_name>/processed`
1. Missing files in `data/<dataset_name>/processed` structure<br>
➡ **Automatic** preprocessing using files in `data/<dataset_name>/raw`
2. Missing files in `data/<dataset_name>/raw` structure<br>
➡ **Automatic** unzipping of the downloaded dataset in `data/<dataset_name>`
3. Missing downloaded dataset in `data/<dataset_name>` structure<br>
➡ ~~**Automatic**~~ **manual** download to `data/<dataset_name>`

## Setting up your own `data/` and `logs/` paths
The `data/` and `logs/` directories will store all your datasets and training 
logs. By default, these are placed in the repository directory. 

Since this may take some space, or your heavy data may be stored elsewhere, you 
may specify other paths for these directories by creating a 
`configs/local/defaults.yaml` file containing the following:

```yaml
# @package paths

# path to data directory
data_dir: /path/to/your/data/

# path to logging directory
log_dir: /path/to/your/logs/
```

## Pre-transforms, transforms, on-device transforms

Pre-transforms are the functions making up the preprocessing. 
These are called only once and their output is saved in 
`data/<dataset_name>/processed/`. These typically encompass neighbor search and 
partition construction.

The transforms are called by the `Dataloaders` at batch-creation time. These 
typically encompass sampling and data augmentations and are performed on CPU, 
before moving the batch to the GPU.

On-device transforms, are transforms to be performed on GPU. These are 
typically compute intensive operations that could not be done once and for all 
at preprocessing time, and are too slow to be performed on CPU.

## Preprocessing hash
Different from `torch_geometric`, you can have **multiple 
preprocessed versions** of each dataset, identified by their preprocessing hash.

This hash will change whenever the preprocessing configuration 
(_i.e._ pre-transforms) is modified in an impactful way (_e.g._ changing the 
partition regularization). 

Modifications of the transforms and on-device 
transforms will not affect your preprocessing hash.

## Mini datasets
Each dataset has a "mini" version which only processes a portion of the data, to
speedup experimentation. To use it, set your the 
[dataset config](configs/datamodule) of your choice:
```yaml
mini: True
```

Or, if you are using the CLI, use the following syntax:
```shell script
# Train SPT on mini-DALES
python src/train.py experiment=dales +datamodule.mini=True
```

## Creating your own dataset
To create your own dataset, you will need to do the following:
- create `YourDataset` class inheriting from `src.datasets.BaseDataset`
- create `YourDataModule` class inheriting from `src.datamodules.DataModule`
- create `configs/datamodule/your_dataset.yaml` config 
 
Instructions are provided in the docstrings of those classes and you can get
inspiration from our code for S3DIS, KITTI-360 and DALES to get started. 

We suggest that your config inherits from `configs/datamodule/default.yaml`. See
`configs/datamodule/s3dis.yaml`, `configs/datamodule/kitti360.yaml`, and 
`configs/datamodule/dales.yaml` for inspiration.

#### Semantic label format
The semantic labels of your dataset must follow certain rules. 

Indeed, your points are expected to have labels within $[0, C]$, where: $C$ is 
the `num_classes` you define in your `YourDataset`. 

- **All labels $[0, C - 1]$ are assumed to be present in your dataset**. As 
such, they will all be used in metrics and losses computation.
- A point with the **$C$ label will be considered void/ignored/unlabeled** 
(whichever you call it). As such, it will be excluded from from metrics and 
losses computation

Hence, make sure the **output of your `YourDataset.read_single_raw_image()` 
reader method never returns labels outside of your $[0, C]$ range**. Besides, 
if some labels in $[0, C - 1]$ are not useful to you (ie absent from your 
dataset), we recommend you remap your labels to a new $[0, C' - 1]$ range
(`torch_geometric.nn.pool.consecutive.consecutive_cluster` can help you with 
that, if need be), while making sure you only use the label $C'$ for
void/ignored/unlabeled points. 
